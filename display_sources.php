<?php
header("Content-Type: text/html; charset=utf-8");
//проверяем, что выполняется из сессии (т.е. не вызвано левым способом)
include("check_session.php");
//считываем переданные параметры
if (isset($_POST['project_id'])){
	$projectID = $_POST['project_id'];
}
if (isset($_POST['type_id'])){
	$typeID = $_POST['type_id'];
}

include ("db_open.php");
//Защита от SQL инъекций
$projectID = (int)$projectID;
$typeID = (int)$typeID;
//Если вдруг номер проекта не соответствует пользователю (кто-то пытается хакнуть), завершаем выполнение
include("check_project.php");

//*******************************************
//Главная исполн¤ема¤ часть начинается отсюда
//*******************************************

//находим источники по данному проекту заданного типа (1 из 3)
$query = "SELECT id, name, visible FROM data_sources WHERE project_id = '{$projectID}' AND type_id = '{$typeID}'";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());		
//выводим их строками таблицы
for ($i=0; $i<mysql_num_rows($result); $i++){
	$sourceName = mysql_result($result,$i,'name');
	$sourceID = mysql_result($result,$i,'id');
	$sourceVisible = mysql_result($result,$i,'visible');
?>
	
	<tr data-source_id="<?php echo $sourceID;?>" data-type_id="<?php echo $typeID;?>">
	<td title="Показать или скрыть источник"><input class="source-visible checkbox2"type="checkbox" <?php if ($sourceVisible == "1") echo "checked";?>></td>
	<td><input class="source-name" type="text" size="20" maxlength="100" value="<?php echo $sourceName;?>"></td>
	<td title="удалить источник" class="delete-source"><div class="del-icon"></div></td>
	</tr>
<?php
}
mysql_free_result($result);
include ("db_close.php");
?>