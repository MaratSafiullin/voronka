<?php

//класс для JSON объекта для формирования выходных данных
class ChartData {

    public $cols = array();
    public $rows = array();

}

//столбец
class Column {

    public $id = "";
    public $label = "";
    public $type = "";

}

//строка
class Row {

    public $c = array();

}

//значение в строке
class RowVal {

    public $v = "";

}

//функция для получения дат первого и последнего дня недели по ее номеру (в году) и году
function getDatesByWeek($_week_number, $_year = null) {
    $year = $_year ? $_year : date('Y');
    $week_number = sprintf('%02d', $_week_number);
    $date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
    $date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
    return array($date_base, $date_limit);
}

//функция для получения обозначения недели по ее году-номеру
function getWeekTitle($yearWeek) {
    $year = substr($yearWeek, 0, 4);
    $week = substr($yearWeek, 5, 2);
    $resArr = getDatesByWeek($week, $year);
    return date('Y.m.d', $resArr[0]) . " - " . date('Y.m.d', $resArr[1]);
}

//функция для получения последнего дня недели по ее году-номеру
function getWeekLastDay($yearWeek) {
    $year = substr($yearWeek, 0, 4);
    $week = substr($yearWeek, 5, 2);
    $resArr = getDatesByWeek($week, $year);
    return date('Y-m-d', $resArr[1]);
}

//функция для получения последнего месяца недели по его году-номеру
function getMonthLastDay($yearMonth) {
    $date = strtotime($yearMonth . "-1");
    $date = $date + 3600 * 24 * 31;
    $monthDay = (integer) date("j", $date);
    $date = $date - 3600 * 24 * $monthDay;
    return date('Y-m-d', $date);
}

//получить объект JSON с данными графика
function getChartData($chartType, $projectID, $displayType, $sourceTypeID, $showFromDate, $showToDate, $columnNames, $sourcesTotal) {
    //$sourcesTotal = false;
    //крайние даты
    $stopDate = date('Y-m-d', strtotime($showToDate));
    $startDate = date('Y-m-d', strtotime($showFromDate));

    //задаем группировку для графиков Timeline в зависимости от периода
    switch ($displayType) {
        case "days":
            $dateFormat = "DATE_FORMAT(record_days.date,'%Y-%m-%d')";
            break;
        case "weeks":
            $dateFormat = "DATE_FORMAT(record_days.date,'%x-%v')";
            break;
        case "months":
            $dateFormat = "DATE_FORMAT(record_days.date,'%x-%m')";
            break;
        case "range":
            $dateFormat = "'{$showFromDate} - {$showToDate}'";
            break;
    }


    //задаем запрашиваемый параметр
    switch ($chartType) {
        case "displays":
        case "total_displays":
            $chartDataParam = "SUM(displays_count)";
            break;
        case "clicks":
        case "total_clicks":
            $chartDataParam = "SUM(clicks_count)";
            break;
        case "leads":
        case "total_leads":
            $chartDataParam = "SUM(leads_count)";
            break;
        case "customers":
        case "total_customers":
            $chartDataParam = "SUM(customers_count)";
            break;
        case "income":
        case "total_income":
            $chartDataParam = "SUM(income)";
            break;
        case "profit":
        case "total_profit":
            $chartDataParam = "SUM(profit) - SUM(clicks_cost)";
            break;
        case "cost":
        case "total_cost":
            $chartDataParam = "SUM(clicks_cost)";
            break;
        case "roi":
            $chartDataParam = "(SUM(profit) - SUM(clicks_cost)) / SUM(clicks_cost)";
            break;
        case "total_avgbill":
            $chartDataParam = "SUM(income) / SUM(customers_count)";
            break;
        case "cost_display":
            $chartDataParam = "SUM(clicks_cost) / SUM(displays_count)";
            break;
        case "cost_click":
            $chartDataParam = "SUM(clicks_cost) / SUM(clicks_count)";
            break;
        case "cost_lead":
            $chartDataParam = "SUM(clicks_cost) / SUM(leads_count)";
            break;
        case "cost_customer":
            $chartDataParam = "SUM(clicks_cost) / SUM(customers_count)";
            break;
        case "cv_displays_clicks":
            $chartDataParam = "SUM(clicks_count) / SUM(displays_count)";
            break;
        case "cv_clicks_leads":
            $chartDataParam = "SUM(leads_count) / SUM(clicks_count)";
            break;
        case "cv_leads_customers":
            $chartDataParam = "SUM(customers_count) / SUM(leads_count)";
            break;
        case "cv_clicks_customers":
            $chartDataParam = "SUM(customers_count) / SUM(clicks_count)";
            break;
        case "mix_income_profit":
            $chartDataParam1 = "SUM(income)";
            $chartDataParam2 = "SUM(profit) - SUM(clicks_cost)";
            break;
        case "mix_income_cost":
            $chartDataParam1 = "SUM(income)";
            $chartDataParam2 = "SUM(clicks_cost)";
            break;
        case "mix_profit_cost":
            $chartDataParam1 = "SUM(profit) - SUM(clicks_cost)";
            $chartDataParam2 = "SUM(clicks_cost)";
            break;
        case "mix_income_clicks":
            $chartDataParam1 = "SUM(income)";
            $chartDataParam2 = "SUM(clicks_count)";
            break;
        case "mix_income_leads":
            $chartDataParam1 = "SUM(income)";
            $chartDataParam2 = "SUM(leads_count)";
            break;
        case "mix_income_customers":
            $chartDataParam1 = "SUM(income)";
            $chartDataParam2 = "SUM(customers_count)";
            break;
        case "mix_income_avgbill":
            $chartDataParam1 = "SUM(income)";
            $chartDataParam2 = "SUM(income) / SUM(customers_count)";
            break;
        case "mix_clicks_cost":
            $chartDataParam1 = "SUM(clicks_count)";
            $chartDataParam2 = "SUM(clicks_cost)";
            break;
        case "mix_clickcost_clicks":
            $chartDataParam1 = "SUM(clicks_cost) / SUM(clicks_count)";
            $chartDataParam2 = "SUM(clicks_count)";
            break;
        case "mix_clicks_leads":
            $chartDataParam1 = "SUM(clicks_count)";
            $chartDataParam2 = "SUM(leads_count)";
            break;
        case "mix_leads_customers":
            $chartDataParam1 = "SUM(leads_count)";
            $chartDataParam2 = "SUM(customers_count)";
            break;
        case "mix_customercost_arppu":
            $chartDataParam1 = "SUM(clicks_cost) / SUM(customers_count)";
            $chartDataParam2 = "(SUM(profit) - SUM(clicks_cost)) / SUM(customers_count)";
            break;
        case "mix_clickcost_arpu":
            $chartDataParam1 = "SUM(clicks_cost) / SUM(clicks_count)";
            $chartDataParam2 = "(SUM(profit) - SUM(clicks_cost)) / SUM(clicks_count)";
            break;
        case "mix_clicks_cv1":
            $chartDataParam1 = "SUM(clicks_count)";
            $chartDataParam2 = "SUM(leads_count) / SUM(clicks_count)";
            break;
        case "mix_leads_cv2":
            $chartDataParam1 = "SUM(leads_count)";
            $chartDataParam2 = "SUM(customers_count) / SUM(leads_count)";
            break;
        case "mix_customers_avgbill":
            $chartDataParam1 = "SUM(customers_count)";
            $chartDataParam2 = "SUM(income) / SUM(customers_count)";
            break;
        case "mix_customers_profitability":
            $chartDataParam1 = "SUM(customers_count)";
            $chartDataParam2 = "(SUM(profit) - SUM(clicks_cost)) / SUM(income)";
            break;

        case "add_avgbill":
            $chartDataParam = "SUM(income) / SUM(customers_count)";
            break;
        case "add_profitability":
            $chartDataParam = "(SUM(profit) - SUM(clicks_cost)) / SUM(income)";
            break;
        case "add_arppu":
            $chartDataParam = "(SUM(profit) - SUM(clicks_cost)) / SUM(customers_count)";
            break;
        case "add_arpu":
            $chartDataParam = "(SUM(profit) - SUM(clicks_cost)) / SUM(clicks_count)";
            break;
    }

    //объект для хранения данных графика
    $chartData = new ChartData;

    //Задаем колонки графика и параметры запроса в зависимости от типа графика (итоги, смешаный или Timeline)
    switch ($chartType) {
        //для итоговых диаграмм (Pie Chart)
        case "total_displays":
        case "total_clicks":
        case "total_leads":
        case "total_customers":
        case "total_income":
        case "total_profit":
        case "total_cost":
        case "total_avgbill":
            //задаем колонки
            //Источник данных
            $dateCol = new Column;
            $dateCol->id = "source";
            $dateCol->label = "Источник";
            $dateCol->type = "string";
            $chartData->cols[0] = $dateCol;
            //показатель для источника
            $dateCol = new Column;
            $dateCol->id = "data";
            $dateCol->label = "Показатель";
            $dateCol->type = "number";
            $chartData->cols[1] = $dateCol;
            //запрашиваемые колонки для SQL запроса
            $selectedColumns = "{$chartDataParam} AS chart_data, data_sources.name AS source_name, data_sources.id AS src_id";
            //группировка для SQL запроса
            $grouping = "source_name, src_id";
            //сортировка для SQL запроса
            $sorting = "source_name, src_id";
            break;
        //для смешаных диаграмм (Финансы)
        case "mix_income_profit":
        case "mix_income_cost":
        case "mix_profit_cost":
        case "mix_income_clicks":
        case "mix_income_leads":
        case "mix_income_customers":
        case "mix_income_avgbill":
        case "mix_clicks_cost":
        case "mix_clickcost_clicks":
        case "mix_clicks_leads":
        case "mix_leads_customers":
        case "mix_customercost_arppu":
        case "mix_clickcost_arpu":
        case "mix_clicks_cv1":
        case "mix_leads_cv2":
        case "mix_customers_avgbill":
        case "mix_customers_profitability":
            //колонка даты/недели/месяца
            $dateCol = new Column;
            $dateCol->id = "dateCol";
            switch ($displayType) {
                case "days":
                    $dateCol->label = "Дата";
                    $dateCol->type = "date";
                    break;
                case "weeks":
                    $dateCol->label = "Неделя";
                    $dateCol->type = "date";
                    break;
                case "months":
                    $dateCol->label = "Месяц";
                    $dateCol->type = "date";
                    break;
                case "range":
                    $dateCol->label = "Диапазон";
                    $dateCol->type = "string";
                    break;
            }
            $chartData->cols[0] = $dateCol;
            //колонки данных
            $columnNames = explode("+", $columnNames);
            $dateCol = new Column;
            $dateCol->id = "data_1";
            $dateCol->label = "Data1"; ////////////$columnNames[0];
            $dateCol->type = "number";
            $chartData->cols[1] = $dateCol;
            $dateCol = new Column;
            $dateCol->id = "data_2";
            $dateCol->label = "Data2"; /////////////$columnNames[1];
            $dateCol->type = "number";
            $chartData->cols[2] = $dateCol;
            //запрашиваемые колонки для SQL запроса
            $selectedColumns = "{$dateFormat} AS date, {$chartDataParam1} AS chart_data_1, {$chartDataParam2} AS chart_data_2";
            //группировка для SQL запроса
            $grouping = "{$dateFormat}";
            //сортировка для SQL запроса
            $sorting = "date";
            break;
        //для графиков Timeline
        case "displays":
        case "clicks":
        case "leads":
        case "customers":
        case "income":
        case "profit":
        case "cost":
        case "roi":
        case "cost_display":
        case "cost_click":
        case "cost_lead":
        case "cost_customer":
        case "cv_displays_clicks":
        case "cv_clicks_leads":
        case "cv_leads_customers":
        case "cv_clicks_customers":
        case "add_avgbill":
        case "add_profitability":
        case "add_arppu":
        case "add_arpu":
            //колонка даты/недели/месяца
            $dateCol = new Column;
            $dateCol->id = "dateCol";
            switch ($displayType) {
                case "days":
                    $dateCol->label = "Дата";
                    $dateCol->type = "date";
                    break;
                case "weeks":
                    $dateCol->label = "Неделя";
                    $dateCol->type = "date";
                    break;
                case "months":
                    $dateCol->label = "Месяц";
                    $dateCol->type = "date";
                    break;
                case "range":
                    $dateCol->label = "Диапазон";
                    $dateCol->type = "string";
                    break;
            }
            $chartData->cols[0] = $dateCol;

            //если запрашиваются итоги по всем источникам, то колонка показателя только одна
            if ($sourcesTotal) {
                $newSourceCol = new Column;
                $newSourceCol->id = "sourceCol";
                $newSourceCol->label = "Все источники";
                $newSourceCol->type = "number";
                $chartData->cols[1] = $newSourceCol;
                //группировка для SQL запроса
                $grouping = "{$dateFormat}";
                //сортировка для SQL запроса
                $sorting = "date";
            }
            //иначе их много
            else {
                //запрос на список видимых источников (для колонок)
                $query = "SELECT data_sources.name AS source_name, data_sources.id AS src_id FROM data_sources 
                            INNER JOIN projects ON projects.id = data_sources.project_id
                            WHERE projects.id = '{$projectID}' AND data_sources.type_id = {$sourceTypeID} AND data_sources.visible = '1'
                            ORDER BY source_name, src_id";
                //получаем данные
                $result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
                for ($i = 0; $i < mysql_num_rows($result); $i++) {
                    $sourceName = mysql_result($result, $i, 'source_name');
                    //сохраняем колонки
                    $newSourceCol = new Column;
                    $newSourceCol->id = "sourceCol" . ($i + 1);
                    $newSourceCol->label = $sourceName;
                    $newSourceCol->type = "number";
                    $chartData->cols[$i + 1] = $newSourceCol;
                }
                mysql_free_result($result);
                //группировка для SQL запроса
                $grouping = "{$dateFormat}, source_name, src_id";
                //сортировка для SQL запроса
                $sorting = "date, source_name, src_id";
            }
            //запрашиваемые колонки для SQL запроса
            $selectedColumns = "{$dateFormat} AS date, {$chartDataParam} AS chart_data, data_sources.name AS source_name, data_sources.id AS src_id";
            break;
    }

    //формируем запрос для получения данных по источникам для заданного диапазона с группировкой по периодам
    $query = "SELECT {$selectedColumns}
	FROM sales_data
	INNER JOIN data_sources ON data_sources.id = sales_data.data_source_id 
	INNER JOIN record_days ON record_days.id = sales_data.day_id
	INNER JOIN projects ON projects.id = record_days.project_id
	WHERE record_days.date >= '{$startDate}' AND record_days.date <= '{$stopDate}' AND projects.id = '{$projectID}' AND data_sources.type_id = {$sourceTypeID} AND data_sources.visible = '1'
	GROUP BY {$grouping}
	ORDER BY {$sorting}";
    //получаем данные
    $result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
    switch ($chartType) {
        //для итоговых диаграмм (Pie Chart)
        case "total_displays":
        case "total_clicks":
        case "total_leads":
        case "total_customers":
        case "total_income":
        case "total_profit":
        case "total_cost":
        case "total_avgbill":
            $chartData->rows[0] = null;
            for ($i = 0; $i < mysql_num_rows($result); $i++) {
                $sourceName = mysql_result($result, $i, 'source_name');
                $rowData = mysql_result($result, $i, 'chart_data');
                $newRow = new Row;
                $val = new RowVal;
                $val->v = $sourceName;
                $newRow->c[0] = $val;
                $val = new RowVal;
                $val->v = $rowData;
                $newRow->c[1] = $val;
                $chartData->rows[$i] = $newRow;
            }
            break;
        //для смешаных диаграмм (Финансы)
        case "mix_income_profit":
        case "mix_income_cost":
        case "mix_profit_cost":
        case "mix_income_clicks":
        case "mix_income_leads":
        case "mix_income_customers":
        case "mix_income_avgbill":
        case "mix_clicks_cost":
        case "mix_clickcost_clicks":
        case "mix_clicks_leads":
        case "mix_leads_customers":
        case "mix_customercost_arppu":
        case "mix_clickcost_arpu":
        case "mix_clicks_cv1":
        case "mix_leads_cv2":
        case "mix_customers_avgbill":
        case "mix_customers_profitability":
            $chartData->rows[0] = null;
            $maxData1 = 0;
            $maxData2 = 0;
            for ($i = 0; $i < mysql_num_rows($result); $i++) {
                $date = mysql_result($result, $i, 'date');
                $rowData1 = mysql_result($result, $i, 'chart_data_1');
                $rowData2 = mysql_result($result, $i, 'chart_data_2');
                //максимумы значений
                if ($rowData1 > $maxData1)
                    $maxData1 = $rowData1;
                if ($rowData2 > $maxData2)
                    $maxData2 = $rowData2;
                //
                $newRow = new Row;
                $dateVal = new RowVal;
                //преобразуем полученное значение (чтоб получить формат "год-месяц-дата" для колонок с датой) 
                switch ($displayType) {
                    case "weeks":
                        $dateVal->v = getWeekLastDay($date);
                        break;
                    case "months":
                        $dateVal->v = getMonthLastDay($date);
                        break;
                    case "days":
                    case "range":
                        $dateVal->v = $date;
                        break;
                }
                $newRow->c[0] = $dateVal;

                $val = new RowVal;
                $val->v = $rowData1;
                $val->f = $rowData1;
                $newRow->c[1] = $val;

                $val = new RowVal;
                $val->v = $rowData2;
                $val->f = $rowData2;
                $newRow->c[2] = $val;
                $chartData->rows[$i] = $newRow;
            }
            //приводим все значения в строках к процентам от максимального
            for ($i = 0; $i < count($chartData->rows); $i++) {
                if (($maxData1 != 0) && ($chartData->rows[$i]->c[1]->v != null))
                    $chartData->rows[$i]->c[1]->v = $chartData->rows[$i]->c[1]->v / $maxData1;
                if (($maxData2 != 0) && ($chartData->rows[$i]->c[2]->v != null))
                    $chartData->rows[$i]->c[2]->v = $chartData->rows[$i]->c[2]->v / $maxData2;
            }
            break;
        //для графиков Timeline
        case "displays":
        case "clicks":
        case "leads":
        case "customers":
        case "income":
        case "profit":
        case "cost":
        case "roi":
        case "cost_display":
        case "cost_click":
        case "cost_lead":
        case "cost_customer":
        case "cv_displays_clicks":
        case "cv_clicks_leads":
        case "cv_leads_customers":
        case "cv_clicks_customers":
        case "add_avgbill":
        case "add_profitability":
        case "add_arppu":
        case "add_arpu":
            $currentDate = "";
            $currentRow = 0;
            $chartData->rows[0] = null;
            for ($i = 0; $i < mysql_num_rows($result); $i++) {
                $date = mysql_result($result, $i, 'date');
                $sourceName = mysql_result($result, $i, 'source_name');
                $rowData = mysql_result($result, $i, 'chart_data');
                //при достижении новой даты создаем новую строку
                if ($date != $currentDate) {
                    //перед этим добавляем текущую строку в выходные данные 
                    if ($currentDate != "") {
                        $chartData->rows[$currentRow] = $newRow;
                        $currentRow++;
                    }
                    //
                    $currentDate = $date;
                    $newRow = new Row;
                    $dateVal = new RowVal;
                    //преобразуем полученное значение (чтоб получить формат "год-месяц-дата" для колонок с датой) 
                    switch ($displayType) {
                        case "weeks":
                            $dateVal->v = getWeekLastDay($date);
                            break;
                        case "months":
                            $dateVal->v = getMonthLastDay($date);
                            break;
                        case "days":
                        case "range":
                            $dateVal->v = $date;
                            break;
                    }
                    $newRow->c[0] = $dateVal;
                    $curSource = 1;
                }
                $sourceVal = new RowVal;
                $sourceVal->v = $rowData;
                $sourceVal->f = $rowData;
                $newRow->c[$curSource] = $sourceVal;
                $curSource++;
            }
            //добавляем последнюю строку
            $chartData->rows[$currentRow] = $newRow;
            break;
    }
    mysql_free_result($result);

    return $chartData;
}

?>