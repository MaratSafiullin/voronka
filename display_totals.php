<?php
header("Content-Type: text/html; charset=utf-8");
//проверяем, что выполняется из сессии (т.е. не вызвано левым способом)
include("check_session.php");
//считываем переданные параметры
if (isset($_POST['project_id'])){
	$projectID = $_POST['project_id'];
}
if (isset($_POST['display_type'])){
	$displayType = $_POST['display_type'];
}
if (isset($_POST['process_date'])){
	$processDate = $_POST['process_date'];
}
if (isset($_POST['source_type_id'])){
	$sourceTypeID = $_POST['source_type_id'];
}
if (isset($_POST['show_from_date'])){
	$showFromDate = $_POST['show_from_date'];
}
if (isset($_POST['show_to_date'])){
	$showToDate = $_POST['show_to_date'];
}

include ("db_open.php");
//Защита от SQL инъекций
$projectID = (int)$projectID;
$sourceTypeID = (int)$sourceTypeID;
if(get_magic_quotes_gpc()==1){
	$displayType=stripslashes(trim($displayType));
	$processDate=stripslashes(trim($processDate));
	$showFromDate=stripslashes(trim($showFromDate));
	$showToDate=stripslashes(trim($showToDate));
} else {
	$displayType=trim($displayType);
	$processDate=trim($processDate);
	$showFromDate=trim($showFromDate);
	$showToDate=trim($showToDate);
}
$displayType=mysql_real_escape_string($displayType);
$processDate=mysql_real_escape_string($processDate);
$showFromDate=mysql_real_escape_string($showFromDate);
$showToDate=mysql_real_escape_string($showToDate);
$displayType = strip_tags($displayType);
$processDate = strip_tags($processDate);
$showFromDate = strip_tags($showFromDate);
$showToDate = strip_tags($showToDate);

//Если вдруг номер проекта не соответствует пользователю (кто-то пытается хакнуть), завершаем выполнение
include("check_project.php");

//*******************************************
//Главная исполняемая часть начинается отсюда
//*******************************************

switch($displayType){
	case "week":
		$date = strtotime($processDate);
		//находим последний день недели, на которую приходится указанная дата
		$weekDay = (integer) date("w" ,$date);
		if ($weekDay == 0) $weekDay = 7;
		$date = $date + 3600 * 24 * (7 - $weekDay) + 3600 * 12; //Че за херня?? почему нужно прибавлять полсуток??
		$startDate = date('Y-m-d', $date - 3600 * 24 * 6);
		$stopDate = date('Y-m-d', $date);
	break;
	case "month":
		$date = strtotime($processDate);
		//Находим первый и последний день месяца, на который пришлась указанная дата
		$monthDay = (integer) date("j", $date);
		$date = $date - 3600 * 24 * ($monthDay - 1) + 3600 * 12; //Че за херня?? почему нужно прибавлять полсуток??
		$date2 = $date + 3600 * 24 * 31;
		$monthDay = (integer) date("j", $date2);
		$date2 = $date2 - 3600 * 24 * $monthDay;
		$startDate = date('Y-m-d', $date);
		$stopDate = date('Y-m-d', $date2);
	break;
	case "quarter":
		$date = strtotime($processDate);
		//Находим первый и последний день квартала, на который пришлась указанная дата
		$month = (integer) date("m", $date);
		$year = (integer) date("Y", $date);
		if (($month >= 1) && ($month <= 3)){
			$startDate = $year . "-01-01";
			$stopDate = $year . "-03-31";
		} elseif (($month >= 4) && ($month <= 6)){
			$startDate = $year . "-04-01";
			$stopDate = $year . "-06-30";
		} elseif (($month >= 7) && ($month <= 9)){
			$startDate = $year . "-07-01";
			$stopDate = $year . "-09-30";
		} elseif (($month >= 10) && ($month <= 12)){
			$startDate = $year . "-10-01";
			$stopDate = $year . "-12-31";
		}	
	break;
	case "year":
		//Находим первый и последний день года, на который пришлась указанная дата
		$date = strtotime($processDate);
		$yearDay = (integer) date("z" ,$date);
		$date = $date - 3600 * 24 * ($yearDay) + 3600 * 12; //Че за херня?? почему нужно прибавлять полсуток??
		$startDate = date('Y-m-d', $date);
		$date = $date + 3600 * 24 * 366;
		$yearDay = (integer) date("z" ,$date);
		$date = $date - 3600 * 24 * ($yearDay + 1);
		$stopDate = date('Y-m-d', $date);
	break;
	case "7days":
		$date = strtotime($processDate);
		//
		$stopDate = date('Y-m-d', $date);
		$startDate = date('Y-m-d', $date - 3600 * 24 * 6);
	break;
	case "30days":
		$date = strtotime($processDate);
		//
		$stopDate = date('Y-m-d', $date);
		$startDate = date('Y-m-d', $date - 3600 * 24 * 29);
	break;
	case "90days":
		$date = strtotime($processDate);
		//
		$stopDate = date('Y-m-d', $date);
		$startDate = date('Y-m-d', $date - 3600 * 24 * 89);
	break;
	case "365days":
		$date = strtotime($processDate);
		//
		$stopDate = date('Y-m-d', $date);
		$startDate = date('Y-m-d', $date - 3600 * 24 * 364);
	break;
	case "range":
		$stopDate = date('Y-m-d', strtotime($showToDate));
		$startDate = date('Y-m-d', strtotime($showFromDate));
	break;
}
	$query = "SELECT SUM(displays_count), SUM(clicks_count), SUM(clicks_cost), SUM(leads_count), SUM(customers_count), SUM(income), SUM(profit) FROM sales_data 
	INNER JOIN data_sources ON data_sources.id = sales_data.data_source_id 
	INNER JOIN record_days ON record_days.id = sales_data.day_id
	INNER JOIN projects ON projects.id = record_days.project_id
	WHERE record_days.date >= '{$startDate}' AND record_days.date <= '{$stopDate}' AND projects.id = '{$projectID}' AND data_sources.type_id = {$sourceTypeID} AND data_sources.visible = '1'";
	$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
	//
	//
	if (mysql_num_rows($result) != 0) {
		$displaysCountTotal = mysql_result($result,$i,'SUM(displays_count)');
		$clicksCountTotal = mysql_result($result,$i,'SUM(clicks_count)');
		$clicksCostTotal = mysql_result($result,$i,'SUM(clicks_cost)');
		$leadsCountTotal = mysql_result($result,$i,'SUM(leads_count)');
		$customersCountTotal = mysql_result($result,$i,'SUM(customers_count)');
		$incomeTotal = mysql_result($result,$i,'SUM(income)');
		$profitTotal = mysql_result($result,$i,'SUM(profit)');
	}
	if (empty($displaysCountTotal)) $displaysCountTotal = 0;
	if (empty($clicksCountTotal)) $clicksCountTotal = 0;
	if (empty($clicksCostTotal)) $clicksCostTotal = 0;
	if (empty($leadsCountTotal)) $leadsCountTotal = 0;
	if (empty($customersCountTotal)) $customersCountTotal = 0;
	if (empty($incomeTotal)) $incomeTotal = 0;
	if (empty($profitTotal)) $profitTotal = 0;
	
	$cellIDTemp = "totals-";
?>
<!-- Тут в атрибутах данные о том что это за период времени -->
<div data-display_type="<?php echo $displayType;?>" data-start_date="<?php echo $startDate;?>" data-stop_date="<?php echo $stopDate;?>" >
	<div class="horiz-fixed-table-part">
		<table id="totals-table-fixed">
			<tbody>
				<tr>
					<td class="column1"><div id="" class="sales-cell sales-cell-column1">
						<select id="select-total-period">
							<option value="week" <?php if ($displayType == "week") echo "selected"; ?>>Текущая Неделя</option>
							<option value="month" <?php if ($displayType == "month") echo "selected"; ?>>Текущий Месяц</option>
							<option value="quarter" <?php if ($displayType == "quarter") echo "selected"; ?>>Текущий Квартал</option>
							<option value="year" <?php if ($displayType == "year") echo "selected"; ?>>Текущий Год</option>
							<option value="7days" <?php if ($displayType == "7days") echo "selected"; ?>>Последние 7 дней</option>
							<option value="30days" <?php if ($displayType == "30days") echo "selected"; ?>>Последние 30 дней</option>
							<option value="90days" <?php if ($displayType == "90days") echo "selected"; ?>>Последние 90 дней</option>
							<option value="365days" <?php if ($displayType == "365days") echo "selected"; ?>>Последние 365 дней</option>
							<option value="range" <?php if ($displayType == "range") echo "selected"; ?>>Выбранный диапазон</option>
						</select>
					</div></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class = "wrapper-floating-table-part">
		<div class="horiz-floating-table-part">
			<table id="totals-table">
				<tbody>
					<tr>
						<?php $cellID = $cellIDTemp . "displaysCount";?>
						<td class="column2" <?php echo $hiddenParam;?>><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column2"><?php echo number_format($displaysCountTotal, 0, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "CTR";?>
						<td class="column3 percents" <?php echo $hiddenParam;?>><div id="<?php echo $cellID;?>" class="sales-cell column3"><?php if ($displaysCountTotal != 0) echo number_format($clicksCountTotal / $displaysCountTotal * 100, 3, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "clicksCount";?>
						<td class="column4"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column4"><?php echo number_format($clicksCountTotal, 0, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "clicksCost";?>
						<td class="column5"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column5"><?php echo number_format($clicksCostTotal, 0, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "averageClickCost";?>
						<td class="column6"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column6"><?php if ($clicksCountTotal != 0) echo number_format($clicksCostTotal / $clicksCountTotal, 2, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "leadsCount";?>
						<td class="column7"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column7"><?php echo number_format($leadsCountTotal, 0, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "siteCV";?>
						<td class="column8 percents"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column8"><?php if ($clicksCountTotal != 0) echo number_format($leadsCountTotal / $clicksCountTotal * 100, 2, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "averageLeadCost";?>
						<td class="column9"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column9"><?php if ($leadsCountTotal != 0) echo number_format($clicksCostTotal / $leadsCountTotal, 2, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "customersCount";?>
						<td class="column10"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column10"><?php echo number_format($customersCountTotal, 0, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "salesSV";?>
						<td class="column11 percents"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column11"><?php if ($leadsCountTotal != 0) echo number_format($customersCountTotal / $leadsCountTotal * 100, 2, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "averageCustomerCost";?>
						<td class="column12"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column12"><?php if ($customersCountTotal != 0) echo number_format($clicksCostTotal / $customersCountTotal, 2, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "income";?>
						<td class="column13"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column13"><?php echo number_format($incomeTotal, 0, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "profit";?>
						<td class="column14"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column14"><?php echo number_format($profitTotal, 0, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "pure-profit";?>
						<td class="column16"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column16"><?php echo number_format($profitTotal - $clicksCostTotal, 0, ".", "'");?></div></td>
						
						<?php $cellID = $cellIDTemp . "ROI";?>
						<td class="column15 percents"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column15"><?php if ($clicksCostTotal != 0) echo number_format(($profitTotal - $clicksCostTotal) / $clicksCostTotal * 100, 2, ".", "'");?></div></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>	
</div>
<?php
	mysql_free_result($result);
?>