<?php
ini_set('session.save_path', $_SERVER['DOCUMENT_ROOT'] . '/sessions/');
session_start();
//���� �� ���������� (� ��������� ������) ������ �� ������, ���������� �� �������
if (trim($_SESSION['email']) == ""){
	header("Location:index.php");
	exit;
}
//����� ��������� ������
setcookie(session_name(), session_id(), time()-60*60*24);
//���� ���� ������� "��������� ����" ���������� ��� ������, ������� ����
//�� ������ ������� ����, ����� ���� �������� �� ������� ������ ��� (�������� ���-�� ��������� ��� ����������� � �����) 
if ($_SESSION['save'] == "yes"){
	setcookie("user_data", "", time()-60*60*24);
}
session_unset();
session_destroy();
header("Location:index.php");
exit;
?>