<?php
header("Content-Type: text/html; charset=utf-8");

include ("db_open.php");

//*******************************************
//Статистика по сайту 
//*******************************************

$RETENTION_15 = 15;
$RETENTION_30 = 30;
$RETENTION_45 = 45;

//задаем диапазон дат для статистики
$DAYS_BACK = 120;
$MONTHS_NUM = $DAYS_BACK / 30;
$toDateSQL = date('Y-m-d', time());
$fromDateSQL = date('Y-m-d', time() - 3600 * 24 * $DAYS_BACK);


//Находим всех неанонимных пользователей (тех у кого есть email)
$query = "SELECT COUNT(id) FROM users WHERE email <> ''";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
echo "Зарегистрированных пользователей всего: <b>" . mysql_result($result, 0, 'COUNT(id)') . "</b>\n<br><br>\n";
mysql_free_result($result);

//Находим все проекты неанонимных пользователей (тех у кого есть email)
$query = "SELECT COUNT(projects.id) as count FROM projects
INNER JOIN users ON users.id = projects.user_id
WHERE users.email <> ''";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
echo "Проектов у зарегистрированных пользователей всего: <b>" . mysql_result($result, 0, 'count') . "</b>\n<br><br>\n";
mysql_free_result($result);

//Находим все записи (дни) в проектах неанонимных пользователей (тех у кого есть email)
$query = "SELECT COUNT(record_days.id) as count FROM record_days
INNER JOIN projects ON projects.id = record_days.project_id
INNER JOIN users ON users.id = projects.user_id
WHERE users.email <> ''";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
echo "Записей (дней) у зарегистрированных пользователей всего: <b>" . mysql_result($result, 0, 'count') . "</b>\n<br><br>\n";
mysql_free_result($result);

echo "--------------------------------------------------------------------------------------------------\n<br><br>\n";

echo "Статистика с <b>" . $fromDateSQL . "</b> по <b>" . $toDateSQL . "</b>:\n<br><br>\n";

//Находим среднее количество зарегистрированных в день, месяц (30 дней) 
$query = "SELECT COUNT(id) FROM users "
        . "WHERE email <> '' AND register_time >= '{$fromDateSQL}' AND register_time <= '{$toDateSQL}' ";       
////////////echo $query . "\n<br><br>\n";       
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
$count = mysql_result($result, 0, 'COUNT(id)');
echo "В среднем регистрируются в месяц: <b>" . $count / $MONTHS_NUM. "</b>\n<br>\n";
echo "В день: <b>" . $count / $DAYS_BACK . "</b>\n<br><br>\n";
mysql_free_result($result);

//Находим количество активных - заходящих на сайт не реже раза в месяц           
$query = "SELECT COUNT(id) AS count "
        . "FROM (SELECT users.id AS id, COUNT(login_log.login_time) AS count FROM users "
        . "INNER JOIN login_log ON login_log.user_id = users.id "
        . "WHERE login_log.login_time >= '{$fromDateSQL}' AND login_log.login_time <= '{$toDateSQL}' "
        . "GROUP BY users.id) AS logins "
        . "WHERE count >= '{$MONTHS_NUM}'";
////////////echo $query . "\n<br><br>\n";       
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
$count = mysql_result($result, 0, 'count');
echo "Заходят не реже раза в месяц (активные): <b>" . $count . "</b>\n<br><br>\n";
mysql_free_result($result);

//Находим сколько раз в месяц в среднем заходят активные пользователи        
$query = "SELECT AVG(count) AS count "
        . "FROM (SELECT users.id AS id, COUNT(login_log.login_time) AS count FROM users "
        . "INNER JOIN login_log ON login_log.user_id = users.id "
        . "WHERE login_log.login_time >= '{$fromDateSQL}' AND login_log.login_time <= '{$toDateSQL}' "
        . "GROUP BY users.id) AS logins "
        . "WHERE count >= '{$MONTHS_NUM}'";
////////////echo $query . "\n<br><br>\n";       
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
$count = mysql_result($result, 0, 'count');
echo "Активные заходят в месяц в среднем раз: <b>" . $count / $MONTHS_NUM . "</b>\n<br><br>\n";
mysql_free_result($result);

//Находим Retention 45 дней, 30 дней, 15 дней
$query = "SELECT COUNT(id) AS count "
        . "FROM (SELECT users.id AS id, users.register_time AS reg_time, MAX(login_log.login_time) AS log_time "
        . "FROM users "
        . "INNER JOIN login_log ON login_log.user_id = users.id "
        . "WHERE users.register_time >= '{$fromDateSQL}' AND users.register_time <= '{$toDateSQL}' "
        . "GROUP BY users.id) AS logins "
        . "WHERE DATEDIFF(log_time, reg_time) > '{$RETENTION_45}'";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
$count = mysql_result($result, 0, 'count');
//for ($i=0; $i<mysql_num_rows($result); $i++){
//    echo mysql_result($result, $i, 'count') . "\n<br>\n";
//}
echo "Reteniton {$RETENTION_45} дней (возвращаются через {$RETENTION_45} дней после регистрации и позже): <b>" . $count . "</b>\n<br>\n";
mysql_free_result($result);
//
$query = "SELECT COUNT(id) AS count "
        . "FROM (SELECT users.id AS id, users.register_time AS reg_time, MAX(login_log.login_time) AS log_time "
        . "FROM users "
        . "INNER JOIN login_log ON login_log.user_id = users.id "
        . "WHERE users.register_time >= '{$fromDateSQL}' AND users.register_time <= '{$toDateSQL}' "
        . "GROUP BY users.id) AS logins "
        . "WHERE DATEDIFF(log_time, reg_time) > '{$RETENTION_30}'";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
$count = mysql_result($result, 0, 'count');
echo "Reteniton {$RETENTION_30} дней (возвращаются через {$RETENTION_30} дней после регистрации и позже): <b>" . $count . "</b>\n<br>\n";
mysql_free_result($result);
//
$query = "SELECT COUNT(id) AS count "
        . "FROM (SELECT users.id AS id, users.register_time AS reg_time, MAX(login_log.login_time) AS log_time "
        . "FROM users "
        . "INNER JOIN login_log ON login_log.user_id = users.id "
        . "WHERE users.register_time >= '{$fromDateSQL}' AND users.register_time <= '{$toDateSQL}' "
        . "GROUP BY users.id) AS logins "
        . "WHERE DATEDIFF(log_time, reg_time) > '{$RETENTION_15}'";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
$count = mysql_result($result, 0, 'count');
echo "Reteniton {$RETENTION_15} дней (возвращаются через {$RETENTION_15} дней после регистрации и позже): <b>" . $count . "</b>\n<br><br>\n";
mysql_free_result($result);

//Находим сколько людей зарегистировалось
$query = "SELECT COUNT(id) AS count "
        . "FROM users "
        . "WHERE users.register_time >= '{$fromDateSQL}' AND users.register_time <= '{$toDateSQL}' ";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
$count = mysql_result($result, 0, 'count');
echo "Зарегистрировалось пользователей за период: <b>" . $count . "</b>\n<br>\n";
mysql_free_result($result);
//Находим сколько людей заводит запись (день) после регистрации
$query = "SELECT COUNT(id) AS count "
        . "FROM (SELECT users.id AS id, COUNT(record_days.id) AS count "
        . "FROM users "
        . "INNER JOIN projects ON projects.user_id = users.id "
        . "INNER JOIN record_days ON record_days.project_id = projects.id "
        . "WHERE users.register_time >= '{$fromDateSQL}' AND users.register_time <= '{$toDateSQL}' "
        . "GROUP BY users.id) AS days ";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
$count = mysql_result($result, 0, 'count');
echo "Завели запись (день): <b>" . $count . "</b>\n<br>\n";
mysql_free_result($result);
//Находим сколько людей завели больше 3 записей (дней)
$query = "SELECT COUNT(id) AS count "
        . "FROM (SELECT users.id AS id, COUNT(record_days.id) AS count "
        . "FROM users "
        . "INNER JOIN projects ON projects.user_id = users.id "
        . "INNER JOIN record_days ON record_days.project_id = projects.id "
        . "WHERE users.register_time >= '{$fromDateSQL}' AND users.register_time <= '{$toDateSQL}' "
        . "GROUP BY users.id) AS days "
        . "WHERE count > '3'";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
$count = mysql_result($result, 0, 'count');
echo "Завели больше 3 записей: <b>" . $count . "</b>\n<br>\n";
mysql_free_result($result);
//Находим сколько людей завели больше 10 записей (дней)
$query = "SELECT COUNT(id) AS count "
        . "FROM (SELECT users.id AS id, COUNT(record_days.id) AS count "
        . "FROM users "
        . "INNER JOIN projects ON projects.user_id = users.id "
        . "INNER JOIN record_days ON record_days.project_id = projects.id "
        . "WHERE users.register_time >= '{$fromDateSQL}' AND users.register_time <= '{$toDateSQL}' "
        . "GROUP BY users.id) AS days "
        . "WHERE count > '10'";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
$count = mysql_result($result, 0, 'count');
echo "Завели больше 10 записей: <b>" . $count . "</b>\n<br>\n";
mysql_free_result($result);
//Находим сколько людей завели больше 30 записей (дней)
$query = "SELECT COUNT(id) AS count "
        . "FROM (SELECT users.id AS id, COUNT(record_days.id) AS count "
        . "FROM users "
        . "INNER JOIN projects ON projects.user_id = users.id "
        . "INNER JOIN record_days ON record_days.project_id = projects.id "
        . "WHERE users.register_time >= '{$fromDateSQL}' AND users.register_time <= '{$toDateSQL}' "
        . "GROUP BY users.id) AS days "
        . "WHERE count > '30'";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
$count = mysql_result($result, 0, 'count');
echo "Завели больше 30 записей: <b>" . $count . "</b>\n<br>\n";
mysql_free_result($result);

include ("db_close.php");
?>