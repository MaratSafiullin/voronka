<?php
header("Content-Type: text/html; charset=utf-8");
include ("check_login.php");
//если уже залогинен отправляем на главную
if (trim($_SESSION['email']) != "")
{
	header("Location:index.php");
	exit;
}
//считываем переданные параметры
if (isset($_POST['action'])) 
{ 
	$action = $_POST['action']; 
}
if (isset($_POST['email'])) 
{ 
	$email = $_POST['email']; 
}
if (isset($_POST['password'])) 
{ 
	$password=$_POST['password']; 
}
if (isset($_POST['save'])) 
{ 
	$save=$_POST['save']; 
}

include ("db_open.php");
//если пытаемся залогиниться (т.е. страница вызвала сама себя с параметрами)
if ($action == "login")
{	
	//Защита от SQL инъекций
	if(get_magic_quotes_gpc()==1){
		$email=stripslashes(trim($email));
		$password=stripslashes(trim($password));
	} else {
		$email=trim($email);
		$password=trim($password);
	}
	$email=mysql_real_escape_string($email);
	$password=mysql_real_escape_string($password);
	$email = strip_tags($email);
	
	if ($email == '') 
	{ 
		unset($email);
	}
	if ($password =='') 
	{ 
		unset($password);
	}

	$err = 0;
	//если пустой майл то ошибка и не пытаемся лезть в базу
	if (empty($email))
	{
		$err = 2;
	}
	//иначе
	else
	{
		//проверяем по базе
		$query = "SELECT * FROM users WHERE email='{$email}' AND pass_hash=MD5('{$password}')";
		$result = mysql_query($query); 
		$myrow = mysql_fetch_array($result);
		mysql_free_result($result);
		//если нет соответствия, отмечаем ошибку логина
		if (empty($myrow['id']))
		{
			$err = 1;
			include ("db_close.php");
		}
		//иначе
		else 
		{
			$passHash = $myrow['pass_hash'];
			//задаем данные сессии
			$_SESSION['email'] = $myrow['email']; 
			$_SESSION['id'] = $myrow['id'];
			//и обновляем дату последнего логина
			$now = date("Y-m-d H:i:s");
			$userID = $_SESSION['id'];
			$query = "UPDATE users SET last_login='{$now}' WHERE id='{$userID}'";
			mysql_query($query);
			//пишем в лог факт логина
			$query = "INSERT INTO login_log (user_id, login_time) VALUES('{$userID}', '{$now}')";
			mysql_query($query);
			//отправляем куки если была галочка "запомнить меня"
			if ($save == 1){
				$_SESSION['save'] = "yes";
				setcookie("user_data", $email."+".$passHash."+".$_SESSION['id'], time()+9999999);
			}
			//отправляем на главную
			header("Location:index.php");
			include ("db_close.php");
			exit;
		}
	}
}
//форма для логина
?>
<html>
	<head>
		<title>Вход для зарегистрированных пользователей</title>
		<?php include("html_head.php");?> 
	</head> 
	<body>
		<div class="top-fix">
			<?php include("header.php");?> 
		</div>
			
		<div class="container1">
			<div class="form1">
				<?php
				//если была ошибка при логине
				switch($err) 
				{
					case 1:
						echo "Неверный E-mail или пароль";
					break;
					case 2:
						echo "Пустой E-mail";
					break;
				}
				?>
				<h2>Войти</h2>
				<form action="sign_in.php" method="post">
					<input type='text' name = 'action' value = 'login' readonly hidden>
					<p>
					<label>Email:<br></label>
					<input class="input1" name="email" type="email" size="20" maxlength="50" value="<?php echo "{$email}" ?>">
					</p>
					<p>
					<label>Пароль:<br></label>
					<input class="input1" name="password" type="password" size="20" maxlength="20" value="<?php echo "{$password}" ?>">
					</p>
					<p>
					<input class="submit1" type="submit" name="submit" value="Войти">
					</p>
					<p>
					<label><input name="save" type="checkbox" value='1' <?php if (($save == 1)) echo "checked" ?>> Запомнить меня</label>
					</p>
					<p>
					<a href="restore_password.php" class="ref">Восстановление пароля</a> 
					</p>
					<p>
					<a href="sign_up.php">Зарегистрироваться</a> 
					</p>
				</form>
				<br>
			</div>
		</div>
			
		<div class="bottom-fix1">
			<?php include("footer.php");?>
		</div>
	</body>
</html>