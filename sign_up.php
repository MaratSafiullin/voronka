<?php
//импортируем функцию добавления проекта
include("add_project_function.php");

header("Content-Type: text/html; charset=utf-8");
//Проверка на корректность адреса почты
function Email_Validation($email)
{
	if(preg_match("/[0-9a-z_\.\-]+@[0-9a-z_\.\-]+\.[a-z]{2,4}/i", $email))
	{
		$res = true;
	}
	else
	{
		$res = false;
	}
    return $res; 
}
//

include ("check_login.php");
//если уже залогинен отправляем на главную
if (trim($_SESSION['email']) != "")
{
	header("Location:index.php");
	exit;
}
//считываем переданные параметры
if (isset($_POST['action'])) 
{ 
	$action = $_POST['action']; 
}
if (isset($_POST['login'])) 
{ 
	$login = $_POST['login']; 
}
if (isset($_POST['email'])) 
{ 
	$email=$_POST['email']; 
}
if (isset($_POST['name'])) 
{ 
	$name=$_POST['name']; 
}
if (isset($_POST['password'])) 
{ 
	$password=$_POST['password']; 
}
if (isset($_POST['save'])) 
{ 
	$save=$_POST['save']; 
}

include ("db_open.php");
//если пытаемся зарегиться (т.е. страница вызвала сама себя с параметрами)
if ($action == "sign_up")
{
	//Защита от SQL инъекций
	if(get_magic_quotes_gpc()==1){
		$login=stripslashes(trim($login));
		$password=stripslashes(trim($password));
		$name=stripslashes(trim($name));
		$email=stripslashes(trim($email));
	} else {
		$login=trim($login);
		$password=trim($password);
		$name=trim($name);
		$email=trim($email);
	}
	$login=mysql_real_escape_string($login);
	$name=mysql_real_escape_string($name);
	$email=mysql_real_escape_string($email);
	$password=mysql_real_escape_string($password);
	$login = strip_tags($login);
	$email = strip_tags($email);
	$name = strip_tags($name);
	
	if ($login == '') 
	{ 
		unset($login);
	} 
	if ($email == '') 
	{ 
		unset($email);
	}
	if ($password =='') 
	{ 
		unset($password);
	}

	$err = 0;
	//если пустая почта то ошибка и не пытаемся писать в базу
	if (empty($email))
	{
		$err = 2;
	}
	//если почта некорректная - тоже
	elseif (!Email_Validation($email))
	{
		$err = 3;
	}
	//если пустой пароль - тоже
	elseif (empty($password))
	{
		$err = 4;
	}
	//иначе
	else 
	{
		//проверяем что такого пользователя нет в базе
		$query = "SELECT id FROM users WHERE email='{$email}' ";
		$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
		$myrow = mysql_fetch_array($result);
		mysql_free_result($result);
		//если есть, то ошибка и ничего не пишем в базу
		if (!empty($myrow['id'])) 
		{
			$err = 1;
			include ("db_close.php");
		}
		//иначе
		else
		{
			$now = date("Y-m-d H:i:s");
			//если регистрируемся сразу по приходу на сайт, будем создавать новую запись в таблице
			if (empty($_SESSION['id'])) { 
				$query = "INSERT INTO users (email, name, pass_hash, register_time, last_login) 
				VALUES('{$email}', '{$name}', MD5('{$password}'), '{$now}', '{$now}')";
				$result2 = mysql_query($query) or die('Incorrect query: ' . mysql_error());
				//добавляем порядок графиков для пользователя
				if ($result2) {
					$userID = mysql_insert_id();
					$charts = array();
					$query = "SELECT number, type, title_1, title_2, title_3, title_4 FROM default_charts_order";
					$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
					for ($i=0; $i<mysql_num_rows($result); $i++){
						$charts[$i] = array();
						$charts[$i]['number'] = mysql_result($result, $i, 'number');
						$charts[$i]['type'] = mysql_result($result, $i, 'type');
						$charts[$i]['title_1'] = mysql_result($result, $i, 'title_1');
						$charts[$i]['title_2'] = mysql_result($result, $i, 'title_2');
						$charts[$i]['title_3'] = mysql_result($result, $i, 'title_3');
						$charts[$i]['title_4'] = mysql_result($result, $i, 'title_4');
					}
					mysql_free_result($result);
					//
					$query = "INSERT INTO charts_order (user_id, number, type, title_1, title_2, title_3, title_4) VALUES ";
					for ($j=0; $j<count($charts)-1; $j++){
						$query = $query . "('{$userID}', '{$charts[$j]['number']}', '{$charts[$j]['type']}', '{$charts[$j]['title_1']}', '{$charts[$j]['title_2']}', '{$charts[$j]['title_3']}', '{$charts[$j]['title_4']}'), ";
					}
					$query = $query . "('{$userID}', '{$charts[$j]['number']}', '{$charts[$j]['type']}', '{$charts[$j]['title_1']}', '{$charts[$j]['title_2']}', '{$charts[$j]['title_3']}', '{$charts[$j]['title_4']}')";
					mysql_query($query) or die('Incorrect query: ' . mysql_error());
				}
				
			} 
			//если были до этого в анонимном режиме - обновим существующую
			else {
				$userID = $_SESSION['id'];
				$query = "UPDATE users SET email='{$email}', name='{$name}', pass_hash=MD5('{$password}'), register_time='{$now}', last_login='{$now}' WHERE id='{$userID}'";
				$result2 = mysql_query($query) or die('Incorrect query: ' . mysql_error());
			}
			
			//выполняем нужный запрос
			if ($result2)
			{
				//находим нужного пользователя
				$query = "SELECT * FROM users WHERE email='{$email}'";
				$result = mysql_query($query); 
				$myrow = mysql_fetch_array($result);
				mysql_free_result($result);
				//задаем данные сессии
				$_SESSION['email']=$myrow['email'];
				$_SESSION['id'] = $myrow['id'];
				$userID = $myrow['id'];
				
				//отправляем куки если была галочка "запомнить меня"
				if ($save == 1)	{
					$_SESSION['save'] = "yes";
					setcookie("user_data", $email."+".$passHash."+".$_SESSION['id'], time()+9999999);
				} 
				//или удаляем если не было (у юзера есть теперь нормальный логин, его проекты не потеряются)
				else {
					setcookie("user_data", "", time()-60*60*24);
				}
				
				//сразу создаем первый проект если у пользователя еще нет проектов
				$query = "SELECT * FROM projects WHERE user_id='{$userID}'";
				$result = mysql_query($query); 
				$myrow = mysql_fetch_array($result);
				mysql_free_result($result);
				if (empty($myrow['id'])) {
					AddProject($userID, "Мой первый проект", "Это ваш первый проект, созданный автоматически");
				}
				//пишем в лог факт логина
				$query = "INSERT INTO login_log (user_id, login_time) VALUES('{$userID}', '{$now}')";
				mysql_query($query);
				//отправляем на главную
				header("Location:index.php");
				include ("db_close.php");
				exit;
			}
			//если не удалось создать пользователя - неизвестная ошибка
			else 
			{
				$err = 9;
				include ("db_close.php");
			}
		}
	}
}
//форма регистрации
?>
<html lang="en" class="" hola_ext_inject="disabled">
    <head>
		<title>Воронка-онлайн.рф - бесплатная воронка продаж и сервис сквозной аналитики для бизнеса</title>
		<?php include("html_head.php");?> 
		<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=PdBUB2sPgvUfGaxpZqZI/bnxzsjY82Rk5hispkAqUVT5ycH/K6JAUiTAgnOslBjKCx4/vXGbxdH8FpKDOQf7DnbCqHFn0/u45L*2lfkLf94WRIS/bVA4L2f2tITd2R21nDEPRWoQchI8ri/OSYeIqueBPzaG8IX0YGVbv7vDSd0-';</script>
	</head>
	<body style="background-color:white;">
		<div class="top-fix">
			<?php include("header.php");?> 
		</div>
			
		<div class="container1">	
			<div class="signup_fon">
				<div class="signup_offer">
				
				<h1>Бесплатная воронка продаж онлайн</h1>
				<h2> Внеси ясность в бизнес процессы: легко сравнивай все рекламные каналы,</br> отслеживай динамику жизненно важных метрик для бизнеса<h2>
				</div>
				<div class="auth_form">
				
				
				
					<?php
					//если была ошибка при регистрации
					switch($err) 
					{
						case 1:
							echo "<p>Этот E-mail уже зарегистрирован</p>";
						break;
						case 2:
							echo "<p>Пустой E-mail</p>";
						break;
						case 3:
							echo "<p>Неправильный E-mail</p>";
						break;
						case 4:
							echo "<p>Пустой пароль (плохая идея)</p>";
						break;
						case 9:
							echo "<p>Что-то пошло не так, попробуйте еще раз</p>";
						break;
					}	
					?>
					
					<form action="sign_up.php" method="post">
						<table>
						<tbody>
							<tr>
								<td>
									<input type='text' name = 'action' value = 'sign_up' readonly hidden>
									
									<label>E-mail (он же логин):</label>
									<br>
									<input name="email" type="email" size="20" maxlength="50" value="<?php echo "{$email}" ?>" oninput="Change_Email(this.value)">
									<br>
									<label id="email_err_mess">&nbsp</label>
									
								</td>
								<td>
									
									<label>Придумай пароль:</label>
									<br>
									<input name="password" type="password" size="20" maxlength="20" value="<?php echo "{$password}" ?>">
									
								</td>
								<td>
									
									<input type="submit" name="submit" id="submit_btn" value="Зарегистрируйся сейчас"></br>
									
								</td>
							
														
									
								
							</tr>		
								
						</tbody>
						</table>
					</form>
				</div>
			</div>
			<div class="signup_trigger">
				<table>
					<tbody>
						<tr class="trigger_top">
							<td>Проще и удобнее </br>чем Excel</td>
							<td>Дополняет Я.Метрику </br>и G.Analytics</td>
							<td>Быстрее </br>и нагляднее</td>
							<td>Бесплатно </br>навсегда</td>
						</tr>
						<tr class="trigger_text">
							<td>
								
								- Срезы по периодам</br>
								- Все формулы уже введены</br>
								- Онлайн работа в команде

							</td>
							<td>Сравнивайте </br>все источники рекламы, </br>как онлайн, так и оффлайн
							</td>
							<td>- Быстрое заполнение</br>
								- Больше данных на экране</br>
								- Легко сравнивать периоды
								
							</td>
							<td>- Полный функционал для ведения воронки продаж </br>
							- Неограниченное число проектов
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
			
		<div class="bottom-fix1">
			<?php include("footer.php");?>
		</div>
    </body>
</html>