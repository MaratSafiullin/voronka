<?php
//функция для получения дат первого и последнего дня недели по ее номеру (в году) и году
function getDatesByWeek($_week_number, $_year = null) {
	$year = $_year ? $_year : date('Y');
	$week_number = sprintf('%02d', $_week_number);
	$date_base = strtotime($year . 'W' . $week_number . '1 00:00:00');
	$date_limit = strtotime($year . 'W' . $week_number . '7 23:59:59');
	return array($date_base, $date_limit);
}

//функция для получения обозначения недели по ее году-номеру
function getWeekTitle($yearWeek){
	$year = substr($yearWeek, 0, 4);
	$week = substr($yearWeek, 5, 2);
	$resArr = getDatesByWeek($week, $year);
	return date('Y.m.d', $resArr[0]) . " - " . date('Y.m.d', $resArr[1]);
}

header("Content-Type: text/html; charset=utf-8");
//проверяем, что выполняется из сессии (т.е. не вызвано левым способом)
include("check_session.php");
//считываем переданные параметры
if (isset($_POST['project_id'])){
	$projectID = $_POST['project_id'];
}
if (isset($_POST['from_date'])){
	$fromDate = $_POST['from_date'];
}
if (isset($_POST['to_date'])){
	$toDate = $_POST['to_date'];
}
if (isset($_POST['display_type'])){
	$displayType = $_POST['display_type'];
}

include ("db_open.php");
//Защита от SQL инъекций
$projectID = (int)$projectID;
if(get_magic_quotes_gpc()==1){
	$fromDate=stripslashes(trim($fromDate));
	$toDate=stripslashes(trim($toDate));
} else {
	$fromDate=trim($fromDate);
	$toDate=trim($toDate);
}
$fromDate = mysql_real_escape_string($fromDate);
$toDate = mysql_real_escape_string($toDate);
$fromDate = strip_tags($fromDate);
$toDate = strip_tags($toDate);


$fromDateSQL = date('Y-m-d', strtotime($fromDate));
$toDateSQL = date('Y-m-d', strtotime($toDate));

//Если вдруг номер проекта не соответствует пользователю (кто-то пытается хакнуть), завершаем выполнение
include("check_project.php");

//*******************************************
//Главная исполняемая часть начинается отсюда
//*******************************************

//Выбираем все данные (по всем источникам) в заданном диапазоне

//задаем группировку в зависимости от периода
$dateFormat = "DATE_FORMAT(record_days.date,'%d.%m.%Y')"; //по умолчанию
switch($displayType){
	case "days":
		$dateFormat = "DATE_FORMAT(record_days.date,'%d.%m.%Y')";
	break;
	case "weeks":
		$dateFormat = "DATE_FORMAT(record_days.date,'%x-%v')";
	break;
	case "months":
		$dateFormat = "DATE_FORMAT(record_days.date,'%x-%m')";
	break;
	case "range":
		$dateFormat = "'{$fromDate} - {$toDate}'";
	break;
}

//запрос с группировкой, суммами показателей, конкатенацией строк комментариев
$query = "SELECT {$dateFormat}  AS date, data_sources.name AS name, data_sources.id AS src_id, SUM(sales_data.displays_count) AS displays_count, SUM(sales_data.clicks_count) AS clicks_count, SUM(sales_data.clicks_cost) AS clicks_cost, SUM(sales_data.leads_count) AS leads_count, SUM(sales_data.customers_count) AS customers_count, SUM(sales_data.income) AS income, SUM(sales_data.profit) AS profit, GROUP_CONCAT(record_days.comment SEPARATOR ', ') AS comment
FROM sales_data
INNER JOIN record_days ON record_days.id = sales_data.day_id
INNER JOIN data_sources ON data_sources.id = sales_data.data_source_id
WHERE record_days.project_id = '{$projectID}' AND data_sources.project_id = '{$projectID}' AND
record_days.date >= '{$fromDateSQL}' AND record_days.date <= '{$toDateSQL}'
GROUP BY {$dateFormat}, name, src_id
ORDER BY date, name";
$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
		
//выводим данные в виде таблицы
?>
<table>
	<thead>
		<tr>
			<th>Дата</th>
			<th>Источник</th>
			<th>Просмотры(тираж)</th>
			<th>CV0</th>
			<th>Лид0</th>
			<th>Бюджет</th>
			<th>Цена Лида0</th>
			<th>Заявки(лиды)</th>
			<th>CV1</th>
			<th>Цена лида</th>
			<th>Клиенты</th>
			<th>CV2(продажи)</th>
			<th>Цена клиента</th>
			<th>Выручка</th>
			<th>Прибыль(без рекламы)</th>
			<th>Прибыль чистая</th>
			<th>ROI</th>
			<th>Комментарий(даты)</th>
		</tr>
	</thead>
	<tbody>
<?php
$displaysCountTotal = 0;
$clicksCountTotal = 0;
$clicksCostTotal = 0;
$leadsCountTotal = 0;
$customersCountTotal = 0;
$incomeTotal = 0;
$profitTotal = 0;
$pureProfitTotal = 0;
for ($i=0; $i<mysql_num_rows($result); $i++){
	$date = mysql_result($result,$i,'date');
	if ($displayType == "weeks") $date = getWeekTitle($date);
	$source = mysql_result($result,$i,'name');
	$displaysCount = mysql_result($result,$i,'displays_count');
	$displaysCountTotal = $displaysCountTotal + $displaysCount;
	$clicksCount = mysql_result($result,$i,'clicks_count');
	$clicksCountTotal = $clicksCountTotal + $clicksCount;
	if ($displaysCount != 0){
		$cv0 = number_format($clicksCount / $displaysCount * 100, 2)."%";
	} else {
		$cv0 = "";
	}
	$clicksCost = mysql_result($result,$i,'clicks_cost');	
	$clicksCostTotal = $clicksCostTotal + $clicksCost;
	if ($clicksCount != 0){
		$singleClickCost = number_format($clicksCost / $clicksCount, 2);
	} else {
		$singleClickCost = "";
	}
	$leadsCount = mysql_result($result,$i,'leads_count');
	$leadsCountTotal = $leadsCountTotal + $leadsCount;
	if ($clicksCount != 0){
		$cv1 = number_format($leadsCount / $clicksCount * 100, 2)."%";
	} else {
		$cv1 = "";
	}
	if ($leadsCount != 0){
		$singleLeadCost = number_format($clicksCost / $leadsCount, 2);
	} else {
		$singleLeadCost = "";
	}
	$customersCount = mysql_result($result,$i,'customers_count');
	$customersCountTotal = $customersCountTotal + $customersCount;
	if ($leadsCount != 0){
		$cv2 = number_format($customersCount / $leadsCount * 100, 2)."%";
	} else {
		$cv2 = "";
	}
	if ($customersCount != 0){
		$singleCustomerCost = number_format($clicksCost / $customersCount, 2);
	} else {
		$singleCustomerCost = "";
	}
	$income = mysql_result($result,$i,'income');
	$incomeTotal = $incomeTotal + $income;
	$profit = mysql_result($result,$i,'profit');
	$profitTotal = $profitTotal + $profit;
	$pureProfit = $profit - $clicksCost;
	if ($clicksCost != 0){
		$roi = number_format($pureProfit / $clicksCost * 100, 2)."%";
	} else {
		$roi = "";
	}
	$comment = mysql_result($result,$i,'comment');
?>
		<tr>
			<td><?php echo $date;?></td>
			<td><?php echo $source;?></td>
			<td><?php echo $displaysCount;?></td>
			<td><?php echo $cv0;?></td>
			<td><?php echo $clicksCount;?></td>
			<td><?php echo $clicksCost;?></td>
			<td><?php echo $singleClickCost;?></td>
			<td><?php echo $leadsCount;?></td>
			<td><?php echo $cv1;?></td>
			<td><?php echo $singleLeadCost;?></td>
			<td><?php echo $customersCount;?></td>
			<td><?php echo $cv2;?></td>
			<td><?php echo $singleCustomerCost;?></td>
			<td><?php echo $income;?></td>
			<td><?php echo $profit;?></td>
			<td><?php echo $pureProfit;?></td>
			<td><?php echo $roi;?></td>
			<td><?php echo $comment;?></td>
		</tr>
<?php
}
if ($displaysCountTotal != 0){
	$cv0Total = number_format($clicksCountTotal / $displaysCountTotal * 100, 2)."%";
} else {
	$cv0Total = "";
}
if ($clicksCountTotal != 0){
	$singleClickCostTotal = number_format($clicksCostTotal / $clicksCountTotal, 2);
} else {
	$singleClickCostTotal = "";
}
if ($clicksCountTotal != 0){
	$cv1Total = number_format($leadsCountTotal / $clicksCountTotal * 100, 2)."%";
} else {
	$cv1Total = "";
}
if ($leadsCountTotal != 0){
	$singleLeadCostTotal = number_format($clicksCostTotal / $leadsCountTotal, 2);
} else {
	$singleLeadCostTotal = "";
}
if ($leadsCountTotal != 0){
	$cv2Total = number_format($customersCountTotal / $leadsCountTotal * 100, 2)."%";
} else {
	$cv2Total = "";
}
if ($customersCountTotal != 0){
	$singleCustomerCostTotal = number_format($clicksCostTotal / $customersCountTotal, 2);
} else {
	$singleCustomerCostTotal = "";
}
$pureProfitTotal = $profitTotal - $clicksCostTotal;
if ($clicksCostTotal != 0){
	$roiTotal = number_format($pureProfitTotal / $clicksCostTotal * 100, 2)."%";
} else {
	$roiTotal = "";
}
?>
	</tbody>
	<thead>
		<tr>
			<th>Итого</th>
			<th>Все</th>
			<th><?php echo $displaysCountTotal;?></th>
			<th><?php echo $cv0Total;?></th>
			<th><?php echo $clicksCountTotal;?></th>
			<th><?php echo $clicksCostTotal;?></th>
			<th><?php echo $singleClickCostTotal;?></th>
			<th><?php echo $leadsCountTotal;?></th>
			<th><?php echo $cv1Total;?></th>
			<th><?php echo $singleLeadCostTotal;?></th>
			<th><?php echo $customersCountTotal;?></th>
			<th><?php echo $cv2Total;?></th>
			<th><?php echo $singleCustomerCostTotal;?></th>
			<th><?php echo $incomeTotal;?></th>
			<th><?php echo $profitTotal;?></th>
			<th><?php echo $pureProfitTotal;?></th>
			<th><?php echo $roiTotal;?></th>
			<th></th>
		</tr>
	</thead>
</table>
<?php
mysql_free_result($result);
include ("db_close.php");
?>