<?php
header("Content-Type: text/html; charset=utf-8");
include("build_comment_string.php");
//Функция выводит в ХТМЛ виде сводные результаты (суммируем данные) по разным источникам клиентов для указанного периода указанного проекта
//Реально используются периоды: день, неделя, месяц, но может быть любой
//Все выводится в таблице, каждый источкик - строка
function displayTimeUnit($startDate, $stopDate, $projectID, $sourceTypeID, $displayType) {
	$query = "SELECT data_sources.id, data_sources.name, data_sources.visible, SUM(displays_count), SUM(clicks_count), SUM(clicks_cost), SUM(leads_count), SUM(customers_count), SUM(income), SUM(profit) FROM sales_data 
	INNER JOIN data_sources ON data_sources.id = sales_data.data_source_id 
	INNER JOIN record_days ON record_days.id = sales_data.day_id
	INNER JOIN projects ON projects.id = record_days.project_id
	WHERE record_days.date >= '{$startDate}' AND record_days.date <= '{$stopDate}' AND projects.id = '{$projectID}' AND data_sources.type_id = {$sourceTypeID}
	GROUP BY data_sources.name, data_sources.id, data_sources.visible
	ORDER BY data_sources.name";
	$result = mysql_query($query) or die('Incorrect query: ' . mysql_error());
?>
	<!-- Тут в атрибутах данные о том что это за период времени -->
	<div data-display_type="<?php echo $displayType;?>" data-start_date="<?php echo $startDate;?>" data-stop_date="<?php echo $stopDate;?>" class="period-container">
		<div class="period-date">
<?php
			switch($displayType){
				case "days":
					$date = strtotime($startDate);
					echo date('d.m.Y', $date);
				break;
				case "weeks":
				case "range":
					$date1 = strtotime($startDate);
					$date2 = strtotime($stopDate);
					echo date('d.m.Y', $date1) . " - " . date('d.m.Y', $date2);
				break;
				case "months":
					$date = strtotime($startDate);
					$date = $date + 2 * 24 * 60 * 60; //////!!!!!!!!!!! че за нахер????? что не так с этим strtotime??
					$month = date("F", $date);
					switch($month) {
						case "January":
							$month = "Январь";
						break;
						case "February":
							$month = "Февраль";
						break;
						case "March":
							$month = "Март";
						break;
						case "April":
							$month = "Апрель";
						break;
						case "May":
							$month = "Май";
						break;
						case "June":
							$month = "Июнь";
						break;
						case "July":
							$month = "Июль";
						break;
						case "August":
							$month = "Август";
						break;
						case "September":
							$month = "Сентябрь";
						break;
						case "October":
							$month = "Октябрь";
						break;
						case "November":
							$month = "Ноябрь";
						break;
						case "December":
							$month = "Декабрь";
						break;
					}
					echo $month . " " . date("o", $date) . "<br>";
				break;
			}
?>
		</div>
		<div class="period-comment" id=<?php echo "period-comment-".$stopDate; ?>>
			<?php 
			$comment = biuldCommentString($startDate, $stopDate, $projectID);
			$comment = trim($comment);
			if (!empty($comment)){
				echo $comment;  
			} else {
				echo "[Добавить комментарий]";
			}
			?>
		</div>
		<div class="sales-table-container">
			<div class="sales-data-table-main-container">
				<table class="sales-data-table">
					<tbody>
<?php
	//Если есть результаты (а они ВСЕГДА должны быть по логике приложения), идем по источникам клиентов выводя данные в строки таблицы
	//Часть показателей - рассчетные величины, в базе не хранятся
	$displaysCountTotal = 0;
	$clicksCountTotal = 0;
	$clicksCostTotal = 0;
	$leadsCountTotal = 0;
	$customersCountTotal = 0;
	$incomeTotal = 0;
	$profitTotal = 0;
	
	if (mysql_num_rows($result) != 0) {
		for ($i=0; $i<mysql_num_rows($result); $i++){
			$dataSourceID = mysql_result($result,$i,'data_sources.id');
			$dataSourceName = mysql_result($result,$i,'data_sources.name');
			$dataSourceVisible = mysql_result($result,$i,'data_sources.visible');
			$displaysCount = mysql_result($result,$i,'SUM(displays_count)');
			$clicksCount = mysql_result($result,$i,'SUM(clicks_count)');
			$clicksCost = mysql_result($result,$i,'SUM(clicks_cost)');
			$leadsCount = mysql_result($result,$i,'SUM(leads_count)');
			$customersCount = mysql_result($result,$i,'SUM(customers_count)');
			$income = mysql_result($result,$i,'SUM(income)');
			$profit = mysql_result($result,$i,'SUM(profit)');
			
			//в итоги идут только те источники, которые видимы
			if ($dataSourceVisible != "0") {
				$displaysCountTotal = $displaysCountTotal + $displaysCount;
				$clicksCountTotal = $clicksCountTotal + $clicksCount;
				$clicksCostTotal = $clicksCostTotal + $clicksCost;
				$leadsCountTotal = $leadsCountTotal + $leadsCount;
				$customersCountTotal = $customersCountTotal + $customersCount;
				$incomeTotal = $incomeTotal + $income;
				$profitTotal = $profitTotal + $profit;
			}
			
			//скрываем ненужные колонки
			/* if (($sourceTypeID == 2) || ($sourceTypeID == 3)){
				$hiddenParam = "class='hidden'";
			} else {
				$hiddenParam = "";
			} */
			$cellIDTemp = "{$stopDate}-{$dataSourceID}-";
?>
			<!-- Тут на элементах висят атрибуты, которые позволяют найти соответствующие записи в базе данных:
			ID источника клиентов(data_source_id), имена столбцов из которых взяты данные для инпутов -->
			<tr data-data_source_id="<?php echo $dataSourceID;?>" <?php if ($dataSourceVisible == "0") echo "class='hidden'"?>>
				<?php $cellID = $cellIDTemp . "dataSourceName";?>
				<td class="column1"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column1"><?php echo $dataSourceName;?></div></td>
				
				<?php $cellID = $cellIDTemp . "displaysCount";?>
				<td <?php echo $hiddenParam;?> class="column2"><div class="sales-cell sales-cell-column2"><input id="<?php echo $cellID;?>" class="sales-input" name="displays_count" type="text" size="12" maxlength="12" value="<?php echo number_format($displaysCount, 0, ".", "'");?>"></div></td>
				
				<?php $cellID = $cellIDTemp . "CTR";?>
				<td <?php echo $hiddenParam;?> class="column3 percents"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column3"><?php if ($displaysCount != 0) echo number_format($clicksCount / $displaysCount * 100, 3, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "clicksCount";?>
				<td class="column4"><div class="sales-cell sales-cell-column4"><input id="<?php echo $cellID;?>" class="sales-input" name="clicks_count" type="text" size="12" maxlength="12" value="<?php echo number_format($clicksCount, 0, ".", "'");?>"></div></td>
				
				<?php $cellID = $cellIDTemp . "clicksCost";?>
				<td class="column5"><div class="sales-cell sales-cell-column5"><input id="<?php echo $cellID;?>" class="sales-input" name="clicks_cost" type="text" size="12" maxlength="12" value="<?php echo number_format($clicksCost, 0, ".", "'");?>"></div></td>
				
				<?php $cellID = $cellIDTemp . "averageClickCost";?>
				<td class="column6"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column6"><?php if ($clicksCount != 0) echo number_format($clicksCost / $clicksCount, 2, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "leadsCount";?>
				<td class="column7"><div class="sales-cell sales-cell-column7"><input id="<?php echo $cellID;?>" class="sales-input" name="leads_count" type="text" size="12" maxlength="12" value="<?php echo number_format($leadsCount, 0, ".", "'");?>"></div></td>
				
				<?php $cellID = $cellIDTemp . "siteCV";?>
				<td class="column8 percents"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column8"><?php if ($clicksCount != 0) echo number_format($leadsCount / $clicksCount * 100, 2, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "averageLeadCost";?>
				<td class="column9"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column9"><?php if ($leadsCount != 0) echo number_format($clicksCost / $leadsCount, 2, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "customersCount";?>
				<td class="column10"><div class="sales-cell sales-cell-column10"><input id="<?php echo $cellID;?>" class="sales-input" name="customers_count" type="text" size="12" maxlength="12" value="<?php echo number_format($customersCount, 0, ".", "'");?>"></div></td>
				
				<?php $cellID = $cellIDTemp . "salesSV";?>
				<td class="column11 percents"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column11"><?php if ($leadsCount != 0) echo number_format($customersCount / $leadsCount * 100, 2, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "averageCustomerCost";?>
				<td class="column12"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column12"><?php if ($customersCount != 0) echo number_format($clicksCost / $customersCount, 2, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "income";?>
				<td class="column13"><div class="sales-cell sales-cell-column13"><input id="<?php echo $cellID;?>" class="sales-input" name="income" type="text" size="12" maxlength="20" value="<?php echo number_format($income, 0, ".", "'");?>"></div></td>
				
				<?php $cellID = $cellIDTemp . "profit";?>
				<td class="column14"><div class="sales-cell sales-cell-column14"><input id="<?php echo $cellID;?>" class="sales-input" name="profit" type="text" size="12" maxlength="12" value="<?php echo number_format($profit, 0, ".", "'");?>"></div></td>
				
				<?php $cellID = $cellIDTemp . "netprofit";?>
				<td class="column16"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column16"><?php echo number_format($profit - $clicksCost, 0, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "ROI";?>
				<td class="column15 percents"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column15"><?php if ($clicksCost != 0) echo number_format(($profit - $clicksCost) / $clicksCost * 100, 2, ".", "'");?></div></td>
			</tr>
<?php
		}
//Итоги
			$cellIDTemp = "{$stopDate}-total-";
			switch($displayType){
				case "days":
					$totalsName = "Итого за день:";
				break;
				case "weeks":
					$totalsName = "Итого за неделю:";
				break;
				case "months":
					$totalsName = "Итого за месяц:";
				break;
				case "range":
					$totalsName = "Итого за диапазон:";
				break;
			}
?>
			<tr data-data_source_id="total" class="sales-data-totals">
				<?php $cellID = $cellIDTemp . "dataSourceName";?>
				<td class="column1"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column1"><?php echo $totalsName;?></div></td>
				
				<?php $cellID = $cellIDTemp . "displaysCount";?>
				<td <?php echo $hiddenParam;?> class="column2"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column2"><?php echo number_format($displaysCountTotal, 0, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "CTR";?>
				<td <?php echo $hiddenParam;?> class="column3 percents"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column3"><?php if ($displaysCountTotal != 0) echo number_format($clicksCountTotal / $displaysCountTotal * 100, 3, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "clicksCount";?>
				<td class="column4"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column4"><?php echo number_format($clicksCountTotal, 0, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "clicksCost";?>
				<td class="column5"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column5"><?php echo number_format($clicksCostTotal, 0, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "averageClickCost";?>
				<td class="column6"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column6"><?php if ($clicksCountTotal != 0) echo number_format($clicksCostTotal / $clicksCountTotal, 2, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "leadsCount";?>
				<td class="column7"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column7"><?php echo number_format($leadsCountTotal, 0, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "siteCV";?>
				<td class="column8 percents"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column8"><?php if ($clicksCountTotal != 0) echo number_format($leadsCountTotal / $clicksCountTotal * 100, 2, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "averageLeadCost";?>
				<td class="column9"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column9"><?php if ($leadsCountTotal != 0) echo number_format($clicksCostTotal / $leadsCountTotal, 2, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "customersCount";?>
				<td class="column10"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column10"><?php echo number_format($customersCountTotal, 0, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "salesSV";?>
				<td class="column11 percents"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column11"><?php if ($leadsCountTotal != 0) echo number_format($customersCountTotal / $leadsCountTotal * 100, 2, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "averageCustomerCost";?>
				<td class="column12"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column12"><?php if ($customersCountTotal != 0) echo number_format($clicksCostTotal / $customersCountTotal, 2, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "income";?>
				<td class="column13"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column13"><?php echo number_format($incomeTotal, 0, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "profit";?>
				<td class="column14"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column14"><?php echo number_format($profitTotal, 0, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "netprofit";?>
				<td class="column16"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column16"><?php echo number_format($profitTotal - $clicksCostTotal, 0, ".", "'");?></div></td>
				
				<?php $cellID = $cellIDTemp . "ROI";?>
				<td class="column15 percents"><div id="<?php echo $cellID;?>" class="sales-cell sales-cell-column15"><?php if ($clicksCostTotal != 0) echo number_format(($profitTotal - $clicksCostTotal) / $clicksCostTotal * 100, 2, ".", "'");?></div></td>
			</tr>
<?php
	}
?>
					</tbody>
				</table>
			</div>
		</div>	
	</div>
<?php
	mysql_free_result($result);
}
?>