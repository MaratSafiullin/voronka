<?php
header("Content-Type: text/html; charset=utf-8");
include ("check_login.php");
//если пользователь не залогинен, отправляем на страницу предлагающую авторизацию
if (empty($_SESSION['email'])) {
    header("Location:sign_up.php");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en" class="" hola_ext_inject="disabled">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Воронка-онлайн.рф - бесплатная воронка продаж и сервис сквозной аналитики для бизнеса</title>

        <link rel="stylesheet" type="text/css" media="all" href="css/voronka.css">
        <link rel="stylesheet" type="text/css" media="all" href="css/jquery-ui.css">
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
        <script type="text/javascript" src="script/jquery-2.1.4.js" charset="utf-8"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript" src="script/jquery-ui.js" charset="utf-8"></script>
        <script type="text/javascript" src="script/jquery.ui.datepicker-ru.js" charset="utf-8"></script>
        <script type="text/javascript" src="script/jquery.easing.1.3.js"  charset="utf-8"></script>
        <script type="text/javascript" src="script/voronka.js" charset="utf-8"></script>
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?105"></script>
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?121"></script>
        <script type="text/javascript">
            VK.init({apiId: 5172002, onlyWidgets: true});
        </script>
    </head>

    <body>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function () {
                    try {
                        w.yaCounter34375260 = new Ya.Metrika({
                            id: 34375260,
                            clickmap: true,
                            trackLinks: true,
                            accurateTrackBounce: true,
                            webvisor: true
                        });
                    } catch (e) {
                    }
                });

                var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () {
                            n.parentNode.insertBefore(s, n);
                        };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else {
                    f();
                }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/34375260" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5";
                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>

        <div class="top-fix">
            <div class="header_box">
                <div id="header_box">
                    <div class="header">
                        <div class="logo">
                            <a href="index.php"><img src="img/logo.png"></a>
                        </div>
                        <div class="site-name">
                            <h1>
                                Воронка-онлайн.рф
                            </h1>
                            <p>
                                Бесплатный сервис  для ведения воронки продаж и сквозной аналитики
                            </p>

                        </div>
                        <div id="top-navbar-user">
                            <a class="ref">Пользователей: <?php include ("get_users_count.php"); ?></a>
                            <a href="#" class="ref" id="export-button">Экспорт</a>
                            <a href="https://www.youtube.com/watch?v=seEM7V7INvs" target="_blank" class="ref">Как пользоваться?</a>
                            <?php
                            if (trim($_SESSION['email']) == "") {
                                ?>
                                <a href="sign_in.php" class="ref">Войти</a> 
                                <a href="sign_up.php" class="ref">Зарегистрироваться</a>
                                <?php
                            } else {
                                ?>
                                <a class="ref" id="userpic"><img src="img/User_logo.png" height="20px" alt=""  class="userpic"></a> 
                                <a href="user_profile.php" class="ref"><?php echo $_SESSION['email']; ?></a>
                                <a href="sign_out.php" class="ref">Выйти</a>
                                <?php
                            }
                            ?>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <div class="select_n_table_box">
                <div class="select_box">
                    <div id="select_box">
                        <div class="select">
                            <div class="data_selector">
                                <div data-period="days" class="period-selector period-selector-selected">
                                    Дни
                                </div>
                                <div data-period="weeks" class="period-selector">
                                    Недели
                                </div>
                                <div data-period="months" class="period-selector">
                                    Месяцы
                                </div>
                                <div data-period="range" class="period-selector">
                                    Диапазон
                                </div>
                            </div>
                            <div class="data_options">			
                                <input readonly id="select-date" type="text" value=<?php echo date('d.m.Y'); ?>>
                                <input id="add-date" type="button" value="+ добавить день">
                                <input id="change-show-dates" type="button" value="Показывать">
                                <label>с:</label>
                                <input readonly id="select-date-from" type="text" value=<?php echo date('d.m.Y'); ?>>
                                <label>по:</label>
                                <input readonly id="select-date-to" type="text" value=<?php echo date('d.m.Y'); ?>>

                                <input id="sources-button" type="button" value="+/- рекламный канал">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="select_table">
                    <div class="horiz-floating-table">
                        <table id="header-table-names">
                            <tbody>
                                <tr>
                                    <td id="headername1" colspan="6"> Реклама</td>
                                    <td id="headername2" colspan="3"> Сайт</td>
                                    <td id="headername3" colspan="4"> Отдел продаж</td>
                                    <td id="headername4" colspan="3"> Прибыль</td>
                                </tr>
                                <tr>
                                    <td id="" class="column1"></td>
                                    <td id="" class="column2"></td>
                                    <td id="" class="column3"></td>
                                    <td id="" class="column4"></td>
                                    <td id="" class="column5"></td>
                                    <td id="" class="column6"></td>
                                    <td id="" class="column7"></td>
                                    <td id="" class="column8"></td>
                                    <td id="" class="column9"></td>
                                    <td id="" class="column10"></td>
                                    <td id="" class="column11"></td>
                                    <td id="" class="column12"></td>
                                    <td id="" class="column13"></td>
                                    <td id="" class="column14"></td>
                                    <td id="" class="column16"></td>
                                    <td id="" class="column15"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="horiz-fixed-table-part">
                        <table id="header-table-fixed">
                            <tbody>
                                <tr>
                                    <td id="header1" class="column1">
                                        <div class="column1">
                                            <select id="select-source-type">
                                                <option value="inet">Оплата за клик</option>
                                                <option value="email">E-mail рассылка</option>
                                                <option value="phone">Холодные звонки</option>
                                                <option value="misc">Прочие</option>
                                            </select> 
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class = "wrapper-floating-table-part">
                        <div class="horiz-floating-table-part">
                            <table id="header-table">
                                <tbody>
                                    <tr>
                                        <td id="header2" class="column2">
                                            <div class="col-rename-btn" data-id="2"></div>
                                            <div class="column2" id="column-2" >Показы</div>
                                            <div class="column-tip" id="column-tip-2">Не загружено</div>
                                        </td>
                                        <td id="header3" class="column3">
                                            <div class="col-rename-btn" data-id="3"></div>
                                            <div class="column3" id="column-3">CTR</div>
                                            <div class="column-tip" id="column-tip-3">Не загружено</div>
                                        </td>
                                        <td id="header4" class="column4">
                                            <div class="col-rename-btn" data-id="4"></div>
                                            <div class="column4" id="column-4">Клики</div>
                                            <div class="column-tip" id="column-tip-4">Не загружено</div>
                                        </td>
                                        <td id="header5" class="column5">
                                            <div class="col-rename-btn" data-id="5"></div>
                                            <div class="column5" id="column-5">Бюджет, р.</div>
                                            <div class="column-tip" id="column-tip-5">Не загружено</div>
                                        </td>
                                        <td id="header6" class="column6">
                                            <div class="col-rename-btn" data-id="6"></div>
                                            <div class="column6" id="column-6">Цена клика, р.</div>
                                            <div class="column-tip" id="column-tip-6">Не загружено</div>
                                        </td>
                                        <td id="header7" class="column7">
                                            <div class="col-rename-btn" data-id="7"></div>
                                            <div class="column7" id="column-7">Заявки (лиды)</div>
                                            <div class="column-tip" id="column-tip-7">Не загружено</div>
                                        </td>
                                        <td id="header8" class="column8">
                                            <div class="col-rename-btn" data-id="8"></div>
                                            <div class="column8" id="column-8">CV1 сайта</div>
                                            <div class="column-tip" id="column-tip-8">Не загружено</div>
                                        </td>
                                        <td id="header9" class="column9">
                                            <div class="col-rename-btn" data-id="9"></div>
                                            <div class="column9" id="column-9">Цена лида, р.</div>
                                            <div class="column-tip" id="column-tip-9">Не загружено</div>
                                        </td>
                                        <td id="header10" class="column10">
                                            <div class="col-rename-btn" data-id="10"></div>
                                            <div class="column10" id="column-10">Клиенты</div>
                                            <div class="column-tip" id="column-tip-10">Не загружено</div>
                                        </td>
                                        <td id="header11" class="column11">
                                            <div class="col-rename-btn" data-id="11"></div>
                                            <div class="column11" id="column-11">CV2 продажи</div>
                                            <div class="column-tip" id="column-tip-11">Не загружено</div>
                                        </td>
                                        <td id="header12" class="column12">
                                            <div class="col-rename-btn" data-id="12"></div>
                                            <div class="column12"  id="column-12">Цена клиента, р.</div>
                                            <div class="column-tip" id="column-tip-12">Не загружено</div>
                                        </td>
                                        <td id="header13" class="column13">
                                            <div class="col-rename-btn" data-id="13"></div>
                                            <div class="column13" id="column-13">Выручка (оборот), р.</div>
                                            <div class="column-tip" id="column-tip-13">Не загружено</div>
                                        </td>
                                        <td id="header14" class="column14">
                                            <div class="col-rename-btn" data-id="14"></div>
                                            <div class="column14"  id="column-14">Прибыль (без рекламы), р.</div>
                                            <div class="column-tip" id="column-tip-14">Не загружено</div>
                                        </td>
                                        <td id="header16" class="column16">
                                            <div class="col-rename-btn" data-id="16"></div>
                                            <div class="column16"  id="column-16">Прибыль чистая, р.</div>
                                            <div class="column-tip" id="column-tip-16">Не загружено</div>
                                        </td>
                                        <td id="header15" class="column15">
                                            <div class="col-rename-btn" data-id="15"></div>
                                            <div class="column15"  id="column-15">ROI</div>
                                            <div class="column-tip" id="column-tip-15">Не загружено</div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="totals">
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div id="main-data">
                <div id="sales-data">
                </div>
                <div id="sales-data-add">
                    <img class="arrow-down" src="img/loading_data.gif">
                </div>
            </div>
        </div>

        <div class="bottom-fix">
            <div id="bottom-fix">
                <div id="projects-data">
                    <div id="projects">
                        <?php include ("display_projects.php"); ?>		
                    </div>
                    <div id="add-project">
                        + Добавить проект
                    </div>
                </div>

                <!-- футер-->
                <div id="links">
                    <a href="comments.php" target="_blank">Ваши отзывы и пожелания</a>
                    <b>I</b>
                    Идея и концепция: <a href="http://www.оптимизация-конверсии.рф/?ch=vo" target="_blank">Оптимизация-конверсии.рф</a>
                    <b>I</b>


                    <div class="fb-like" style="display:inline-block; top:0px; margin-right:20px;" data-href="http://xn----7sbbf2bcjmddcaew.xn--p1ai" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
                    <div id="vk_like" element_id="voronka1" style="display:inline-block;top:5px;"></div>
                    <script type="text/javascript">
                        VK.Widgets.Like("vk_like", {type: "button"}, 1);
                    </script>
                </div>
            </div>
        </div>

        <div id="charts-panel" class="non-visible">
            <div id="charts-source-total-swither">
                <input class="checkbox2" id="charts-sources-total" type="checkbox">
                <label for="charts-sources-total" id="charts-source-total-swither-title">Сумма по всем источникам</label>
            </div>
            <div id="charts-area"></div>
            <div id="charts-panel-switch-show"></div>
            <div id="charts-panel-switch-hide"></div>
            <div id="charts-panel-reload"></div>
        </div>

        <!--Данные о настройках пользователя: последний проект-->
        <?php include ("get_user_settings.php"); ?>	

        <div class="form2" id="comments-form">
            <h2>Добавить комментарий</h2>
            <div id="comments-block">
            </div>
            <p>
                <input class="submit2" id="mod-comment-button" type="button" value="Добавить">
            </p>
        </div>

        <div id="date-form">
            <div class="del-confirmation-block">
                <label>Удалить дату?</label>
                <br>
                <input class="checkbox2" id="del-date-confirm" type="checkbox">
                Подтверждаю
                <input class="submit2" id="del-date-button" type="button" value="Удалить">
                <br>
                <label class="del-confirmation-timer"></label>
            </div>
            <h2>Изменить дату</h2>
            <p>
                <input readonly id="mod-date" type="text">
            <div class="del-icon"></div>
        </p>
        <p>
            <input class="submit2" id="modify-date-button" type="button" value="Сохранить">
        </p>
    </div>

    <div id="add-project-form">
        <h2>Добавить проект</h2>
        <label>Название:</label>
        <br>
        <input id="create-project-name" type="text" size="20" maxlength="20" value="">
        <p></p>
        <p>
            <label>Комментарий:</label>
            <br>
            <textarea id="create-project-comment" cols="40" rows="5"></textarea>
        </p>
        <p>
            <input class="submit2" id="create-project-button" type="button" value="Создать">
        </p>
    </div>

    <div id="mod-project-form">
        <div class="del-confirmation-block">
            <label>Удалить проект?</label>
            <br>
            <input class="checkbox2" id="del-project-confirm" type="checkbox">
            Подтверждаю
            <input class="submit2" id="del-project-button" type="button" value="Удалить">
            <br>
            <label class="del-confirmation-timer"></label>
        </div>
        <h2>Редактировать проект</h2>
        <label>Название:</label>
        <br>
        <input id="modify-project-name" type="text" size="20" maxlength="20" value="">
        <div class="del-icon"></div>
        <p>
            <label>Комментарий:</label>
            <br>
            <textarea id="modify-project-comment" cols="40" rows="5"></textarea>
        </p>
        <p>
            <input class="submit2" id="modify-project-button" type="button" value="Сохранить">
        </p>
    </div>

    <div id="sources-form">
        <div class="del-confirmation-block">
            <label>Удалить источник?</label>
            Данные невозможно будет восстановить. Вы можете просто скрыть его или переименовать.
            <br>
            <input class="checkbox2" id="del-source-confirm" type="checkbox">
            Подтверждаю
            <input class="submit2" id="del-source-button" type="button" value="Удалить">
            <br>
            <label class="del-confirmation-timer"></label>
        </div>
        <h2>Источники трафика</h2>
        <div>
            <table class="sourses-table">
                <tbody id="sources">

                </tbody>
            </table>
            <label>Добавить источник<br></label>
            <input id="sources-name" type="text" size="20" maxlength="20" value="">
            <input class="submit2" id="sources-create" type="button" value="Добавить">
        </div>
    </div>

    <div id="mod-column-form">
        <h2>Редактировать колонку</h2>
        <label>Название:</label>
        <br>
        <input id="modify-column-name" type="text" size="20" maxlength="20" value="">
        <p></p>
        <p>
            <label>Комментарий:</label>
            <br>
            <textarea id="modify-column-comment" cols="40" rows="5"></textarea>
        </p>
        <p>
            <input class="submit2" id="modify-column-button" type="button" value="Сохранить">
        </p>
    </div>

    <div class="hidden" id="help-video">

        <div style="width: 840px; height: 500px; margin: auto; tex-align:center;">
            <h1 style="margin: auto; font-size: 19px; font-family: pt sans,sans-serif;">Пожалуйста посмотрите видеоинструкцию</h1>
            <iframe width="840" height="472" src="https://www.youtube.com/embed/seEM7V7INvs" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>

    <div id="chart-full-form">
        <div id="chart-full-container"></div>
    </div>

    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'>
        (function () {
            var widget_id = 'OAEOHOOA6U';
                        var s = document.createElement('script');
                        s.type = 'text/javascript';
                        s.async = true;
                        s.src = '//code.jivosite.com/script/widget/' + widget_id;
                        var ss = document.getElementsByTagName('script')[0];
                        ss.parentNode.insertBefore(s, ss);
                    })();</script>
    <!-- {/literal} END JIVOSITE CODE -->
</body>
</html>