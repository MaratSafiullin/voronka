var CURDATE = new Date();
var TODAY = getLocaleDateStr(CURDATE);
var showFromDate = TODAY;
var showToDate = TODAY;
var curentProcessDate = TODAY;
var stopDate = curentProcessDate;

var haveBottomDataToLoad = true;
var readyToLoadMore = true;
var finishedChangingHeaders = true;
var sourceTypesToLoad = 0;
var projectParamsToLoad = 0;
var sourcesToChange = 0;
var timerInProgress = [];

var displayType = "days";
var projectID = 0;
var sourceTypeID = 1;
var totalType = "week";
var columnNum = 0;

var vScroll = $(document).scrollTop();
var hScroll = $(document).scrollLeft();

var chartsPanelSwitchPos = 1;
var easingType = 'easeOutCirc';
var chartsSourcesTotal = false;
var chartsOrder = [];
var chartsReloadTimeout;

var sourceHighlightColor = "#DFE3E8";

//Перевернуть строку
//для общих целей
function reverseStr(str) {
    return str.split("").reverse().join("");
}

//получить дату в удобоваримом виде (напр 01.01.2001), используется вместо toLocaleDateString(), т.к. та криво работает в некоторых браузерах
function getLocaleDateStr(dateObj) {
    var date = dateObj.getDate();
    if (date < 10)
        date = "0" + date;
    var month = dateObj.getMonth() + 1;
    if (month < 10)
        month = "0" + month;
    var year = dateObj.getFullYear();
    var str = date + "." + month + "." + year;
    return str;
}

//Преобразуем строку даты (российский формат) в объект даты
//используется заместо Date.parse() ?????х.з. может это и велосипед
function strToDate(str) {
    var year = Number(str.substr(6, 4));
    var month = Number(str.substr(3, 2)) - 1;
    var day = Number(str.substr(0, 2));
    return new Date(year, month, day);
}

//получить объект даты из строки 
//специально чтобы работало в кривом сафари 
function strToDateForPeriods(str) {
    var year = Number(str.substr(0, 4));
    var month = Number(str.substr(5, 2)) - 1;
    var day = Number(str.substr(8, 2));
    return new Date(year, month, day);
}

//Подгружаем данные в конец ДИВа (в основном при пролистывании вниз)
//Подгружается очередная порция перидов (день, неделя, месяц)
//Нужно дождаться конца предыдущей подгрузки
//Если нечего подгружать, то АЯКС вернет пустой ХТМЛ
//Обновляем каждый раз дату, начиная с которой подгружать в следующий раз
function appendBottomSalesData(processDate) {
    if (readyToLoadMore) {
        readyToLoadMore = false;
        $.post('load_sales_data.php', {process_date: processDate, show_from_date: showFromDate, display_type: displayType, project_id: projectID, source_type_id: sourceTypeID}, function (html) {
            $('#sales-data').append(html);
            //Обновляем дату, начиная с которой подгружать данные к конец в следующий раз
            if (curentProcessDate !== stopDate) {
                curentProcessDate = stopDate;
            }
            //если дата не изменилась, то ставим флаг, чтоб не пытаться опять грузить пустоту
            else {
                haveBottomDataToLoad = false;
                //скрываем ДИВ подгрузки данных
                $("#sales-data-add").fadeOut(0);
            }
            //если загрузили период, то точно не нужно грузить что-то еще
            if (displayType === "range") {
                haveBottomDataToLoad = false;
            }
            //теперь можно грузить следующую порцию
            readyToLoadMore = true;
            //вешаем обработчики на добавленные части (периоды)
            addInputHandler();
            addCommentsHandler();
            addDateHandler();
            //к добавленным частям документа добавляем еще фиксированные по горизонтали "заголовки таблиц" (первый столбец с названиями источников данных) 
            addFixedTableHeaders();
            //Форматируем строки с обозначением границ отображаемого периода (чтоб красиво смотрелось) 
            formatPeriodSigns(false);

            //подгружаем еще данные вниз автоматически, так чтобы появилась полоса прокрутки (только если ее еще нет)
            if (haveBottomDataToLoad) {
                if ($(window).height() >= $(document).height()) {
                    appendBottomSalesData(curentProcessDate);
                }
            }
        }, 'html');
    }
}

//Форматируем строки с обозначением границ отображаемого периода (чтоб красиво смотрелось)
//пройдемся по всем ДИВам и заменим содержимое
function formatPeriodSigns(startFromTop) {
    var periodDates = $(".period-date");
    if (periodDates.length !== 0) {
        var containerDiv;
        var dateStart, dateStop;
        //если добавляли период в верхнюю часть окна (при создании нового периода в проекте), то нет смысла начинать снизу 
        var startPos;
        if (startFromTop) {
            startPos = 0;
        } else {
            startPos = periodDates.length - 1;
        }
        //	
        for (var i = startPos; i >= 0; i--) {
            containerDiv = periodDates[i].parentElement;
            dateStart = strToDateForPeriods(containerDiv.dataset["start_date"]);
            dateStop = strToDateForPeriods(containerDiv.dataset["stop_date"]);
            switch (displayType) {
                case "days":
                    periodDates[i].innerHTML = periodSign("day", dateStart);
                    break;
                case "weeks":
                case "range":
                    periodDates[i].innerHTML = periodSign("range", dateStart, dateStop);
                    break;
            }
        }
    }
}

//к добавленным частям документа добавляем еще фиксированные по горизонтали "заголовки таблиц" (первый столбец с названиями источников данных)
function addFixedTableHeaders() {
    var containers = $(".sales-table-container");
    //для каждого ДИВа с таблицей данных
    for (var i = containers.length - 1; i >= 0; i--) {
        //если еще нет "заголовка"
        if (!$(containers[i]).children().is(".sales-data-table-header-container")) {
            var mainTableRows = $(containers[i]).find(".sales-data-table tr");
            //создаем ДИВ с "заголовком"
            var headerContainer = $("<div class='sales-data-table-header-container'></div>");
            var headerTable = $("<table class='sales-data-table'></table>");
            $(headerContainer).append(headerTable);
            //копируем все строки из оригинальной таблицы
            for (var j = 0; j < mainTableRows.length; j++) {
                var headerRow = $(mainTableRows[j]).clone(true);
                var headerCells = $(headerRow).children();
                //при этом удаляем все ячейки кроме первой из копии
                for (var k = 1; k < headerCells.length; k++) {
                    $(headerCells[k]).remove();
                }
                $(headerTable).append(headerRow);
            }
            //добавляем полученный ДИВ в надконтейнер (ДИВ)
            $(containers[i]).append(headerContainer);
        }
        //
        else {
            break;
        }
    }
    //запускаем обработку скролла чтоб выставить "заголовки" в нужное место
    $(window).scroll();
}

//Добавляем период к проекту (если уже есть, то ничего не добавится)
//Срабатывает только по завершению работы предыдущей функции
function addSalesData(processDate) {
    if (readyToLoadMore) {
        readyToLoadMore = false;
        $.post('add_sales_data.php', {process_date: processDate, display_type: displayType, project_id: projectID, source_type_id: sourceTypeID}, function (html) {
            //скрываем инструкцию если была на странице
            $(".instruction").fadeOut(0);
            //добавленный период будет показан в самом верху
            $('#sales-data').prepend(html);
            readyToLoadMore = true;
            //вешаем обработчики на добавленные элементы
            addInputHandler();
            addCommentsHandler();
            addDateHandler();
            //Если добавленная дата вылазит за наш диапазон показа, то расширяем диапазон вверх или вниз
            //Обновляем поля выбора диапазона
            //Сохраняем новые настройки проекта
            var dtAddDate = strToDate(processDate);
            var dtShowFromDate = strToDate(showFromDate);
            var dtShowToDate = strToDate(showToDate);
            if (Number(dtAddDate) > Number(dtShowToDate)) {
                showToDate = getLocaleDateStr(dtAddDate);
                changeCalendar("#select-date-to", displayType);
                saveProjectSettings();
                //если пытались добавить дату которая уже в базе, но вылазит за границу периода, перезагрузим содержимое, чтобы ее было видно
                if (html === "") {
                    reloadSalesData();
                }
            }
            if (Number(dtAddDate) < Number(dtShowFromDate)) {
                showFromDate = getLocaleDateStr(dtAddDate);
                changeCalendar("#select-date-from", displayType);
                saveProjectSettings();
            }
            //Форматируем строки с обозначением границ отображаемого периода (чтоб красиво смотрелось)
            formatPeriodSigns(true);
        }, 'html');
    }
}

//функция вносящая изменения в базу и страницу на основании нового числа в ячейке
function modSalesData(objInput, newVal, colName, dataSourceID, StartDate, StopDate, displayType, projectID) {
    //изменяем значение в базе
    $.post('mod_sales_data.php', {amount: newVal, col_name: colName, data_source_id: dataSourceID, start_date: StartDate, stop_date: StopDate, display_type: displayType, project_id: projectID}, function (html) {
        //в самой ячейке, может быть отличным от того, что пытались ввести (см. mod_sales_data.php)
        values = html.split("\n");
        objInput.value = values[0];

        //находим ссылки на другие ячейки той же строки
        id = "#" + StopDate + "-" + dataSourceID + "-displaysCount";
        objDisplaysCount = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-CTR";
        objCTR = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-clicksCount";
        objClicksCount = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-clicksCost";
        objClicksCost = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-averageClickCost";
        objAverageClickCost = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-leadsCount";
        objLeadsCount = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-siteCV";
        objSiteCV = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-averageLeadCost";
        objAverageLeadCost = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-customersCount";
        objCustomersCount = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-salesSV";
        objSalesSV = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-averageCustomerCost";
        objAverageCustomerCost = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-income";
        objIncome = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-profit";
        objProfit = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-netprofit";
        objNetProfit = $(id)[0];
        id = "#" + StopDate + "-" + dataSourceID + "-ROI";
        objROI = $(id)[0];
        //и на ячейки итогов
        id = "#" + StopDate + "-total-displaysCount";
        objDisplaysCountTotal = $(id)[0];
        id = "#" + StopDate + "-total-CTR";
        objCTRTotal = $(id)[0];
        id = "#" + StopDate + "-total-clicksCount";
        objClicksCountTotal = $(id)[0];
        id = "#" + StopDate + "-total-clicksCost";
        objClicksCostTotal = $(id)[0];
        id = "#" + StopDate + "-total-averageClickCost";
        objAverageClickCostTotal = $(id)[0];
        id = "#" + StopDate + "-total-leadsCount";
        objLeadsCountTotal = $(id)[0];
        id = "#" + StopDate + "-total-siteCV";
        objSiteCVTotal = $(id)[0];
        id = "#" + StopDate + "-total-averageLeadCost";
        objAverageLeadCostTotal = $(id)[0];
        id = "#" + StopDate + "-total-customersCount";
        objCustomersCountTotal = $(id)[0];
        id = "#" + StopDate + "-total-salesSV";
        objSalesSVTotal = $(id)[0];
        id = "#" + StopDate + "-total-averageCustomerCost";
        objAverageCustomerCostTotal = $(id)[0];
        id = "#" + StopDate + "-total-income";
        objIncomeTotal = $(id)[0];
        id = "#" + StopDate + "-total-profit";
        objProfitTotal = $(id)[0];
        id = "#" + StopDate + "-total-netprofit";
        objNetProfitTotal = $(id)[0];
        id = "#" + StopDate + "-total-ROI";
        objROITotal = $(id)[0];

        //убираем форматирование, разбивку по разрядам
        objDisplaysCount.value = objDisplaysCount.value.replace(/'/g, '');
        objCTR.innerHTML = objCTR.innerHTML.replace(/'/g, '');
        objClicksCount.value = objClicksCount.value.replace(/'/g, '');
        objClicksCost.value = objClicksCost.value.replace(/'/g, '');
        objAverageClickCost.innerHTML = objAverageClickCost.innerHTML.replace(/'/g, '');
        objLeadsCount.value = objLeadsCount.value.replace(/'/g, '');
        objSiteCV.innerHTML = objSiteCV.innerHTML.replace(/'/g, '');
        objAverageLeadCost.innerHTML = objAverageLeadCost.innerHTML.replace(/'/g, '');
        objCustomersCount.value = objCustomersCount.value.replace(/'/g, '');
        objSalesSV.innerHTML = objSalesSV.innerHTML.replace(/'/g, '');
        objAverageCustomerCost.innerHTML = objAverageCustomerCost.innerHTML.replace(/'/g, '');
        objIncome.value = objIncome.value.replace(/'/g, '');
        objProfit.value = objProfit.value.replace(/'/g, '');
        objNetProfit.innerHTML = objNetProfit.innerHTML.replace(/'/g, '');
        objROI.innerHTML = objROI.innerHTML.replace(/'/g, '');

        objDisplaysCountTotal.innerHTML = objDisplaysCountTotal.innerHTML.replace(/'/g, '');
        objCTRTotal.innerHTML = objCTRTotal.innerHTML.replace(/'/g, '');
        objClicksCountTotal.innerHTML = objClicksCountTotal.innerHTML.replace(/'/g, '');
        objClicksCostTotal.innerHTML = objClicksCostTotal.innerHTML.replace(/'/g, '');
        objAverageClickCostTotal.innerHTML = objAverageClickCostTotal.innerHTML.replace(/'/g, '');
        objLeadsCountTotal.innerHTML = objLeadsCountTotal.innerHTML.replace(/'/g, '');
        objSiteCVTotal.innerHTML = objSiteCVTotal.innerHTML.replace(/'/g, '');
        objAverageLeadCostTotal.innerHTML = objAverageLeadCostTotal.innerHTML.replace(/'/g, '');
        objCustomersCountTotal.innerHTML = objCustomersCountTotal.innerHTML.replace(/'/g, '');
        objSalesSVTotal.innerHTML = objSalesSVTotal.innerHTML.replace(/'/g, '');
        objAverageCustomerCostTotal.innerHTML = objAverageCustomerCostTotal.innerHTML.replace(/'/g, '');
        objIncomeTotal.innerHTML = objIncomeTotal.innerHTML.replace(/'/g, '');
        objProfitTotal.innerHTML = objProfitTotal.innerHTML.replace(/'/g, '');
        objNetProfitTotal.innerHTML = objNetProfitTotal.innerHTML.replace(/'/g, '');
        objROITotal.innerHTML = objROITotal.innerHTML.replace(/'/g, '');

        //В зависимости от того, что изменили, делаем пересчет значений в других ячейках
        switch (colName) {
            case "displays_count":
                objDisplaysCountTotal.innerHTML = parseInt(objDisplaysCountTotal.innerHTML) + parseInt(values[0]) - parseInt(values[1]);

                if (objDisplaysCount.value !== 0) {
                    objCTR.innerHTML = Number(100 * objClicksCount.value / objDisplaysCount.value).toFixed(3);
                } else {
                    objCTR.innerHTML = "";
                }

                if (objDisplaysCountTotal.innerHTML !== "0") {
                    objCTRTotal.innerHTML = Number(100 * parseInt(objClicksCountTotal.innerHTML) / parseInt(objDisplaysCountTotal.innerHTML)).toFixed(3);
                } else {
                    objCTRTotal.innerHTML = "";
                }
                break;
            case "clicks_count":
                objClicksCountTotal.innerHTML = parseInt(objClicksCountTotal.innerHTML) + parseInt(values[0]) - parseInt(values[1]);

                if (objDisplaysCount.value !== 0) {
                    objCTR.innerHTML = Number(100 * objClicksCount.value / objDisplaysCount.value).toFixed(3);
                } else {
                    objCTR.innerHTML = "";
                }
                if (objClicksCount.value !== 0) {
                    objAverageClickCost.innerHTML = Number(objClicksCost.value / objClicksCount.value).toFixed(2);
                } else {
                    objAverageClickCost.innerHTML = "";
                }
                if (objClicksCount.value !== 0) {
                    objSiteCV.innerHTML = Number(100 * objLeadsCount.value / objClicksCount.value).toFixed(2);
                } else {
                    objSiteCV.innerHTML = "";
                }

                if (objDisplaysCountTotal.innerHTML !== "0") {
                    objCTRTotal.innerHTML = Number(100 * parseInt(objClicksCountTotal.innerHTML) / parseInt(objDisplaysCountTotal.innerHTML)).toFixed(3);
                } else {
                    objCTRTotal.innerHTML = "";
                }
                if (objClicksCountTotal.innerHTML !== "0") {
                    objAverageClickCostTotal.innerHTML = Number(parseInt(objClicksCostTotal.innerHTML) / parseInt(objClicksCountTotal.innerHTML)).toFixed(2);
                } else {
                    objAverageClickCostTotal.innerHTML = "";
                }
                if (objClicksCountTotal.innerHTML !== "0") {
                    objSiteCVTotal.innerHTML = Number(100 * parseInt(objLeadsCountTotal.innerHTML) / parseInt(objClicksCountTotal.innerHTML)).toFixed(2);
                } else {
                    objSiteCVTotal.innerHTML = "";
                }
                break;
            case "clicks_cost":
                objClicksCostTotal.innerHTML = parseInt(objClicksCostTotal.innerHTML) + parseInt(values[0]) - parseInt(values[1]);

                if (objClicksCount.value !== 0) {
                    objAverageClickCost.innerHTML = Number(objClicksCost.value / objClicksCount.value).toFixed(2);
                } else {
                    objAverageClickCost.innerHTML = "";
                }
                if (objLeadsCount.value !== 0) {
                    objAverageLeadCost.innerHTML = Number(objClicksCost.value / objLeadsCount.value).toFixed(2);
                } else {
                    objAverageLeadCost.innerHTML = "";
                }
                if (objCustomersCount.value !== 0) {
                    objAverageCustomerCost.innerHTML = Number(objClicksCost.value / objCustomersCount.value).toFixed(2);
                } else {
                    objAverageCustomerCost.innerHTML = "";
                }
                objNetProfit.innerHTML = objProfit.value - objClicksCost.value;
                if (objClicksCost.value !== 0) {
                    objROI.innerHTML = Number(100 * parseInt(objNetProfit.innerHTML) / objClicksCost.value).toFixed(2);
                } else {
                    objROI.innerHTML = "";
                }

                if (objClicksCountTotal.innerHTML !== "0") {
                    objAverageClickCostTotal.innerHTML = Number(parseInt(objClicksCostTotal.innerHTML) / parseInt(objClicksCountTotal.innerHTML)).toFixed(2);
                } else {
                    objAverageClickCostTotal.innerHTML = "";
                }
                if (objLeadsCountTotal.innerHTML !== "0") {
                    objAverageLeadCostTotal.innerHTML = Number(parseInt(objClicksCostTotal.innerHTML) / parseInt(objLeadsCountTotal.innerHTML)).toFixed(2);
                } else {
                    objAverageLeadCostTotal.innerHTML = "";
                }
                if (objCustomersCountTotal.innerHTML !== "0") {
                    objAverageCustomerCostTotal.innerHTML = Number(parseInt(objClicksCostTotal.innerHTML) / parseInt(objCustomersCountTotal.innerHTML)).toFixed(2);
                } else {
                    objAverageCustomerCostTotal.innerHTML = "";
                }
                objNetProfitTotal.innerHTML = parseInt(objProfitTotal.innerHTML) - parseInt(objClicksCostTotal.innerHTML);
                if (objClicksCostTotal.innerHTML !== "0") {
                    objROITotal.innerHTML = Number(100 * parseInt(objNetProfitTotal.innerHTML) / parseInt(objClicksCostTotal.innerHTML)).toFixed(2);
                } else {
                    objROITotal.innerHTML = "";
                }
                break;
            case "leads_count":
                objLeadsCountTotal.innerHTML = parseInt(objLeadsCountTotal.innerHTML) + parseInt(values[0]) - parseInt(values[1]);
                if (objClicksCount.value !== 0) {
                    objSiteCV.innerHTML = Number(100 * objLeadsCount.value / objClicksCount.value).toFixed(2);
                } else {
                    objSiteCV.innerHTML = "";
                }
                if (objLeadsCount.value !== 0) {
                    objAverageLeadCost.innerHTML = Number(objClicksCost.value / objLeadsCount.value).toFixed(2);
                } else {
                    objAverageLeadCost.innerHTML = "";
                }
                if (objLeadsCount.value !== 0) {
                    objSalesSV.innerHTML = Number(100 * objCustomersCount.value / objLeadsCount.value).toFixed(2);
                } else {
                    objSalesSV.innerHTML = "";
                }

                if (objClicksCountTotal.innerHTML !== "0") {
                    objSiteCVTotal.innerHTML = Number(100 * parseInt(objLeadsCountTotal.innerHTML) / parseInt(objClicksCountTotal.innerHTML)).toFixed(2);
                } else {
                    objSiteCVTotal.innerHTML = "";
                }
                if (objLeadsCountTotal.innerHTML !== "0") {
                    objAverageLeadCostTotal.innerHTML = Number(parseInt(objClicksCostTotal.innerHTML) / parseInt(objLeadsCountTotal.innerHTML)).toFixed(2);
                } else {
                    objAverageLeadCostTotal.innerHTML = "";
                }
                if (objLeadsCountTotal.innerHTML !== "0") {
                    objSalesSVTotal.innerHTML = Number(100 * parseInt(objCustomersCountTotal.innerHTML) / parseInt(objLeadsCountTotal.innerHTML)).toFixed(2);
                } else {
                    objSalesSVTotal.innerHTML = "";
                }
                break;
            case "customers_count":
                objCustomersCountTotal.innerHTML = parseInt(objCustomersCountTotal.innerHTML) + parseInt(values[0]) - parseInt(values[1]);

                if (objLeadsCount.value !== 0) {
                    objSalesSV.innerHTML = Number(100 * objCustomersCount.value / objLeadsCount.value).toFixed(2);
                } else {
                    objSalesSV.innerHTML = "";
                }
                if (objCustomersCount.value !== 0) {
                    objAverageCustomerCost.innerHTML = Number(objClicksCost.value / objCustomersCount.value).toFixed(2);
                } else {
                    objAverageCustomerCost.innerHTML = "";
                }

                if (objLeadsCountTotal.innerHTML !== "0") {
                    objSalesSVTotal.innerHTML = Number(100 * parseInt(objCustomersCountTotal.innerHTML) / parseInt(objLeadsCountTotal.innerHTML)).toFixed(2);
                } else {
                    objSalesSVTotal.innerHTML = "";
                }
                if (objCustomersCountTotal.innerHTML !== "0") {
                    objAverageCustomerCostTotal.innerHTML = Number(parseInt(objClicksCostTotal.innerHTML) / parseInt(objCustomersCountTotal.innerHTML)).toFixed(2);
                } else {
                    objAverageCustomerCostTotal.innerHTML = "";
                }
                break;
            case "income":
                objIncomeTotal.innerHTML = parseInt(objIncomeTotal.innerHTML) + parseInt(values[0]) - parseInt(values[1]);
                break;
            case "profit":
                objProfitTotal.innerHTML = parseInt(objProfitTotal.innerHTML) + parseInt(values[0]) - parseInt(values[1]);

                objNetProfit.innerHTML = objProfit.value - objClicksCost.value;
                if (objClicksCost.value !== 0) {
                    objROI.innerHTML = Number(100 * parseInt(objNetProfit.innerHTML) / objClicksCost.value).toFixed(2);
                } else {
                    objROI.innerHTML = "";
                }

                objNetProfitTotal.innerHTML = parseInt(objProfitTotal.innerHTML) - parseInt(objClicksCostTotal.innerHTML);
                if (objClicksCostTotal.innerHTML !== "0") {
                    objROITotal.innerHTML = Number(100 * parseInt(objNetProfitTotal.innerHTML) / parseInt(objClicksCostTotal.innerHTML)).toFixed(2);
                } else {
                    objROITotal.innerHTML = "";
                }
                break;
        }

        //разбивка чисел (которые могли измениться) в таблице по разрядам, возврат форматирования
        objDisplaysCount.value = objDisplaysCount.value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objCTR.innerHTML = objCTR.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objClicksCount.value = objClicksCount.value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objClicksCost.value = objClicksCost.value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objAverageClickCost.innerHTML = objAverageClickCost.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objLeadsCount.value = objLeadsCount.value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objSiteCV.innerHTML = objSiteCV.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objAverageLeadCost.innerHTML = objAverageLeadCost.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objCustomersCount.value = objCustomersCount.value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objSalesSV.innerHTML = objSalesSV.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objAverageCustomerCost.innerHTML = objAverageCustomerCost.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objIncome.value = objIncome.value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objProfit.value = objProfit.value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objNetProfit.innerHTML = objNetProfit.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objROI.innerHTML = objROI.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");

        objDisplaysCountTotal.innerHTML = objDisplaysCountTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objCTRTotal.innerHTML = objCTRTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objClicksCountTotal.innerHTML = objClicksCountTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objClicksCostTotal.innerHTML = objClicksCostTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objAverageClickCostTotal.innerHTML = objAverageClickCostTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objLeadsCountTotal.innerHTML = objLeadsCountTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objSiteCVTotal.innerHTML = objSiteCVTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objAverageLeadCostTotal.innerHTML = objAverageLeadCostTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objCustomersCountTotal.innerHTML = objCustomersCountTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objSalesSVTotal.innerHTML = objSalesSVTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objAverageCustomerCostTotal.innerHTML = objAverageCustomerCostTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objIncomeTotal.innerHTML = objIncomeTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objProfitTotal.innerHTML = objProfitTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objNetProfitTotal.innerHTML = objNetProfitTotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        objROITotal.innerHTML = objROITotal.innerHTML.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        //итоги в шапке попросту перезагружаем из базы, пересчитать их тут нереально
        reloadTotals();
    }, 'html');
}

//Обработка взаимодействия в инпутами таблиц данных
function addInputHandler() {
    //Убираем существующие обработчики
    $(".sales-input").unbind("keydown");
    $(".sales-input").unbind("focusin");
    $(".sales-input").unbind("focusout");
    $(".sales-input").unbind("change");
    //При нажатии вверх или (вниз/enter) переводим фокус в инпут в другой строке
    //клавиши WSAD тоже используются для перехода между полями
    //Скроллим страницу чтобы это поле ввода было в видимой части
    $(".sales-input").keydown(function (e) {
        var rowLenght = $(this.parentElement.parentElement.parentElement).find("input").length;
        var inputsArr = $(".sales-data-table").find("input:visible");
        var currentInputNum;
        var newInputNum;
        for (var i = 0; i < inputsArr.length; i++) {
            if (inputsArr[i] === this) {
                currentInputNum = i;
                break;
            }
        }
        //вверх
        if ((e.which === 38) || (e.which === 87)) {
            newInputNum = currentInputNum - rowLenght;
            if (newInputNum >= 0) {
                inputsArr[newInputNum].focus();
            }
        }
        //вниз
        if ((e.which === 40) || (e.which === 13) || (e.which === 83)) {
            newInputNum = currentInputNum + rowLenght;
            if (newInputNum < inputsArr.length) {
                inputsArr[newInputNum].focus();
            }
        }
        //влево
        if ((e.which === 65)) {
            newInputNum = currentInputNum - 1;
            if (newInputNum >= 0) {
                inputsArr[newInputNum].focus();
            }
        }
        //вправо
        if ((e.which === 68)) {
            newInputNum = currentInputNum + 1;
            if (newInputNum < inputsArr.length) {
                inputsArr[newInputNum].focus();
            }
        }

        /* if ((e.which === 38) || (e.which === 40) || (e.which === 13) || (e.which === 87) || (e.which === 83) || (e.which === 65) || (e.which === 68)){
         //скролл
         prevCellTop = $(this.parentElement.parentElement).offset().top;
         prevCellLeft = $(this.parentElement.parentElement).offset().left;
         nextCellTop = $(inputsArr[newInputNum].parentElement.parentElement).offset().top;
         nextCellLeft = $(inputsArr[newInputNum].parentElement.parentElement).offset().left;
         vScroll = vScroll - prevCellTop + nextCellTop;
         hScroll = hScroll - prevCellLeft + nextCellLeft;
         if (vScroll > $(document).height()) vScroll = $(document).height();
         if (hScroll > $(document).width()) hScroll = $(document).width();
         if (vScroll < 0) vScroll = 0;
         if (hScroll < 0) hScroll = 0;
         $("body,html").animate({"scrollTop": vScroll}, 0);
         $("body,html").animate({"scrollLeft": hScroll}, 0);
         } */

        if ((e.which === 87) || (e.which === 83) || (e.which === 65) || (e.which === 68)) {
            return false;
        }
    });

    //При фокусе на инпут если там был "0" удаляем его, подсвечиваем ячейку таблицы
    //Убираем форматирование числа
    //Подсвечиваем строки с выбраным источником данных (во всех таблицах)
    $(".sales-input").focusin(function () {
        //
        if (this.value === "0") {
            this.value = "";
        }
        //убираем форматирование, разбивку по разрядам
        else {
            this.value = this.value.replace(/'/g, '');
        }
        //подсветка
        var cell = this.parentElement.parentElement;
        $(cell).css("outline-style", "groove");
        //подсветка источника
        var tRow = this.parentElement.parentElement.parentElement;
        var dataSourceID = tRow.dataset['data_source_id'];
        var sourceCells = $('[data-data_source_id=' + dataSourceID + ']').find("td");
        for (var i = 0; i < sourceCells.length; i++) {
            sourceCells[i].dataset["orig_bckgr"] = $(sourceCells[i]).css("background-color");
        }
        $(sourceCells).css("background-color", sourceHighlightColor);
    });
    //При потере фокуса если там было пусто возвращаем "0", убираем подсветку ячейку таблицы
    //Применяем форматирование числа
    $(".sales-input").focusout(function () {
        if (this.value === "") {
            this.value = "0";
        }
        //разбивка числа в инпуте по разрядам
        else {
            this.value = this.value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1'");
        }
        //подсветка
        var cell = this.parentElement.parentElement;
        $(cell).css("outline-style", "none");
        //подсветка источника
        var tRow = this.parentElement.parentElement.parentElement;
        var dataSourceID = tRow.dataset['data_source_id'];
        var sourceCells = $('[data-data_source_id=' + dataSourceID + ']').find("td");
        for (var i = 0; i < sourceCells.length; i++) {
            $(sourceCells[i]).css("background-color", sourceCells[i].dataset["orig_bckgr"]);
        }
    });

    //При изменении данных в редактируемой ячейке проверяем является ли новое значение числом, и, если нет, то меняем его на 0, вносим изменения в базу и значения других ячеек
    $(".sales-input").change(function () {
        //убираем форматирование, разбивку по разрядам
        this.value = this.value.replace(/'/g, '');
        //
        newVal = parseInt(this.value);
        if (isNaN(newVal))
            newVal = 0;
        colName = this.name;
        dataSourceID = this.parentElement.parentElement.parentElement.dataset["data_source_id"];
        displayType = this.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.dataset["display_type"];
        StartDate = this.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.dataset["start_date"];
        StopDate = this.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.dataset["stop_date"];
        objInput = this;
        //функция вносящая изменения в базу и страницу
        modSalesData(objInput, newVal, colName, dataSourceID, StartDate, StopDate, displayType, projectID);
    });
}

//При клике по ДИВу проекта меняем текущий номер проекта, перезагружаем данные по продажам и инфо об источниках
//либо открываем форму с данными этого проекта (если клик по проекту который уже выбран)
function addProjectClickHandler() {
    //открыть диалог модификации проекта
    function openModDialog(element) {
        $("#mod-project-form").dialog("option", "position", {my: "left bottom", at: "left bottom", of: element});
        $("#mod-project-form").dialog("open");
    }

    $(".project").click(function () {
        //если кликаем по выбранному проекту
        if ($(this).hasClass("project-selected")) {
            //получаем его данные из базы и по завершению открываем форму для изменения
            //до завершения загрузки не открываем, чтоб не было колиизии
            var projectBookmark = this;
            projectParamsToLoad = 2;
            $.post('display_project.php', {project_id: projectID, param_name: "name"}, function (html) {
                $("#modify-project-name")[0].value = html;
                projectParamsToLoad--;
                if (projectParamsToLoad === 0) {
                    openModDialog(projectBookmark);
                }
            }, 'html');
            $.post('display_project.php', {project_id: projectID, param_name: "comment"}, function (html) {
                $("#modify-project-comment")[0].value = html;
                projectParamsToLoad--;
                if (projectParamsToLoad === 0) {
                    openModDialog(projectBookmark);
                }
            }, 'html');
        }
        //иначе переключаемся на другой
        else {
            $(".project-selected").removeClass("project-selected");
            $(this).addClass("project-selected");
            projectID = this.dataset["project_id"];
            //сохраняем настройки пользователя
            saveUserSettings();
            //загружаем настройки проекта, обновляем данные
            loadProjectData();
        }
    });
}

//Перезагрузка основных данных
//Сбрасываем флаги и загружаем новые данные в ДИВ в соответствии с выбранным перидом
//Если предыдущая загрузка данных не завершилась, дождемся завершения, чтоб не было коллизии
function reloadSalesData() {
    if (readyToLoadMore) {
        //показываем ДИВ подгрузки данных
        $("#sales-data-add").fadeIn(0);
        curentProcessDate = showToDate;
        stopDate = curentProcessDate;
        haveBottomDataToLoad = true;
        $('#sales-data').empty();
        appendBottomSalesData(curentProcessDate);
        //загружаем графики
        clearTimeout(chartsReloadTimeout);
        chartsReloadTimeout = setTimeout(reloadChartsPanel, 3000);
    } else {
        setTimeout(reloadSalesData, 100);
    }
}

//Перезагружаем инфо по источникам в форму
function reloadSources() {
    //если предыдущая загрузка завершилась
    if (sourceTypesToLoad === 0) {
        //очистка
        $("#sources").empty();
        sourceTypesToLoad = 1;
        //
        $.post('display_sources.php', {type_id: sourceTypeID, project_id: projectID}, function (html) {
            $('#sources').append(html);
            sourceTypesToLoad--;
            if (sourceTypesToLoad === 0) {
                addDeleteSourceHandler();
                addModifySourceHandler();
            }
        }, 'html');
    }
    //иначе пробуем еще раз
    else {
        setTimeout(reloadSources, 100);
    }
}

//Добавляем новый проект
//Срабатывает только по завершению работы предыдущей загрузки данных
function addProject(projectName, projectComment) {
    if (readyToLoadMore) {
        readyToLoadMore = false;
        //Создание проекта
        $.post('add_project.php', {project_name: projectName, project_comment: projectComment}, function (html) {
            $('#projects').empty();
            $('#sales-data').empty();
            //Загружаем новый список проектов
            $.post('display_projects.php', {reload: "yes"}, function (html) {
                $('#projects').append(html);
                //обработка кликов
                addProjectClickHandler();
                //Перезагружаем данные продаж и инфо по источникам
                readyToLoadMore = true;
                reloadSalesData();
                reloadTotals();
                reloadSources();
                //сразу сохраняем настройки пользователя
                saveUserSettings();
                //и настройки проекта
                saveProjectSettings();
                //
                $("#add-project-form").dialog("close");
            }, 'html');
        }, 'html');
    } else {
        setTimeout(addProject, 100, projectName, projectComment);
    }
}

//Вешаем на клик по ДИВу в датой открытие окна для редактирования даты (только если отображение по дням) 
function addDateHandler() {
    //открыть диалог редактирования коммента
    function openModDialog(element) {
        $("#date-form").dialog("option", "position", {my: "right top", at: "right top", of: element});
        $("#date-form").dialog("open");
    }
    //
    $(".period-date").unbind("click");
    //
    if (displayType === "days") {
        $(".period-date").css("cursor", "pointer");
        $(".period-date").click(function () {
            var date = this.parentElement.dataset["start_date"];
            $("#date-form")[0].dataset["date"] = date;
            date = strToDateForPeriods(date);
            $("#date-form").dialog("option", "title", periodSign("day", date));
            var calendarID = "#mod-date";
            $(calendarID)[0].dataset["datepickerStartDate"] = date;
            $(calendarID)[0].dataset["datepickerEndDate"] = date;
            $(calendarID)[0].value = periodSign("day", date);
            openModDialog(this);
        });
    }
}

//Вешаем на клик по ДИВу в комментарием открытие окна для редактирования коммента 
function addCommentsHandler() {
    //открыть диалог редактирования коммента
    function openModDialog(element) {
        $("#comments-form").dialog("option", "position", {my: "right top", at: "right top", of: element});
        $("#comments-form").dialog("open");
    }
    //
    $(".period-comment").unbind("click");
    //
    $(".period-comment").click(function () {
        startDate = this.parentElement.dataset["start_date"];
        stopDate = this.parentElement.dataset["stop_date"];
        var commentBlock = this;
        $.post('display_comments.php', {project_id: projectID, start_date: startDate, stop_date: stopDate}, function (html) {
            $("#comments-block").empty();
            $("#comments-block").append(html);
            openModDialog(commentBlock);
        }, 'html');
    });
}

//Таймер для блока подтверждения удаления, скрывает ДИВ по прошествии определенного числа секунд 
function hideTimerDelBlock(time, formID) {
    if (timerInProgress[formID]) {
        $(formID + " .del-confirmation-timer")[0].innerHTML = time;
        if (time <= 0) {
            $(formID + " .del-confirmation-block").fadeOut(0);
            timerInProgress[formID] = false;
        } else {
            setTimeout(hideTimerDelBlock, 1000, time - 1, formID);
        }
    }
}

//Обработка кликов на удаление источника
function addDeleteSourceHandler() {
    //Убираем существующие обработчики
    $(".delete-source").unbind("click");
    $("#del-source-button").unbind("click");

    //клик по иконке удаления
    $(".delete-source").click(function () {
        var formID = "#sources-form";
        if (!timerInProgress[formID]) {
            //показываем блок с подтверждением удаления
            var parentRow = this.parentElement;
            $("#del-source-button")[0].dataset["source_id"] = parentRow.dataset["source_id"];
            $("#del-source-confirm")[0].checked = false;
            $(formID + " .del-confirmation-block").fadeIn(0);
            //запускаем таймер	
            timerInProgress[formID] = true;
            hideTimerDelBlock(15, formID);
        }
    });

    //клик по кнопке подтверждения удаления
    $("#del-source-button").click(function () {
        if ($("#del-source-confirm")[0].checked) {
            var formID = "#sources-form";
            var sourceID = this.dataset["source_id"];
            sourcesToChange++;
            $.post('del_source.php', {project_id: projectID, source_id: sourceID}, function (html) {
                if (html === "sucess") {
                    //удаляем строку с источником
                    var sourceRows = $(formID + " tr");
                    for (var i = 0; i < sourceRows.length; i++) {
                        if (sourceRows[i].dataset['source_id'] === sourceID) {
                            $(sourceRows[i]).remove();
                            break;
                        }
                    }
                    //скрываем блок подтверждения 
                    $(formID + " .del-confirmation-block").fadeOut(0);
                    timerInProgress[formID] = false;
                    //
                    reloadSources();
                    sourcesToChange--;
                }
            }, 'html');
        }
    });
}

//Обработка редактирования источника
function addModifySourceHandler() {
    $(".source-name").change(function () {
        sourceID = this.parentElement.parentElement.dataset["source_id"];
        sourceName = this.value;
        $.post('mod_source.php', {project_id: projectID, source_id: sourceID, source_name: sourceName}, function (html) {
            //перезагружаем данные  !!!!!! не надо перезагружать все 
            reloadSalesData();
        }, 'html');
    });

    $(".source-visible").change(function () {
        sourceID = this.parentElement.parentElement.dataset["source_id"];
        if (this.checked) {
            sourceVisible = "1";
        } else {
            sourceVisible = "0";
        }
        sourcesToChange++;
        $.post('mod_source.php', {project_id: projectID, source_id: sourceID, source_visible: sourceVisible}, function (html) {
            sourcesToChange--;
        }, 'html');
    });
}


//Добавляем источник в проект (добавляем записи по источнику ко всем дням проекта)
function addSource(typeID, sourceName) {
    sourcesToChange++;
    $.post('add_source.php', {project_id: projectID, type_id: typeID, source_name: sourceName}, function (html) {
        reloadSources();
        sourcesToChange--;
    }, 'html');
}

//Перезагружаем таблицу с итогами. Контрол для выбора периода итогов внутри таблицы, сразу вешаем на него обработчик
function reloadTotals() {
    $.post('display_totals.php', {project_id: projectID, display_type: totalType, process_date: TODAY, source_type_id: sourceTypeID, show_from_date: showFromDate, show_to_date: showToDate}, function (html) {
        $("#totals").empty();
        $("#totals").append(html);

        //обработчик изменения значения в контроле
        $("#select-total-period").change(function () {
            totalType = this.value;
            reloadTotals();
            //сохраняем настройки проекта
            saveProjectSettings();
        });

    }, 'html');
}

//выставляем нужную опцию в контроле типа итогов (нужно при загрузке настроек проекта)
function changeTotalsControl(totalType) {
    options = $("#select-total-period");
    for (var i = 0; i < options.length; i++) {
        if (options[i].selected) {
            $("#select-total-period")[0].selectedIndex = i;
            break;
        }
    }
}

//изменяем заголовки таблицы (при смене типа источников), выставляем нужную опцию в контроле (нужно при загрузке настроек проекта)
function changeHeaders(sourceTypeID) {
    finishedChangingHeaders = false;
    switch (sourceTypeID) {
        case 1:
            $("#headername1")[0].innerHTML = "Реклама";
            $("#headername2")[0].innerHTML = "Сайт";
            break;
        case 2:
            $("#headername1")[0].innerHTML = "Рассылка";
            $("#headername2")[0].innerHTML = "Письмо";
            break;
        case 3:
            $("#headername1")[0].innerHTML = "Выход на ЛПР";
            $("#headername2")[0].innerHTML = "Презентация";
            break;
        case 4:
            $("#headername1")[0].innerHTML = "Реклама";
            $("#headername2")[0].innerHTML = "Презентация (точка продаж)";
            break;
    }
    //
    $.post('get_column_names.php', {project_id: projectID, type_id: sourceTypeID}, function (html) {
        settings = html.split("\n");
        for (var i = 0; i < settings.length; i++) {
            data = settings[i].split("+");
            id = "#column-" + (data[0]);
            idTip = "#column-tip-" + (data[0]);
            $(id).empty();
            $(id).append(data[1]);
            $(idTip).empty();
            $(idTip).append(data[2]);
        }
        finishedChangingHeaders = true;
    }, 'html');
    //
    $("#select-source-type")[0].selectedIndex = sourceTypeID - 1;
}

//Меняем название кнопки добавления периода, посвечиваем нужную кнопку выбора периода (нужно при загрузке настроек проекта), меняем тип календарей
function changeAddPeriodButton(displayType) {
    if (displayType === "range") {
        $("#add-date").prop("disabled", true);
        $("#add-date").css("visibility", "hidden");
        $("#select-date").css("visibility", "hidden");
    } else {
        $("#add-date").prop("disabled", false);
        $("#add-date").css("visibility", "visible");
        $("#select-date").css("visibility", "visible");
    }
    switch (displayType) {
        case "days":
            $("#add-date")[0].value = "+ добавить день";
            break;
        case "weeks":
            $("#add-date")[0].value = "+ добавить неделю";
            break;
        case "months":
            $("#add-date")[0].value = "+ добавить месяц";
            break;
        case "range":
            $("#add-date")[0].value = "недоступно";
            break;
    }
    //
    $(".period-selector-selected").removeClass("period-selector-selected");
    buttons = $(".period-selector");
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i].dataset["period"] === displayType) {
            $(buttons[i]).addClass("period-selector-selected");
            break;
        }
    }
    //
    changeCalendar("#select-date", displayType);
    changeCalendar("#select-date-from", displayType);
    changeCalendar("#select-date-to", displayType);
}

//формируем строку периода в удобочитаемом виде на основании крайних дат и типа периода
//используется для (пост)форматирования обозначения периода (день, месяц, неделя...)
function periodSign(periodType, dateStart, dateStop) {
    //формируем строку из объекта даты в соотвествии с переданными опциями
    //используется вместо toLocaleDateString, чтобы работало нормально в разных браузерах
    //варианты использования жестко граничены, т.к. нужна только для узкой задачи
    function dateString(date, options) {
        var result = "";
        var monthArr = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
        var weekArr = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
        if (options.weekday === "short")
            result = result + weekArr[date.getDay()] + ", ";
        if (options.day === "numeric")
            result = result + date.getDate() + " ";
        if (options.month === "short")
            result = result + monthArr[date.getMonth()] + " ";
        if (options.year === "numeric")
            result = result + date.getFullYear();
        return result;
    }

    //если передавали даты в виде строки, преобразуем в объекты
    if (typeof (dateStart) === "string")
        dateStart = strToDate(dateStart);
    if (typeof (dateStop) === "string")
        dateStop = strToDate(dateStop);
    //в зависимости от типа периода формируем строку
    switch (periodType) {
        case "day":
            var optionsStart = {
                weekday: 'short',
                year: 'numeric',
                month: 'short',
                day: 'numeric'
            };
            var sign = dateString(dateStart, optionsStart);
            break;
        case "week":
        case "range":
            var optionsStop = {
                year: 'numeric',
                month: 'short',
                day: 'numeric'
            };
            var optionsStart = {
                day: 'numeric'
            };
            var startMonth = dateStart.getMonth();
            var stopMonth = dateStop.getMonth();
            var startYear = dateStart.getFullYear();
            var stopYear = dateStop.getFullYear();
            if (startMonth !== stopMonth) {
                optionsStart.month = "short";
            }
            if (startYear !== stopYear) {
                optionsStart.year = "numeric";
            }

            startStr = dateString(dateStart, optionsStart);
            stopStr = dateString(dateStop, optionsStop);

            var sign = startStr + " - " + stopStr;
            break;
        case "month":
            var optionsStart = {
                year: 'numeric',
                month: 'short'
            };
            var sign = dateString(dateStart, optionsStart);
            break;
    }

    return sign;
}

//Меняем тип календаря: выбор дней, недель или месяцев
function changeCalendar(calendarID, displayType) {
    //в зависимости от того, для какого поля мы устанавливаем календарь, выбираем значение даты для установки начального значения поля (может быть конкретная дата, неделя или месяц куда приходится эта дата)
    var defDate;
    if ((calendarID === "#select-date") || (calendarID === "#mod-date")) {
        defDate = CURDATE;
    } else if ((calendarID === "#select-date-from")) {
        defDate = strToDate(showFromDate);
    } else if ((calendarID === "#select-date-to")) {
        defDate = strToDate(showToDate);
    }
    //задаем календарь для поля
    // dataset["datepickerStartDate"] и dataset["datepickerEndDate"] используются для хранения выбранных значений периода (его начальная и конечная дата), они же используются функциями, которым надо считывать заданное значение
    //атрибут "value" нужен только чтобы показывать красиво отформатировынную строку для удобства чтения
    switch (displayType) {
        case "days":
        case "range":
            $(calendarID).datepicker("destroy");
            $(calendarID).datepicker({
                changeMonth: true,
                changeYear: true,
                onSelect: function (dateText, inst) {
                    var date = $(this).datepicker('getDate');
                    $(calendarID)[0].dataset["datepickerStartDate"] = date;
                    $(calendarID)[0].dataset["datepickerEndDate"] = date;
                    $(calendarID)[0].value = periodSign("day", date);
                },
                afterShow: function () {
                    //после показа календаря опять подменим значение пола на отформатированную строку
                    var dateStart = new Date($(calendarID)[0].dataset["datepickerStartDate"]);
                    $(calendarID)[0].value = periodSign("day", dateStart);
                },
                //перез показом подменяем значение поля на читаемое календарем чтобы он переключился на текущий выбраный период (красивую отформатированную строку он не прочитает)
                beforeShow: function () {
                    var dateStart = new Date($(calendarID)[0].dataset["datepickerStartDate"]);
                    $(calendarID)[0].value = getLocaleDateStr(dateStart);
                }
            });
            $(calendarID).datepicker($.datepicker.regional["ru"]);
            //выставляем значение поля даты и устанавливаем начальные значения первого и последнего дня выбраной недели (для корректной работы обработчиков нажатия кнопок)
            $(calendarID)[0].value = periodSign("day", defDate);
            $(calendarID)[0].dataset["datepickerStartDate"] = defDate;
            $(calendarID)[0].dataset["datepickerEndDate"] = defDate;
            break;
        case "weeks":
            //Подсветка дат в календаре
            highlightDates = function () {
                window.setTimeout(function () {
                    $('.ui-datepicker-current-day a').addClass('ui-state-active');
                }, 1);
            };
            //
            $(calendarID).datepicker("destroy");
            //
            $(calendarID).datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                onSelect: function (dateText, inst) {
                    var date = $(this).datepicker('getDate');
                    weekDay = date.getDay();
                    if (weekDay === 0)
                        weekDay = 7;
                    var dateStart = new Date(date.getFullYear(), date.getMonth(), date.getDate() - weekDay + 1);
                    var dateStop = new Date(date.getFullYear(), date.getMonth(), date.getDate() - weekDay + 7);
                    $(calendarID)[0].dataset["datepickerStartDate"] = dateStart;
                    $(calendarID)[0].dataset["datepickerEndDate"] = dateStop;
                    $(calendarID)[0].value = periodSign("week", dateStart, dateStop);
                },
                beforeShowDay: function (date) {
                    var cssClass = '';
                    var dateStart = new Date($(calendarID)[0].dataset["datepickerStartDate"]);
                    var dateStop = new Date($(calendarID)[0].dataset["datepickerEndDate"]);
                    if (date >= dateStart && date <= dateStop)
                        cssClass = 'ui-datepicker-current-day';
                    return [true, cssClass];
                },
                afterShow: function () {
                    //после показа календаря опять подменим значение пола на отформатированную строку
                    var dateStart = new Date($(calendarID)[0].dataset["datepickerStartDate"]);
                    var dateStop = new Date($(calendarID)[0].dataset["datepickerEndDate"]);
                    $(calendarID)[0].value = periodSign("week", dateStart, dateStop);
                    //
                    $('.ui-datepicker-calendar tr').on('mousemove', function () {
                        $(this).find('td a').addClass('ui-state-hover');
                    });
                    $('.ui-datepicker-calendar tr').on('mouseleave', function () {
                        $(this).find('td a').removeClass('ui-state-hover');
                    });
                    highlightDates();
                },
                //перез показом подменяем значение поля на читаемое календарем чтобы он переключился на текущий выбраный период (красивую отформатированную строку он не прочитает)
                beforeShow: function () {
                    var dateStart = new Date($(calendarID)[0].dataset["datepickerStartDate"]);
                    $(calendarID)[0].value = getLocaleDateStr(dateStart);
                }
            });
            $(calendarID).datepicker($.datepicker.regional["ru"]);

            //выставляем значение поля даты и устанавливаем начальные значения первого и последнего дня выбраной недели (для корректной подсветки)
            weekDay = defDate.getDay();
            if (weekDay === 0)
                weekDay = 7;
            var dateStart = new Date(Number(defDate) - (weekDay - 1) * 24 * 60 * 60 * 1000);
            var dateStop = new Date(Number(defDate) + (7 - weekDay) * 24 * 60 * 60 * 1000);
            $(calendarID)[0].dataset["datepickerStartDate"] = dateStart;
            $(calendarID)[0].dataset["datepickerEndDate"] = dateStop;
            $(calendarID)[0].value = periodSign("week", dateStart, dateStop);
            break;
        case "months":
            //Подсветка дат в календаре
            highlightDates = function () {
                window.setTimeout(function () {
                    $('.ui-datepicker-current-day a').addClass('ui-state-active');
                }, 1);
            };
            //
            $(calendarID).datepicker("destroy");
            //
            $(calendarID).datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: false,
                selectOtherMonths: false,
                onSelect: function (dateText, inst) {
                    var date = $(this).datepicker('getDate');
                    var dateStart = new Date(date.getFullYear(), date.getMonth(), 1);
                    var dateStop = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    $(calendarID)[0].dataset["datepickerStartDate"] = dateStart;
                    $(calendarID)[0].dataset["datepickerEndDate"] = dateStop;
                    $(calendarID)[0].value = periodSign("month", dateStart);
                },
                beforeShowDay: function (date) {
                    var cssClass = '';
                    var dateStart = new Date($(calendarID)[0].dataset["datepickerStartDate"]);
                    var dateStop = new Date($(calendarID)[0].dataset["datepickerEndDate"]);
                    if (date >= dateStart && date <= dateStop)
                        cssClass = 'ui-datepicker-current-day';
                    return [true, cssClass];
                },
                afterShow: function () {
                    //после показа календаря опять подменим значение пола на отформатированную строку
                    var dateStart = new Date($(calendarID)[0].dataset["datepickerStartDate"]);
                    $(calendarID)[0].value = periodSign("month", dateStart);
                    //
                    $('.ui-datepicker-calendar').on('mousemove', function () {
                        $(this).find('td a').addClass('ui-state-hover');
                    });
                    $('.ui-datepicker-calendar').on('mouseleave', function () {
                        $(this).find('td a').removeClass('ui-state-hover');
                    });
                    highlightDates();
                },
                //перез показом подменяем значение поля на читаемое календарем чтобы он переключился на текущий выбраный период (красивую отформатированную строку он не прочитает)
                beforeShow: function () {
                    var dateStart = new Date($(calendarID)[0].dataset["datepickerStartDate"]);
                    $(calendarID)[0].value = getLocaleDateStr(dateStart);
                }
            });
            $(calendarID).datepicker($.datepicker.regional["ru"]);

            //выставляем значение поля даты и устанавливаем начальные значения первого и последнего дня выбраного месяца (для корректной подсветки)
            monthDay = defDate.getDate();
            var dateStart = new Date(Number(defDate) - (monthDay - 1) * 24 * 60 * 60 * 1000);
            var dateStop = new Date(Number(dateStart) + 32 * 24 * 60 * 60 * 1000);
            monthDay = dateStop.getDate();
            dateStop = new Date(Number(dateStop) - monthDay * 24 * 60 * 60 * 1000);
            $(calendarID)[0].dataset["datepickerStartDate"] = dateStart;
            $(calendarID)[0].dataset["datepickerEndDate"] = dateStop;
            $(calendarID)[0].value = periodSign("month", dateStart);
            break;
    }
}

//загружаем настройки проекта, обновляем данные
function loadProjectData() {
    $.post('get_project_settings.php', {project_id: projectID}, function (html) {
        settings = html.split("+");
        //
        showFromDate = TODAY;
        showToDate = TODAY;
        //если удалось прочитать данные проекта
        if (html !== "") {
            if (settings[0] !== "")
                displayType = settings[0];
            if (settings[1] !== "")
                totalType = settings[1];
            if (settings[2] !== "")
                sourceTypeID = Number(settings[2]);
            //меняем заголовки, выставляем опцию в контроле
            changeHeaders(sourceTypeID);
            //выставляем опцию в контроле
            changeTotalsControl(totalType);
            //Устанавливаем даты показывать "С" и "ПО"
            if (settings[3] !== "")
                showFromDate = settings[3];
            if (settings[4] !== "")
                showToDate = settings[4];
        }
        //меняем кнопки периода
        changeAddPeriodButton(displayType);
        //Подгружаем первую порцию данных в ДИВ
        //Загружаем инфо по источникам 
        //Итоги
        reloadSalesData();
        reloadSources();
        reloadTotals();
    }, 'html');
}

//сохраняем настройки пользователя
function saveUserSettings() {
    var chartsOrderJSON = JSON.stringify(chartsOrder);
    //
    var cpPos = chartsPanelSwitchPos;
    if (cpPos > 1)
        cpPos = 1;
    //
    $.post('mod_user_settings.php', {project_id: projectID, charts_panel_switch_pos: cpPos, charts_order: chartsOrderJSON}, function (html) {
    }, 'html');
}

//сохраняем настройки проекта
function saveProjectSettings() {
    $.post('mod_project_settings.php', {project_id: projectID, display_type: displayType, total_type: totalType, source_type_id: sourceTypeID, show_from_date: showFromDate, show_to_date: showToDate}, function (html) {
    }, 'html');
}

//экспорт таблицы в файл (для открытия в экселе)
//откопано в инете
var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)));
            }
    , format = function (s, c) {
        return s.replace(/{(\w+)}/g, function (m, p) {
            return c[p];
        });
    };
    return function (table, name) {
        if (!table.nodeType)
            table = document.getElementById(table);
        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
        window.location.href = uri + base64(format(template, ctx));
    };
})();

//панель графиков, установить положение полностью спрятана
function chartPanelSetHidden() {
    var height = $(window).height() - 80;
    $("#charts-panel").css("height", height);
    var width = 0;
    $("#charts-panel").animate({'width': width}, {queue: false, duration: 500, easing: easingType});
    chartsPanelSwitchPos = 0;
    $("#charts-panel-switch-hide").addClass("hidden");
    $("#charts-panel-reload").addClass("hidden");
    $("#charts-panel-switch-show").removeClass("hidden");
    //сохраняем настройки пользователя
    saveUserSettings();
}

//панель графиков, установить положение с правого края
function chartPanelSetSmall() {
    $("#charts-panel-switch-hide").removeClass("hidden");
    $("#charts-panel-reload").removeClass("hidden");
    $("#charts-panel-switch-show").removeClass("hidden");
    $(".chart-block-up").removeClass("hidden");
    $(".chart-block-down").removeClass("hidden");
    //перестраиваем панель
    $("#charts-panel").addClass("non-visible");
    var block;
    for (var i = 0; i < chartsOrder.length; i++) {
        block = $("#chart-box-" + chartsOrder[i].type);
        $("#charts-area").append(block);
    }
    $(".chart-block").removeClass("inline-block");
    $(".chart-group").remove();
    $("#charts-panel").removeClass("non-visible");
    //
    var height = $(window).height() - 80;
    $("#charts-panel").css("height", height);
    //var width = Math.round($(window).width() * 0.25) + 10;
    var width = Math.round((($(window).width() - 1280) / 2 + (1280 - 140)) / 4) + 10;
    $("#charts-panel").animate({'width': width}, {queue: false, duration: 500, easing: easingType});
    chartsPanelSwitchPos = 1;
    //сохраняем настройки пользователя
    saveUserSettings();
}

//панель графиков, установить положение полностью раскрыта
function chartPanelSetFull() {
    $("#charts-panel-switch-hide").removeClass("hidden");
    $("#charts-panel-reload").removeClass("hidden");
    $("#charts-panel-switch-show").addClass("hidden");
    $(".chart-block-up").addClass("hidden");
    $(".chart-block-down").addClass("hidden");
    //перестраиваем панель
    $("#charts-panel").addClass("non-visible");
    var block;
    $(".chart-block").addClass("inline-block");
    //группы
    $(".chart-group").remove();

    group = $("<div class='chart-group'><div class='chart-group-title'>Общие графики</div></div>");
    $("#charts-area").append(group);
    block = $("#chart-box-displays");
    group.append(block);
    block = $("#chart-box-clicks");
    group.append(block);
    block = $("#chart-box-leads");
    group.append(block);
    block = $("#chart-box-customers");
    group.append(block);
    block = $("#chart-box-income");
    group.append(block);
    block = $("#chart-box-profit");
    group.append(block);
    block = $("#chart-box-cost");
    group.append(block);
    block = $("#chart-box-roi");
    group.append(block);

    group = $("<div class='chart-group'><div class='chart-group-title'>Дополнительные метрики</div></div>");
    $("#charts-area").append(group);
    block = $("#chart-box-add_avgbill");
    group.append(block);
    block = $("#chart-box-add_profitability");
    group.append(block);
    block = $("#chart-box-add_arppu");
    group.append(block);
    block = $("#chart-box-add_arpu");
    group.append(block);

    group = $("<div class='chart-group'><div class='chart-group-title'>Стоимость на разных этапах</div></div>");
    $("#charts-area").append(group);
    block = $("#chart-box-cost_display");
    group.append(block);
    block = $("#chart-box-cost_click");
    group.append(block);
    block = $("#chart-box-cost_lead");
    group.append(block);
    block = $("#chart-box-cost_customer");
    group.append(block);

    /* пока закомментировано, потому что эти графики временно исключены
     group = $("<div class='chart-group'><div class='chart-group-title'>Зависимость показателей</div></div>");
     $("#charts-area").append(group);
     block = $("#chart-box-mix_income_profit");
     group.append(block);
     block = $("#chart-box-mix_income_cost");
     group.append(block);
     block = $("#chart-box-mix_profit_cost");
     group.append(block);
     block = $("#chart-box-mix_income_clicks");
     group.append(block);
     block = $("#chart-box-mix_income_leads");
     group.append(block);
     block = $("#chart-box-mix_income_customers");
     group.append(block);
     block = $("#chart-box-mix_income_avgbill");
     group.append(block);
     block = $("#chart-box-mix_clicks_cost");
     group.append(block);
     block = $("#chart-box-mix_clickcost_clicks");
     group.append(block);
     block = $("#chart-box-mix_clicks_leads");
     group.append(block);
     block = $("#chart-box-mix_leads_customers");
     group.append(block);
     block = $("#chart-box-mix_customercost_arppu");
     group.append(block);
     block = $("#chart-box-mix_clickcost_arpu");
     group.append(block);*/

    group = $("<div class='chart-group'><div class='chart-group-title'>Конверсии</div></div>");
    $("#charts-area").append(group);
    block = $("#chart-box-cv_displays_clicks");
    group.append(block);
    block = $("#chart-box-cv_clicks_leads");
    group.append(block);
    block = $("#chart-box-cv_leads_customers");
    group.append(block);
    block = $("#chart-box-cv_clicks_customers");
    group.append(block);

    /* пока закомментировано, потому что эти графики временно исключены
     group = $("<div class='chart-group'><div class='chart-group-title'>Влияние масштаба на эффективность</div></div>");
     $("#charts-area").append(group);
     block = $("#chart-box-mix_clicks_cv1");
     group.append(block);
     block = $("#chart-box-mix_leads_cv2");
     group.append(block);
     block = $("#chart-box-mix_customers_avgbill");
     group.append(block);
     block = $("#chart-box-mix_customers_profitability");
     group.append(block);*/

    group = $("<div class='chart-group'><div class='chart-group-title'>Итоги</div></div>");
    $("#charts-area").append(group);
    block = $("#chart-box-total_displays");
    group.append(block);
    block = $("#chart-box-total_clicks");
    group.append(block);
    block = $("#chart-box-total_leads");
    group.append(block);
    block = $("#chart-box-total_customers");
    group.append(block);
    block = $("#chart-box-total_income");
    group.append(block);
    block = $("#chart-box-total_profit");
    group.append(block);
    block = $("#chart-box-total_cost");
    group.append(block);
    block = $("#chart-box-total_avgbill");
    group.append(block);

    $("#charts-panel").removeClass("non-visible");
    //
    var height = $(window).height() - 80;
    $("#charts-panel").css("height", height);
    if ($(window).width() > 1280) {
        var width = ($(window).width() - 1280) / 2 + (1280 - 140);
    } else {
        var width = ($(window).width() - 140);
    }
    $("#charts-panel").animate({'width': width}, {queue: false, duration: 500, easing: easingType});
    chartsPanelSwitchPos = 2;
    //сохраняем настройки пользователя
    saveUserSettings();
}

//пересоздать блоки графиков на панели (под текущие высоту и ширину)
function recreateChartBlocks() {
    function appendBlock(type, title, number, chartWidth, chartHeight, columnNameStr) {
        var newChartBlock = $("<div id='chart-box-" + type + "' class='chart-block' data-chart_type='" + type + "' data-chart_number='" + number + "'><div class='chart-title'>" + title + "</div><div id='chart-container-" + type + "' class='chart-container' data-column_name_str='" + columnNameStr + "'></div><div class='chart-block-up'></div><div class='chart-block-down'></div><div class='chart-block-enlarge'></div></div>");
        $("#charts-area").append(newChartBlock);
        $(newChartBlock).css("width", chartWidth);
        $(newChartBlock).css("height", chartHeight);
    }

    $("#charts-panel").addClass("non-visible");
    //высота
    var height = $(window).height() - 80;
    $("#charts-panel").css("height", height);
    $("#charts-area").empty();
    //var chartWidth = Math.round($(window).width() * 0.25) - 15;
    var chartWidth = Math.round((($(window).width() - 1280) / 2 + (1280 - 140)) / 4) - 15;
    var chartHeight = Math.round(height * 0.25);
    var newChartBlock;
    //создаем и размещаем
    var title;
    for (var i = 0; i < chartsOrder.length; i++) {
        //строим заголовок
        switch (sourceTypeID) {
            case 1:
                title = chartsOrder[i].title1;
                break;
            case 2:
                title = chartsOrder[i].title2;
                break;
            case 3:
                title = chartsOrder[i].title3;
                break;
            case 4:
                title = chartsOrder[i].title4;
                break;
        }
        //разбор строки, подстановка имен колонок
        var columnNum;
        var columnStr;
        var column;
        var index1, index2;
        var columnNameStr = "";
        index1 = title.indexOf("{");
        while (title.indexOf("{") > -1) {
            index2 = title.indexOf("}");
            if (index2 > -1) {
                columnNum = title.substr(index1 + 1, index2 - index1 - 1);
                column = null;
                columnStr = "";
                try {
                    column = $("#header-table #" + "column-" + columnNum)[0];
                } catch (e) {
                }
                if (column !== null) {
                    columnStr = column.innerHTML;
                }
                title = title.substr(0, index1) + "<b>" + columnStr + "</b>" + title.substr(index2 + 1);
                columnNameStr = columnNameStr + columnStr + "+";
            } else {
                title = title.substr(0, index1) + title.substr(index1 + 1);
            }
            index1 = title.indexOf("{");
        }
        //
        appendBlock(chartsOrder[i].type, title, chartsOrder[i].number, chartWidth, chartHeight, columnNameStr);
    }
    //
    $("#charts-panel").removeClass("non-visible");
}

//обработка кликов по графикам
function addChartClickHandler() {
    $(".chart-block").unbind("click");
    $(".chart-block-up").unbind("click");
    $(".chart-block-down").unbind("click");
    //клик по самому графику
    $(".chart-block").click(function () {
        //пересоздаем контейнер графика
        $("#chart-full-form").empty();
        var columnNameStr = $(this).children(".chart-container")[0].dataset['column_name_str'];
        var container = $("<div id='chart-full-container' data-column_name_str='" + columnNameStr + "'></div>");
        $("#chart-full-form").append(container);
        //задаем параметры чтоб диалог был на все окно и открываем его
        var width = $(window).width();
        var height = $(window).height();
        var chartTitle = $(this).children(".chart-title")[0].innerHTML;
        $("#chart-full-form").dialog("option", "width", width);
        $("#chart-full-form").dialog("option", "height", height);
        $("#chart-full-form").dialog("open");
        $(".ui-dialog").css("top", "0px");
        $(".ui-dialog").css("left", "0px");
        $(".ui-dialog-title").empty();
        $(".ui-dialog-title").prepend(chartTitle);
        var chartType = this.dataset['chart_type'];
        //рисуем график
        $.post('get_chart_data.php', {chart_type: chartType, project_id: projectID, display_type: displayType, source_type_id: sourceTypeID, show_from_date: showFromDate, show_to_date: showToDate, sources_total: chartsSourcesTotal}, function (html) {
            var data = JSON.parse(html);
            drawChart(chartType, data, "chart-full-container", "full", "");
        });

    });
    //клик по кнопке вверх, меняем блоки местами
    $(".chart-block-up").click(function (event) {
        var block = $(this).parent('div')[0];
        var prevBlock = $(block).prev()[0];
        if (prevBlock !== undefined) {
            $(block).insertBefore(prevBlock);
            //меняем местами записи в массиве
            var number = block.dataset['chart_number'];
            for (var i = 0; i < chartsOrder.length; i++) {
                if (chartsOrder[i].number === number) {
                    if (i > 0) {
                        var prevRec = chartsOrder[i - 1];
                        chartsOrder[i - 1] = chartsOrder[i];
                        chartsOrder[i] = prevRec;
                    }
                    break;
                }
            }
        }
        //сохраняем
        saveUserSettings();
        //
        event.stopPropagation();
    });
    //клик по кнопке вниз, меняем блоки местами
    $(".chart-block-down").click(function (event) {
        var block = $(this).parent('div')[0];
        var nextBlock = $(block).next()[0];
        if (nextBlock !== undefined) {
            $(block).insertAfter(nextBlock);
            //меняем местами записи в массиве
            var number = block.dataset['chart_number'];
            for (var i = 0; i < chartsOrder.length; i++) {
                if (chartsOrder[i].number === number) {
                    if (i < chartsOrder.length - 1) {
                        var nextRec = chartsOrder[i + 1];
                        chartsOrder[i + 1] = chartsOrder[i];
                        chartsOrder[i] = nextRec;
                    }
                    break;
                }
            }
        }
        //сохраняем
        saveUserSettings();
        //
        event.stopPropagation();
    });
}

//перезагрузить графики на панели
function reloadChartsPanel() {
    //ждем пока загрузятся заголовки столбцов
    if (finishedChangingHeaders) {
        //пересоздать блоки
        recreateChartBlocks();
        //вешаем обработчики
        addChartClickHandler();
        //перезадаем вид панели
        switch (chartsPanelSwitchPos) {
            case 0:
                chartPanelSetHidden();
                break;
            case 1:
                chartPanelSetSmall();
                break;
            case 2:
                chartPanelSetFull();
                break;
        }
        //грузим графики
        var containerName;
        var chartTypes = JSON.stringify(chartsOrder);
        $.post('get_all_charts_data.php', {chart_types: chartTypes, project_id: projectID, display_type: displayType, source_type_id: sourceTypeID, show_from_date: showFromDate, show_to_date: showToDate, sources_total: chartsSourcesTotal}, function (html) {
            var dataArr = JSON.parse(html);
            for (var i = 0; i < chartsOrder.length; i++) {
                containerName = "chart-container-" + chartsOrder[i].type;
                drawChart(chartsOrder[i].type, dataArr[i], containerName, "small", "");
            }
        });
    } else {
        setTimeout(reloadChartsPanel, 100);
    }
}

//получить данные для графика и отрисовать в нужном диве
function drawChart(chartType, data, chartBox, chartSize, chartTitle) {
    var container = document.getElementById(chartBox);
    if ((data.cols.length > 1) && (data.rows[0] !== null)) {
        //если первая колонка - дата, преобразуем значения из строки в объект даты
        if (data.cols[0].type === "date") {
            for (var i = 0; i < data.rows.length; i++) {
                data.rows[i].c[0].v = strToDateForPeriods(data.rows[i].c[0].v);
            }
        }
        //для некоторых типов графиков переименивываем колонки в соответствии с массивом хранящимся в ДИВе-контейнере
        if ((chartType === "mix_income_profit") || (chartType === "mix_income_cost") || (chartType === "mix_profit_cost") || (chartType === "mix_income_clicks") || (chartType === "mix_income_leads") || (chartType === "mix_income_customers") || (chartType === "mix_income_avgbill") || (chartType === "mix_clicks_cost") || (chartType === "mix_clickcost_clicks") || (chartType === "mix_clicks_leads") || (chartType === "mix_leads_customers") || (chartType === "mix_customercost_arppu") || (chartType === "mix_clickcost_arpu") || (chartType === "mix_clicks_cv1") || (chartType === "mix_leads_cv2") || (chartType === "mix_customers_avgbill") || (chartType === "mix_customers_profitability")) {
            var columnNameStr = container.dataset['column_name_str'];
            var columnNameArr = columnNameStr.split("+");
            for (var i = 0; i < columnNameArr.length - 1; i++) {
                data.cols[i + 1].label = columnNameArr[i];
            }
            if ((chartType === "mix_income_avgbill") || (chartType === "mix_customers_avgbill")) {
                data.cols[2].label = "Средний чек";
            } else if (chartType === "mix_customers_profitability") {
                data.cols[2].label = "Рентабельность";
            } else if (chartType === "mix_customercost_arppu") {
                data.cols[2].label = "ARPPU";
            } else if (chartType === "mix_clickcost_arpu") {
                data.cols[2].label = "ARPU";
            }
        }
        //
        var dataTable = new google.visualization.DataTable(data);
        switch (chartSize) {
            case "full":
                var chartOptions = {
                    title: chartTitle,
                    pointSize: 5,
                    hAxis: {
                        gridlines: {
                            count: 10
                        }
                    },
                    vAxis: {
                        gridlines: {
                            count: 10
                        }
                    }
                };
                break;
            case "small":
            default:
                var chartOptions = {
                    legend: {
                        position: 'none'
                    },
                    hAxis: {
                        gridlines: {
                            count: 5
                        }
                    },
                    vAxis: {
                    },
                    chartArea: {
                        width: '75%',
                        height: '75%',
                        left: '20%',
                        top: '5%'
                    },
                    pointSize: 3
                };
                break;
        }
        //различные доп параметры для разных типов графиков
        if ((chartType === "income") || (chartType === "profit") || (chartType === "cost") || (chartType === "cost_display") || (chartType === "cost_click") || (chartType === "cost_lead") || (chartType === "cost_customer") || (chartType === "add_avgbill") || (chartType === "add_arppu") || (chartType === "add_arpu")) {
            chartOptions.vAxis.format = 'currency';
            //подсказки
            for (var i = 0; i < data.rows.length; i++) {
                for (var j = 1; j < data.rows[i].c.length; j++) {
                    data.rows[i].c[j].f = Number.parseFloat(data.rows[i].c[j].f).toFixed(2) + " руб.";
                }
            }
        }
        if ((chartType === "roi") || (chartType === "cv_displays_clicks") || (chartType === "cv_clicks_leads") || (chartType === "cv_leads_customers") || (chartType === "cv_clicks_customers") || (chartType === "mix_income_profit") || (chartType === "mix_income_cost") || (chartType === "mix_profit_cost") || (chartType === "mix_income_clicks") || (chartType === "mix_income_leads") || (chartType === "mix_income_customers") || (chartType === "mix_income_avgbill") || (chartType === "mix_clicks_cost") || (chartType === "mix_clickcost_clicks") || (chartType === "mix_clicks_leads") || (chartType === "mix_leads_customers") || (chartType === "mix_customercost_arppu") || (chartType === "mix_clickcost_arpu") || (chartType === "mix_clicks_cv1") || (chartType === "mix_leads_cv2") || (chartType === "mix_customers_avgbill") || (chartType === "mix_customers_profitability") || (chartType === "add_profitability")) {
            chartOptions.vAxis.format = 'percent';
            //подсказки
            if ((chartType === "roi") || (chartType === "cv_displays_clicks") || (chartType === "cv_clicks_leads") || (chartType === "cv_leads_customers") || (chartType === "cv_clicks_customers") || (chartType === "add_profitability")) {
                for (var i = 0; i < data.rows.length; i++) {
                    for (var j = 1; j < data.rows[i].c.length; j++) {
                        data.rows[i].c[j].f = (Number.parseFloat(data.rows[i].c[j].f) * 100).toFixed(3) + "%";
                    }
                }
            }
            //подсказки
            if ((chartType === "mix_income_profit") || (chartType === "mix_income_cost") || (chartType === "mix_profit_cost") || (chartType === "mix_income_clicks") || (chartType === "mix_income_leads") || (chartType === "mix_income_customers") || (chartType === "mix_income_avgbill") || (chartType === "mix_clickcost_clicks") || (chartType === "mix_customercost_arppu") || (chartType === "mix_clickcost_arpu")) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i].c[1].f = (Number.parseFloat(data.rows[i].c[1].f)).toFixed(2) + " руб.";
                }
            }
            //подсказки
            if ((chartType === "mix_income_profit") || (chartType === "mix_income_cost") || (chartType === "mix_profit_cost") || (chartType === "mix_income_avgbill") || (chartType === "mix_clicks_cost") || (chartType === "mix_customercost_arppu") || (chartType === "mix_clickcost_arpu") || (chartType === "mix_customers_avgbill")) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i].c[2].f = (Number.parseFloat(data.rows[i].c[2].f)).toFixed(2) + " руб.";
                }
            }
            //подсказки
            if ((chartType === "mix_clicks_cv1") || (chartType === "mix_leads_cv2") || (chartType === "mix_customers_profitability")) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i].c[2].f = (Number.parseFloat(data.rows[i].c[2].f) * 100).toFixed(3) + "%";
                }
            }
        }

        if (data.cols[0].type === "date") {
            chartOptions.hAxis.format = 'd MMM';
        }
        if ((chartType === "total_displays") || (chartType === "total_clicks") || (chartType === "total_leads") || (chartType === "total_customers") || (chartType === "total_income") || (chartType === "total_profit") || (chartType === "total_cost") || (chartType === "total_avgbill")) {
            chartOptions.pieHole = 1 / 3;
        }
        //устанавливаем тип графика
        switch (chartType) {
            //для итоговых диаграмм (Pie Chart)
            case "total_displays":
            case "total_clicks":
            case "total_leads":
            case "total_customers":
            case "total_income":
            case "total_profit":
            case "total_cost":
            case "total_avgbill":
                //проверяем на наличие отрицательных значений
                var hasNegativeVal = false;
                for (i=0; i<data.rows.length; i++){
                    if (data.rows[i].c[1].v < 0){
                        hasNegativeVal = true;
                        break;
                    }
                }
                
                //если есть отрицательные значения, то показываем результат в виде столбчатой диаграммы
                if (hasNegativeVal) {
                    //для уменьшенного вида не показываем подписи столбцов
                    if (chartSize === "small") {
                        chartOptions.hAxis.textPosition = "none";
                    }
                    var chart = new google.visualization.ColumnChart(container);
                } 
                //иначе выводим круговую диаграмму 
                else {
                    //очень странный глюк гугловской библиотеки. при преобразовании напрямую из объекта data график кривой
                    dataTable = new google.visualization.DataTable();
                    dataTable.addColumn('string', 'Источник');
                    dataTable.addColumn('number', 'Показатель');
                    for (var j = 0; j < data.rows.length; j++) {
                        dataTable.addRows([
                            [data.rows[j].c[0].v, Number.parseFloat(data.rows[j].c[1].v)]
                        ]);
                    }
                    //
                    if (chartSize === "small") {
                        chartOptions.legend = {};
                        chartOptions.chartArea = {
                            width: '90%',
                            height: '90%',
                            left: '5%',
                            top: '5%'
                        };
                    }
                    //
                    var chart = new google.visualization.PieChart(container);
                }
                break;
            //для графиков Timeline
            case "displays":
            case "clicks":
            case "leads":
            case "customers":
            case "income":
            case "profit":
            case "cost":
            case "roi":
            case "cost_display":
            case "cost_click":
            case "cost_lead":
            case "cost_customer":
            case "cv_displays_clicks":
            case "cv_clicks_leads":
            case "cv_leads_customers":
            case "cv_clicks_customers":
            case "add_avgbill":
            case "add_profitability":
            case "add_arppu":
            case "add_arpu":
                var chart = new google.visualization.LineChart(container);
                break;
            case "mix_income_profit":
            case "mix_income_cost":
            case "mix_profit_cost":
            case "mix_income_clicks":
            case "mix_income_leads":
            case "mix_income_customers":
            case "mix_income_avgbill":
            case "mix_clicks_cost":
            case "mix_clickcost_clicks":
            case "mix_clicks_leads":
            case "mix_leads_customers":
            case "mix_customercost_arppu":
            case "mix_clickcost_arpu":
            case "mix_clicks_cv1":
            case "mix_leads_cv2":
            case "mix_customers_avgbill":
            case "mix_customers_profitability":
                var chart = new google.visualization.AreaChart(container);
                break;
        }
        chart.draw(dataTable, chartOptions);
    }
}

//********************************************************
//Когда загрузили документ
$(document).ready(function () {
    //если есть данные о последнем открытом проекте отмечаем его как выбранный(иначе выбран будет первый созданный)	
    if (projectID !== 0) {
        $(".project-selected").removeClass("project-selected");
        projectDivsArr = $(".project");
        for (var i = 0; i < projectDivsArr.length; i++) {
            if (projectDivsArr[i].dataset["project_id"] === projectID) {
                $(projectDivsArr[i]).addClass("project-selected");
            }
        }
        //загружаем настройки проекта, обновляем данные
        loadProjectData();
    }
    //иначе задаем формат календарей (чтоб красиво смотрелось)
    //и показывем видеоинструкцию	
    else {
        changeAddPeriodButton(displayType);
        $("#sales-data").empty();
        var videoCode = $("#help-video")[0].innerHTML;
        $("#sales-data").append(videoCode);
        //скрываем ДИВ подгрузки данных
        $("#sales-data-add").fadeOut(0);
    }
    //Добавляем обработчик клика по ДИВу проекта
    addProjectClickHandler();

    //Вешаем календарь на поле ввода для изменения даты
    changeCalendar("#mod-date", "days");

    // Вешаем на событие прокрутки окна события 
    $(window).scroll(function () {
        //вниз - подгрузку данных в конец ДИВа, если скроллбар дошел до низа
        if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
            if (haveBottomDataToLoad)
                appendBottomSalesData(curentProcessDate);
        }
        //по горизонтали - сдвигаем части таблиц так чтобы первый столбец оставался на месте(по горизонтали)
        var shiftLeft = $(window).scrollLeft();
        //таблицы в голове страницы, сдвигаем сами таблицы, т.к. у элементов position: fixed
        //на месте остается первый столбец, все остальное сдвигается скриптом
        $(".horiz-floating-table-part").css("left", "-" + shiftLeft + "px");
        $(".horiz-floating-table").css("left", "-" + shiftLeft + "px");
        //таблицы данных, все сдвигается как нужно вместе с документом, но поверх основной таблицы наложена копия первого столбца, которая сдвигается, оставаясь в левой части экрана, создавая эффект неподвижного по горизонтали столбца
        $(".sales-data-table-header-container").css("left", shiftLeft + "px");
        //
        $(".period-date").css("left", shiftLeft + "px");
        //также сдвигаем шапку и низ страницы
        $("#select_box").css("left", "-" + shiftLeft + "px");
        $("#header_box").css("left", "-" + shiftLeft + "px");
        $("#bottom-fix").css("left", "-" + shiftLeft + "px");
    });

    //При клике по ДИВу внизу данных по продажам подгружаем очередную порцию, может заменять пролистывание вниз
    $("#sales-data-add").click(function () {
        if (haveBottomDataToLoad)
            appendBottomSalesData(curentProcessDate);
    });

    //Вешаем на кнопку
    //Добавляем в начало ДИВа выбранный пользователем период. Если он уже есть в базе, то ничего не добавится
    $("#add-date").click(function () {
        dateStr = $("#select-date")[0].dataset["datepickerEndDate"];
        addDate = getLocaleDateStr(new Date(dateStr));
        addSalesData(addDate);
    });

    //Вешаем на кнопку
    //Изменение диапазона отображения
    //Перезагрузка данных
    $("#change-show-dates").click(function () {
        var dateStr;
        dateStr = $("#select-date-from")[0].dataset["datepickerStartDate"];
        showFromDate = getLocaleDateStr(new Date(dateStr));
        dateStr = $("#select-date-to")[0].dataset["datepickerEndDate"];
        showToDate = getLocaleDateStr(new Date(dateStr));
        //Сохраняем настройки проекта
        saveProjectSettings();
        //Перезагрузка
        reloadSalesData();
        reloadTotals();
    });

    //При изменении периода отображения перезагружаем основной ДИВ с данными
    $(".period-selector ").click(function () {
        if (!$(this).hasClass("period-selector-selected")) {
            $(".period-selector-selected").removeClass("period-selector-selected");
            $(this).addClass("period-selector-selected");
            displayType = this.dataset["period"];
            //перезагрузка
            reloadSalesData();
            //Меняем тип итогов при необходимости, перезагружаем итоги
            switch (displayType) {
                case "weeks":
                    if ((totalType === "week") || (totalType === "7days")) {
                        totalType = "month";
                    }
                    break;
                case "months":
                    if ((totalType === "week") || (totalType === "7days") || (totalType === "month") || (totalType === "30days")) {
                        totalType = "year";
                    }
                    break;
            }
            reloadTotals();
            //Меняем название кнопки, меняем тип календаря
            changeAddPeriodButton(displayType);
            //сохраняем настройки проекта
            saveProjectSettings();
        }
    });

    //При выборе другого типа источника перезагружаем данные, набор источников
    //Будет показан другой набор для того же проекта
    $("#select-source-type").change(function () {
        switch (this.value) {
            case "inet":
                sourceTypeID = 1;
                break;
            case "email":
                sourceTypeID = 2;
                break;
            case "phone":
                sourceTypeID = 3;
                break;
            case "misc":
                sourceTypeID = 4;
                break;
        }
        //меняем заголовки
        changeHeaders(sourceTypeID);
        //
        reloadSources();
        reloadSalesData();
        reloadTotals();
        //сохраняем настройки проекта
        saveProjectSettings();
    });

    //Вешаем появление формы добавления проекта на клик
    $("#add-project").click(function () {
        $("#add-project-form").dialog("option", "position", {my: "left bottom", at: "left bottom", of: this});
        $("#add-project-form").dialog("open");
    });

    //При нажатии кнопки создаем новый проект
    $("#create-project-button").click(function () {
        addProject($("#create-project-name")[0].value, $("#create-project-comment")[0].value);
    });

    //При нажатии кнопки изменяем данные проекта, изменяем текст в ДИВе
    $("#modify-project-button").unbind("click");
    $("#modify-project-button").click(function () {
        projectName = $("#modify-project-name")[0].value;
        projectComment = $("#modify-project-comment")[0].value;
        divProj = $(".project-selected")[0];
        $.post('mod_project.php', {project_id: projectID, project_name: projectName, project_comment: projectComment}, function (html) {
            divProj.innerHTML = projectName;
            $("#mod-project-form").dialog("close");
        }, 'html');
    });

    //обработка кликов на удаление проекта
    //Убираем существующие обработчики
    var formID = "#mod-project-form";
    $(formID + " .del-icon").unbind("click");
    $("#del-project-button").unbind("click");
    //клик по иконке удаления
    $(formID + " .del-icon").click(function () {
        formID = "#mod-project-form";
        if (!timerInProgress[formID]) {
            //показываем блок с подтверждением удаления
            $("#del-project-confirm")[0].checked = false;
            $(formID + " .del-confirmation-block").fadeIn(0);
            //запускаем таймер	
            timerInProgress[formID] = true;
            hideTimerDelBlock(15, formID);
        }
    });
    //клик по кнопке подтверждения удаления
    $("#del-project-button").click(function () {
        if ($("#del-project-confirm")[0].checked) {
            var formID = "#mod-project-form";
            $.post('del_project.php', {project_id: projectID}, function (html) {
                if (html === "sucess") {
                    //скрываем блок подтверждения 
                    $(formID + " .del-confirmation-block").fadeOut(0);
                    timerInProgress[formID] = false;
                    //скрываем диалог 
                    $(formID).dialog("close");

                    //перезагружаем данные
                    $('#projects').empty();
                    $('#sales-data').empty();
                    //Загружаем новый список проектов
                    $.post('display_projects.php', {reload: "yes"}, function (html) {
                        $('#projects').append(html);
                        //обработка кликов
                        addProjectClickHandler();
                        //Перезагружаем данные нового выбраного проекта
                        loadProjectData();
                    }, 'html');
                }
            }, 'html');
        }
    });

    //клик по кнопке изменения даты
    $("#modify-date-button").click(function () {
        var formID = "#date-form";
        var oldDate = this.parentElement.parentElement.dataset["date"];
        var newDate = $("#mod-date")[0].dataset["datepickerEndDate"];
        newDate = getLocaleDateStr(new Date(newDate));
        $.post('mod_date.php', {project_id: projectID, old_date: oldDate, new_date: newDate}, function (html) {
            if (html !== "") {
                //скрываем диалог 
                $(formID).dialog("close");
                //меняем атрибуты контейнера и строку с датой (могут и не измениться, если PHP скрипт не менял дату)
                var periodContainers = $(".period-container");
                for (var i = 0; i < periodContainers.length; i++) {
                    if (periodContainers[i].dataset["stop_date"] === oldDate) {
                        newDate = html;
                        periodContainers[i].dataset["start_date"] = newDate;
                        periodContainers[i].dataset["stop_date"] = newDate;
                        newDate = strToDateForPeriods(newDate);
                        $(periodContainers[i]).find(".period-date")[0].innerHTML = periodSign("day", newDate);
                        break;
                    }
                }
            }
        }, 'html');
    });

    //обработка кликов на удаление даты
    //Убираем существующие обработчики
    var formID = "#date-form";
    $(formID + " .del-icon").unbind("click");
    $("#del-date-button").unbind("click");
    //клик по иконке удаления
    $(formID + " .del-icon").click(function () {
        formID = "#date-form";
        if (!timerInProgress[formID]) {
            //показываем блок с подтверждением удаления
            $("#del-date-confirm")[0].checked = false;
            $(formID + " .del-confirmation-block").fadeIn(0);
            //запускаем таймер	
            timerInProgress[formID] = true;
            hideTimerDelBlock(15, formID);
        }
    });
    //клик по кнопке подтверждения удаления
    $("#del-date-button").click(function () {
        if ($("#del-date-confirm")[0].checked) {
            var formID = "#date-form";
            var date = this.parentElement.parentElement.dataset["date"];
            $.post('del_date.php', {project_id: projectID, date: date}, function (html) {
                if (html === "sucess") {
                    //скрываем блок подтверждения 
                    $(formID + " .del-confirmation-block").fadeOut(0);
                    timerInProgress[formID] = false;
                    //скрываем диалог 
                    $(formID).dialog("close");
                    //удаляем ДИВ со страницы
                    var periodContainers = $(".period-container");
                    for (var i = 0; i < periodContainers.length; i++) {
                        if (periodContainers[i].dataset["stop_date"] === date) {
                            $(periodContainers[i]).remove();
                            break;
                        }
                    }
                    //обновляем итоги
                    reloadTotals();
                }
            }, 'html');
        }
    });

    //Обработка кликов на переименование колонок
    $(".col-rename-btn").click(function () {
        var div = this;
        //запоминаем номер колонки
        columnNum = div.dataset['id'];
        //находим данные колонки
        $.post('get_column_names.php', {project_id: projectID, type_id: sourceTypeID}, function (html) {
            settings = html.split("\n");
            for (var i = 0; i < settings.length; i++) {
                data = settings[i].split("+");
                if (data[0] === columnNum) {
                    $("#modify-column-name").val(data[1]);
                    $("#modify-column-comment").val(data[2]);
                    break;
                }
            }
            //открываем форму
            $("#mod-column-form").dialog("option", "position", {my: "right top", at: "right top", of: div});
            $("#mod-column-form").dialog("open");
        }, 'html');
    });

    //При нажатии кнопки изменяем данные колонки, изменяем текст в ДИВах
    $("#modify-column-button").unbind("click");
    $("#modify-column-button").click(function () {
        columnName = $("#modify-column-name")[0].value;
        columnComment = $("#modify-column-comment")[0].value;
        //обновить в базе
        $.post('mod_column.php', {project_id: projectID, source_type_id: sourceTypeID, column_num: columnNum, column_name: columnName, column_comment: columnComment}, function (html) {
            //обновить на странице
            id = "#column-" + (columnNum);
            idTip = "#column-tip-" + (columnNum);
            $(id).empty();
            $(id).append(columnName);
            $(idTip).empty();
            $(idTip).append(columnComment);
            //
            $("#mod-column-form").dialog("close");
            //обновить панель графиков
            reloadChartsPanel();
        }, 'html');
    });

    //При закрытии формы с источниками данных перезагружаем данные
    function reloadAfterSourcesChange() {
        reloadSalesData();
        reloadTotals();
    }

    //Создаем диалоги jQuery UI из ДИВов для всплывающих окон (форм)
    $("#sources-form").dialog({
        autoOpen: false,
        modal: true,
        draggable: false,
        //при закрытии диалога убрать блок подтверждения удаления (чтоб не было при след открытии, а то смотрится криво)
        close: function () {
            var formID = "#sources-form";
            $(formID + " .del-confirmation-block").fadeOut(0);
            timerInProgress[formID] = false;
            //обновляем данные, итоги, графики
            reloadAfterSourcesChange();
        }
    });
    $("#comments-form").dialog({
        autoOpen: false,
        modal: true,
        draggable: false
    });
    $("#add-project-form").dialog({
        autoOpen: false,
        modal: true,
        draggable: false
    });
    $("#mod-project-form").dialog({
        autoOpen: false,
        modal: true,
        draggable: false,
        //при закрытии диалога убрать блок подтверждения удаления (чтоб не было при след открытии, а то смотрится криво)
        close: function () {
            var formID = "#mod-project-form";
            $(formID + " .del-confirmation-block").fadeOut(0);
            timerInProgress[formID] = false;
        }
    });
    $("#date-form").dialog({
        autoOpen: false,
        modal: true,
        draggable: false,
        //при закрытии диалога убрать блок подтверждения удаления (чтоб не было при след открытии, а то смотрится криво)
        close: function () {
            var formID = "#date-form";
            $(formID + " .del-confirmation-block").fadeOut(0);
            timerInProgress[formID] = false;
        }
    });
    $("#mod-column-form").dialog({
        autoOpen: false,
        modal: true,
        draggable: false
    });
    $("#chart-full-form").dialog({
        autoOpen: false,
        modal: true,
        draggable: false,
        close: function () {
            $(".ui-dialog-title").empty();
            $(".ui-dialog-title").append("&nbsp;");
        }
    });
    //делаем диалоги фиксированными в окне
    $(".ui-dialog").css("position", "fixed");

    //Вешаем на кнопку появление формы с источниками
    $("#sources-button").click(function () {
        $("#sources-form").dialog("option", "position", {my: "right top", at: "right top", of: this});
        var height = $(window).height() - 80;
        $("#sources-form").dialog("option", "maxHeight", height);
        $("#sources-form").dialog("open");
    });

    //Вешаем на кнопку добавление источника данных
    $("#sources-create").click(function () {
        addSource(sourceTypeID, $("#sources-name")[0].value);
        $("#sources-name")[0].value = "";
    });

    //Вешаем на кнопку обновление комментария по дню (и периоду отображения)
    $("#mod-comment-button").click(function () {
        date = $("#last-comment")[0].dataset["date"];
        startDate = $("#last-comment")[0].dataset["start_date"];
        stopDate = $("#last-comment")[0].dataset["stop_date"];
        comment = $("#last-comment")[0].value;
        $.post('mod_comment.php', {project_id: projectID, date: date, start_date: startDate, stop_date: stopDate, comment: comment}, function (html) {
            //пишем в ДИВ в комментарием новый консолидированный комментарий по всему периоду
            $("#period-comment-" + stopDate).empty();
            $("#period-comment-" + stopDate).append(html);
            $("#comments-form").dialog("close");
        }, 'html');
    });

    //Добавляем к Datepicker обработку события afterShow. Нужно будет для выбора недели или месяца 
    $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
    $.datepicker._updateDatepicker = function (inst) {
        $.datepicker._updateDatepicker_original(inst);
        var afterShow = this._get(inst, 'afterShow');
        if (afterShow)
            afterShow.apply((inst.input ? inst.input[0] : null));  // trigger custom callback
    };

    //Вешаем на кнопку экспорт данных
    $("#export-button").click(function () {
        $.post('display_export_table.php', {project_id: projectID, from_date: showFromDate, to_date: showToDate, display_type: displayType}, function (html) {
            var table = $(html)[0];
            tableToExcel(table, 'Excel');
        }, 'html');
    });

    // показать подсказку
    $('#header-table .column2').mouseenter(function () {
        var tipID = "#column-tip-2";
        $(tipID).fadeIn(5);
    });
    $('#header-table .column3').mouseenter(function () {
        var tipID = "#column-tip-3";
        $(tipID).fadeIn(5);
    });
    $('#header-table .column4').mouseenter(function () {
        var tipID = "#column-tip-4";
        $(tipID).fadeIn(5);
    });
    $('#header-table .column5').mouseenter(function () {
        $('#column-tip-5').fadeIn(5);
    });
    $('#header-table .column6').mouseenter(function () {
        var tipID = "#column-tip-6";
        $(tipID).fadeIn(5);
    });
    $('#header-table .column7').mouseenter(function () {
        $('#column-tip-7').fadeIn(5);
    });
    $('#header-table .column8').mouseenter(function () {
        var tipID = "#column-tip-8";
        $(tipID).fadeIn(5);
    });
    $('#header-table .column9').mouseenter(function () {
        $('#column-tip-9').fadeIn(5);
    });
    $('#header-table .column10').mouseenter(function () {
        $('#column-tip-10').fadeIn(5);
    });
    $('#header-table .column11').mouseenter(function () {
        $('#column-tip-11').fadeIn(5);
    });
    $('#header-table .column12').mouseenter(function () {
        $('#column-tip-12').fadeIn(5);
    });
    $('#header-table .column13').mouseenter(function () {
        $('#column-tip-13').fadeIn(5);
    });
    $('#header-table .column14').mouseenter(function () {
        $('#column-tip-14').fadeIn(5);
    });
    $('#header-table .column15').mouseenter(function () {
        $('#column-tip-15').fadeIn(5);
    });
    $('#header-table .column16').mouseenter(function () {
        $('#column-tip-16').fadeIn(5);
    });

    // скрыть подсказку
    $('#header-table .column2').mouseleave(function () {
        $('#column-tip-2').fadeOut(5);
    });
    $('#header-table .column3').mouseleave(function () {
        $('#column-tip-3').fadeOut(5);
    });
    $('#header-table .column4').mouseleave(function () {
        $('#column-tip-4').fadeOut(5);
    });
    $('#header-table .column5').mouseleave(function () {
        $('#column-tip-5').fadeOut(5);
    });
    $('#header-table .column6').mouseleave(function () {
        $('#column-tip-6').fadeOut(5);
    });
    $('#header-table .column7').mouseleave(function () {
        $('#column-tip-7').fadeOut(5);
    });
    $('#header-table .column8').mouseleave(function () {
        $('#column-tip-8').fadeOut(5);
    });
    $('#header-table .column9').mouseleave(function () {
        $('#column-tip-9').fadeOut(5);
    });
    $('#header-table .column10').mouseleave(function () {
        $('#column-tip-10').fadeOut(5);
    });
    $('#header-table .column11').mouseleave(function () {
        $('#column-tip-11').fadeOut(5);
    });
    $('#header-table .column12').mouseleave(function () {
        $('#column-tip-12').fadeOut(5);
    });
    $('#header-table .column13').mouseleave(function () {
        $('#column-tip-13').fadeOut(5);
    });
    $('#header-table .column14').mouseleave(function () {
        $('#column-tip-14').fadeOut(5);
    });
    $('#header-table .column15').mouseleave(function () {
        $('#column-tip-15').fadeOut(5);
    });
    $('#header-table .column16').mouseleave(function () {
        $('#column-tip-16').fadeOut(5);
    });

    //клик по перелючателю положения панели графиков - раскрыть
    $("#charts-panel-switch-show").click(function () {
        switch (chartsPanelSwitchPos) {
            case 0:
                chartsPanelSwitchPos = 1;
                reloadChartsPanel();
                break;
            case 1:
                chartPanelSetFull();
                break;
        }
    });

    //клик по перелючателю положения панели графиков - спрятать
    $("#charts-panel-switch-hide").click(function () {
        switch (chartsPanelSwitchPos) {
            case 1:
                chartPanelSetHidden();
                break;
            case 2:
                chartPanelSetSmall();
                break;
        }
    });

    //клик по ДИВу обновления панели графиков
    $("#charts-panel-reload").click(function () {
        reloadChartsPanel();
    });
    
    //клик по чекбоксу суммирования по источникам
    $("#charts-sources-total").click(function () {
        chartsSourcesTotal = $("#charts-sources-total")[0].checked;
        reloadChartsPanel();
    });

    //задаем положение панели в соответствии с сохраненным параметром пользователя
    switch (chartsPanelSwitchPos) {
        case 0:
            chartPanelSetHidden();
            break;
        case 1:
            chartPanelSetSmall();
            break;
        case 2:
            chartPanelSetFull();
            break;
    }

    //при изменении размеров окна подстраиваем панель графиков
    $(window).resize(function () {
        var height = $(window).height() - 80;
        $("#charts-panel").css("height", height);
        switch (chartsPanelSwitchPos) {
            case 1:
                var width = Math.round($(window).width() * 0.25) + 10;
                $("#charts-panel").css("width", width);
                break;
            case 2:
                var width = 0;
                if ($(window).width() > 1280) {
                    width = ($(window).width() - 1280) / 2 + (1280 - 140);
                } else {
                    width = ($(window).width() - 140);
                }
                $("#charts-panel").css("width", width);
                break;
        }
    });
});

//загрузка гугловских инструментов для графиков
google.load('visualization', '1', {packages: ['corechart']});