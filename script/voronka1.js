//********************************************************
//Когда загрузили документ
$(document).ready(function(){
	// Вешаем на событие прокрутки окна события
	$(window).scroll(function(){
		//по горизонтали - сдвигаем шапку и низ страницы
		var shiftLeft = $(window).scrollLeft();
		$("#header_box").css("left", "-" + shiftLeft + "px");
		$("#bottom-fix").css("left", "-" + shiftLeft + "px");
	});
	
	$(window).scroll();
});