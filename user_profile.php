<?php
header("Content-Type: text/html; charset=utf-8");
//Проверка на корректность адреса почты
function Email_Validation($email)
{
	if(preg_match("/[0-9a-z_\.\-]+@[0-9a-z_\.\-]+\.[a-z]{2,4}/i", $email))
	{
		$res = true;
	}
	else
	{
		$res = false;
	}
    return $res; 
}
//

include ("check_login.php");
//если не залогинен отправляем на главную
if (trim($_SESSION['email']) == "")
{
	header("Location:index.php");
	exit;
}
//считываем переданные параметры
if (isset($_POST['action'])) 
{ 
	$action = $_POST['action']; 
}
if (isset($_POST['email'])) 
{ 
	$email = $_POST['email']; 
}
if (isset($_POST['password'])) 
{ 
	$password=$_POST['password']; 
}
if (isset($_POST['old_password'])) 
{ 
	$oldPassword=$_POST['old_password']; 
}

include ("db_open.php");
//если пытаемся изменить данные (т.е. страница вызвала сама себя с параметрами)
if ($action == "change_profile")
{	
	//Защита от SQL инъекций
	if(get_magic_quotes_gpc()==1){
		$email=stripslashes(trim($email));
		$password=stripslashes(trim($password));
		$oldPassword=stripslashes(trim($oldPassword));
	} else {
		$email=trim($email);
		$password=trim($password);
		$oldPassword=trim($oldPassword);
	}
	$email=mysql_real_escape_string($email);
	$password=mysql_real_escape_string($password);
	$oldPassword=mysql_real_escape_string($oldPassword);
	$email = strip_tags($email);
	
	if ($email == '') 
	{ 
		unset($email);
	}
	if ($password =='') 
	{ 
		unset($password);
	}

	$err = 0;
	//если пустой майл и пароль, то ошибка и не пытаемся лезть в базу
	if (empty($email) && empty($password))
	{
		$err = 1;
	}
	//если почта некорректная (но непустая) - тоже
	elseif (!Email_Validation($email) && !empty($email))
	{
		$err = 2;
	}
	//иначе
	else
	{
		$oldEmail = $_SESSION['email'];
		//проверяем по базе правильность пароля
		$query = "SELECT * FROM users WHERE email='{$oldEmail}' AND pass_hash=MD5('{$oldPassword}')";
		$result = mysql_query($query); 
		$myrow = mysql_fetch_array($result);
		mysql_free_result($result);
		//если нет соответствия, отмечаем ошибку пароля
		if (empty($myrow['id']))
		{
			$err = 3;
			include ("db_close.php");
		}
		//иначе
		else 
		{
			$userID = $myrow['id'];
			//меняем только те данные, которые пользователь вводил (непустые поля)
			$query = "UPDATE users SET ";
			$setStr = "";
			if (!empty($email)){
				$setStr = $setStr . "email='{$email}'";
			}
			if (!empty($password)){
				if (!empty($setStr)){
					$setStr = $setStr . ", ";
				}
				$setStr = $setStr . "pass_hash=MD5('{$password}')";
			}
			$query = $query . $setStr . " WHERE id='{$userID}'";
			
			//Вносим изменения
			$result2 = mysql_query($query) or die('Incorrect query: ' . mysql_error());
			if ($result2) {
				//находим нужного пользователя
				$query = "SELECT * FROM users WHERE id='{$userID}'";
				$result = mysql_query($query); 
				$myrow = mysql_fetch_array($result);
				mysql_free_result($result);
				$passHash = $myrow['pass_hash'];
				//задаем данные сессии, на всякий случай обновляем все
				$_SESSION['email']=$myrow['email'];
				$_SESSION['id'] = $myrow['id'];
				$userID = $myrow['id'];
				//отправляем куки если у пользователя есть эта опция
				if ($_SESSION['save'] == "yes"){
					setcookie("user_data", $email."+".$passHash."+".$_SESSION['id'], time()+9999999);
				}
				include ("db_close.php");
				$oldPassword = "";
				$err = -1;
			}
			// если не получилось
			else {
				$err = 9;
				include ("db_close.php");
			}
		}
	}
}
//форма для редактирования
?>
<html>
	<head>
		<title>Профиль</title>
		<?php include("html_head.php");?> 
	</head>
	<body>
		<div class="container1">
			<div class="top-fix">
				<?php include("header.php");?> 
			</div>
			
			<div class="form1">
				<?php
				//если была ошибка при попытке изменения
				switch($err) 
				{
					case 1:
						echo "Оба поля пустые, нечего менять";
					break;
					case 2:
						echo "Неправильный E-mail";
					break;
					case 3:
						echo "Неверный пароль";
					break;
					case 9:
						echo "Что-то пошло не так, попробуйте еще раз";
					break;
					case -1:
						echo "Изменено успешно";
					break;
				}
				?>
				<h2>Профиль пользователя</h2>
				Укажите новые данные для изменения или оставьте поле пустым если не хотите его менять
				<form action="user_profile.php" method="post">
					<input type='text' name = 'action' value = 'change_profile' readonly hidden>
					<p>
					<label>Новый Email:<br></label>
					<input class="input1" name="email" type="email" size="20" maxlength="50" value="<?php echo "{$email}" ?>">
					</p>
					<p>
					<label>Новый пароль:<br></label>
					<input class="input1" name="password" type="password" size="20" maxlength="20" value="<?php echo "{$password}" ?>">
					</p>
					<p>
					<label>Текущий пароль:<br></label>
					<input class="input1" name="old_password" type="password" size="20" maxlength="20" value="<?php echo "{$oldPassword}" ?>">
					</p>
					<p>
					<input class="submit1" type="submit" name="submit" value="Изменить">
					</p>
				</form>
				<br>
			</div>

			<div class="bottom-fix1">
				<?php Include("footer.php");?>
			</div>
		</div>
	</body>
</html>