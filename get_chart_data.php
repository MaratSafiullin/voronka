<?php
include("get_chart_data_functon.php");

header("Content-Type: text/html; charset=utf-8");
//проверяем, что выполняется из сессии (т.е. не вызвано левым способом)
include("check_session.php");
//считываем переданные параметры
if (isset($_POST['chart_type'])){
	$chartType = $_POST['chart_type'];
}
if (isset($_POST['project_id'])){
	$projectID = $_POST['project_id'];
}
if (isset($_POST['display_type'])){
	$displayType = $_POST['display_type'];
}
if (isset($_POST['source_type_id'])){
	$sourceTypeID = $_POST['source_type_id'];
}
if (isset($_POST['show_from_date'])){
	$showFromDate = $_POST['show_from_date'];
}
if (isset($_POST['show_to_date'])){
	$showToDate = $_POST['show_to_date'];
}
if (isset($_POST['column_names'])){
	$columnNames = $_POST['column_names'];
}
if (isset($_POST['sources_total'])){
	$sourcesTotal = $_POST['sources_total'];
}
if ($sourcesTotal === "true") {
    $sourcesTotal = true;
} else {
    $sourcesTotal = false;
}

include ("db_open.php");

//Защита от SQL инъекций
$projectID = (int)$projectID;
$sourceTypeID = (int)$sourceTypeID;
if(get_magic_quotes_gpc()==1){
	$chartType=stripslashes(trim($chartType));
	$displayType=stripslashes(trim($displayType));
	$showFromDate=stripslashes(trim($showFromDate));
	$showToDate=stripslashes(trim($showToDate));
} else {
	$chartType=trim($chartType);
	$displayType=trim($displayType);
	$showFromDate=trim($showFromDate);
	$showToDate=trim($showToDate);
}
$chartType=mysql_real_escape_string($chartType);
$displayType=mysql_real_escape_string($displayType);
$showFromDate=mysql_real_escape_string($showFromDate);
$showToDate=mysql_real_escape_string($showToDate);
$chartType = strip_tags($chartType);
$displayType = strip_tags($displayType);
$showFromDate = strip_tags($showFromDate);
$showToDate = strip_tags($showToDate);

//Если вдруг номер проекта не соответствует пользователю (кто-то пытается хакнуть), завершаем выполнение
include("check_project.php");

//*******************************************
//Главная исполняемая часть начинается отсюда
//*******************************************

$chartData = getChartData($chartType, $projectID, $displayType, $sourceTypeID, $showFromDate, $showToDate, $columnNames, $sourcesTotal);

include ("db_close.php");

$jsonStr = json_encode($chartData);
echo $jsonStr;
?>