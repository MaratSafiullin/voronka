<?php
header("Content-Type: text/html; charset=utf-8");
//Проверка на корректность адреса почты
function Email_Validation($email)
{
	if(preg_match("/[0-9a-z_\.\-]+@[0-9a-z_\.\-]+\.[a-z]{2,4}/i", $email))
	{
		$res = true;
	}
	else
	{
		$res = false;
	}
    return $res; 
}

//создает случайную строку для пароля
function randomPass($length=10, $chrs = '1234567890qwertyuiopasdfghjklzxcvbnm'){
	for($i = 0; $i < $length; $i++) {
		$pwd .= $chrs{mt_rand(0, strlen($chrs)-1)};
	}
	return $pwd;
}
//

include ("check_login.php");
//если залогинен отправляем на главную
if (trim($_SESSION['email']) != "")
{
	header("Location:index.php");
	exit;
}
//считываем переданные параметры
if (isset($_POST['action'])) 
{ 
	$action = $_POST['action']; 
}
if (isset($_POST['email'])) 
{ 
	$email = $_POST['email']; 
}

include ("db_open.php");
//если пытаемся восстановить пароль (т.е. страница вызвала сама себя с параметрами)
if ($action == "login")
{	
	//Защита от SQL инъекций
	if(get_magic_quotes_gpc()==1){
		$email=stripslashes(trim($email));
	} else {
		$email=trim($email);
	}
	$email=mysql_real_escape_string($email);
	$email = strip_tags($email);
	
	if ($email == '') 
	{ 
		unset($email);
	}

	$err = 0;
	//если почта некорректная, то ошибка и не пытаемся лезть в базу
	if (!Email_Validation($email)) {
		$err = 2;
	}
	//иначе
	else {
		//проверяем по базе наличие Email
		$query = "SELECT * FROM users WHERE email='{$email}'";
		$result = mysql_query($query); 
		$myrow = mysql_fetch_array($result);
		mysql_free_result($result);
		//если нет соответствия, отмечаем ошибку пароля
		if (empty($myrow['id']))
		{
			$err = 3;
			include ("db_close.php");
		}
		//иначе
		else 
		{
			$userID = $myrow['id'];
			//задаем новый пароль
			$pass = randomPass();
			$query = "UPDATE users SET pass_hash = MD5('{$pass}') WHERE id='{$userID}'";
			//Вносим изменения
			$result2 = mysql_query($query) or die('Incorrect query: ' . mysql_error());
			if ($result2) {
				include ("db_close.php");
				//отправляем почту
				$emailText = "Новый пароль: {$pass}\n\n"."Это автоматически созданное сообщение. Не отвечайте на него.";
				$head = "From: noreply@воронка-онлайн.рф\n";
				//$head = $head."To: {$email}\n";
				$head = $head."Subject: Воронка-онлайн. Восстановление пароля\n";
				mail($email, 'Восстановление пароля', $emailText, $head);
				//
				$email = "";
				$err = -1;
			}
			// если не получилось
			else {
				$err = 9;
				include ("db_close.php");
			}
		}
	}
}
//форма для редактирования
?>
<html>
	<head>
		<title>Восстановление пароля</title>
		<?php include("html_head.php");?> 
		<?php?>
	 <? Include("header.php");?> 
	<body>
		<div class="container1">
			<div class="form1">
			<?php
			//если была ошибка при попытке изменения
			switch($err) 
			{
				case 2:
					echo "Неправильный E-mail";
				break;
				case 3:
					echo "E-mail не найден в базе";
				break;
				case 9:
					echo "Что-то пошло не так, попробуйте еще раз";
				break;
				case -1:
					echo "Новый пароль отправлен на почту";
				break;
			}
			?>
			<h2>Восстановление пароля</h2>
			<form  action="restore_password.php" method="post">
				<input type='text' name = 'action' value = 'login' readonly hidden>
				<p>
				Введите почтовый адрес и мы вышлем новый пароль.
				<p>
				<label>Email:<br></label>
				</p>
				<input class="input1"name="email" type="email" size="20" maxlength="50" value="<?php echo "{$email}" ?>">
				</p>
				<p>
				<input class="submit1" type="submit" name="submit" value="Получить новый пароль">
				</p>
				<p>
				<a href="sign_in.php" class="ref">Войти</a> 
				</p>
			</form>
			<br>
			</div>
		</div>
		<div class="bottom-fix1">
			<?php Include("footer.php");?>
		</div>
	</body>
</html>