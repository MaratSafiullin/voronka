<?php
header("Content-Type: text/html; charset=utf-8");
//проверяем, что выполняется из сессии (т.е. не вызвано левым способом)
include("check_session.php");
//считываем переданные параметры
if (isset($_POST['process_date'])){
	$processDate = $_POST['process_date'];
}
if (isset($_POST['show_from_date'])){
	$showFromDate = $_POST['show_from_date'];
}
if (isset($_POST['display_type'])){
	$displayType = $_POST['display_type'];
}
if (isset($_POST['project_id'])){
	$projectID = $_POST['project_id'];
}
if (isset($_POST['source_type_id'])){
	$sourceTypeID = $_POST['source_type_id'];
}
$displayCount = 5;
$unitsDisplayed = 0;

//Функция выводит в ХТМЛ виде сводные результаты (суммируем данные) по разным источникам клиентов для указанного периода указанного проекта
//Реально используются периоды: день, неделя, месяц, но может быть любой
include ("display_sales_data.php");

include ("db_open.php");
//Защита от SQL инъекций, тут в общем-то не для всех передаваемых параметров нужна, но пусть будет проверка 
$projectID = (int)$projectID;
$dataSourceID = (int)$dataSourceID;
if(get_magic_quotes_gpc()==1){
	$processDate=stripslashes(trim($processDate));
	$showFromDate=stripslashes(trim($showFromDate));
	$displayType=stripslashes(trim($displayType));
} else {
	$processDate=trim($processDate);
	$showFromDate=trim($showFromDate);
	$displayType=trim($displayType);
}
$processDate=mysql_real_escape_string($processDate);
$showFromDate=mysql_real_escape_string($showFromDate);
$displayType=mysql_real_escape_string($displayType);
$processDate = strip_tags($processDate);
$showFromDate = strip_tags($showFromDate);
$displayType = strip_tags($displayType);

$stopDate = $processDate;
$showFromDate = strtotime($showFromDate);

//*******************************************
//Главная исполняемая часть начинается отсюда
//*******************************************

//Если вдруг номер проекта не соответствует пользователю (кто-то пытается хакнуть), завершаем выполнение
include("check_project.php");

//Находим крайние даты записей по данному проекту
$query = "SELECT date FROM record_days WHERE project_id = '{$projectID}'";
$result = mysql_query($query);
if (mysql_num_rows($result) != 0) {
	$query = "SELECT MAX(date) FROM record_days WHERE project_id = '{$projectID}'";
	$resultMax = mysql_query($query);
	$query = "SELECT MIN(date) FROM record_days WHERE project_id = '{$projectID}'";
	$resultMin = mysql_query($query);
	$dateMax = strtotime(mysql_result($resultMax,0,'MAX(date)'));
	$dateMin = strtotime(mysql_result($resultMin,0,'MIN(date)'));
	mysql_free_result($resultMax);
	mysql_free_result($resultMin);
} 
//если нет данных по этому проекту вообще, то переключаем режим отображения (фактически ничего не показывать)
else {
	$displayType = "none";
}
mysql_free_result($result);

//Выводим периоды записей по проекту (как правило несколько периодов в обратном временном порядке, от поздних к ранним)
//В зависимости от типа запрашиваемых периодов (дни, недели, месяцы) разные варианты:
switch($displayType){
	//если данные по дням
	case "days":
		$date = strtotime($processDate);
		//выводим данные пока не отобразим нужное количество дней или не выйдем за пределы дат записей по данному проекту 
		while(($unitsDisplayed < $displayCount) && ($date >= $dateMin) && ($date >= $showFromDate)){
			//начальная и конечная дата периода совпадают
			$startDate = date('Y-m-d', $date);
			$stopDate = date('Y-m-d', $date);
			//для начала проверим есль ли вообще записи по данному дню
			$query = "SELECT date FROM record_days WHERE project_id = '{$projectID}' AND date = '{$startDate}'";
			$resultHasRecs = mysql_query($query);
			//если да, то выводим данные по дню, увеличиваем счетчик
			if (mysql_num_rows($resultHasRecs) != 0){
				displayTimeUnit($startDate, $stopDate, $projectID, $sourceTypeID, $displayType);
				$unitsDisplayed++;
			}
			mysql_free_result($resultHasRecs);
			//переходим на следующий день
			$date = $date - 3600 * 24;
		}
		//возвращаем следующий (за выведенными) период в javascript страницы, запросившей данные
		$stopDate = date('Y-m-d', $date);
	break;
	
	//если данные по неделям принцип почти тот же, что и для дней
	case "weeks":
		$date = strtotime($processDate);
		//находим последний день недели, на которую приходится указанная дата
		$weekDay = (integer) date("w" ,$date);
		if ($weekDay == 0) $weekDay = 7;
		$date = $date + 3600 * 24 * (7 - $weekDay) + 3600 * 12; //Че за херня?? почему нужно прибавлять полсуток??
		//вывод данных
		while(($unitsDisplayed < $displayCount) && ($date >= $dateMin) && ($date >= $showFromDate)){
			//Вычисляем начальную дату (пнд) и конечную (вск)
			$startDate = date('Y-m-d', $date - 3600 * 24 * 6);
			$stopDate = date('Y-m-d', $date);
			//
			$query = "SELECT date FROM record_days WHERE project_id = '{$projectID}' AND date >= '{$startDate}' AND date <= '{$stopDate}'";
			$resultHasRecs = mysql_query($query);
			if (mysql_num_rows($resultHasRecs) != 0){
				displayTimeUnit($startDate, $stopDate, $projectID, $sourceTypeID, $displayType);
				$unitsDisplayed++;
			}
			mysql_free_result($resultHasRecs);
			//вычисляем последний день следующей недели
			$date = $date - 3600 * 24 * 7;
		}
		//возвращаем следующий (за выведенными) период в javascript страницы, запросившей данные
		$stopDate = date('Y-m-d', $date);
	break;
	
	//если данные по месяцам принцип почти тот же, что и для дней
	case "months":
		$date = strtotime($processDate);
		//Находим первый и последний день месяца, на который пришлась указанная дата
		$monthDay = (integer) date("j", $date);
		$date = $date - 3600 * 24 * ($monthDay - 1) + 3600 * 12; //Че за херня?? почему нужно прибавлять полсуток??
		$date2 = $date + 3600 * 24 * 31;
		$monthDay = (integer) date("j", $date2);
		$date2 = $date2 - 3600 * 24 * $monthDay;
		//
		while(($unitsDisplayed < $displayCount) && ($date2 >= $dateMin) && ($date2 >= $showFromDate)){
			//Вычисляем первый и последний день месяца
			$startDate = date('Y-m-d', $date);
			$stopDate = date('Y-m-d', $date2);
			//
			$query = "SELECT date FROM record_days WHERE project_id = '{$projectID}' AND date >= '{$startDate}' AND date <= '{$stopDate}'";
			$resultHasRecs = mysql_query($query);
			if (mysql_num_rows($resultHasRecs) != 0){ 
				displayTimeUnit($startDate, $stopDate, $projectID, $sourceTypeID, $displayType);
				$unitsDisplayed++;
			}
			mysql_free_result($resultHasRecs);
			//Находим первый и последний день следующего месяца
			$date2 = $date - 3600 * 24;
			$date = $date - 3600 * 24 * 2;
			$monthDay = (integer) date("j" ,$date);
			$date = $date - 3600 * 24 * ($monthDay - 1);
		}
		//
		$stopDate = date('Y-m-d', $date2);
	break;

	//если данные по заданному диапазону, просто выводим один блок
	case "range":
		//начальная и конечная дата периода
		$startDate = date('Y-m-d', $showFromDate);
		$stopDate = date('Y-m-d', strtotime($processDate));
		//для начала проверим есть ли вообще записи по диапазону
		$query = "SELECT date FROM record_days WHERE project_id = '{$projectID}' AND date >= '{$startDate}' AND date <= '{$stopDate}'";
		$resultHasRecs = mysql_query($query);
		//если да, то выводим данные по диапазону
		if (mysql_num_rows($resultHasRecs) != 0){
			displayTimeUnit($startDate, $stopDate, $projectID, $sourceTypeID, $displayType);
		}
		mysql_free_result($resultHasRecs);
		//возвращаем следующий дату за выведенным диапазоном в javascript страницы, запросившей данные
		//при этом типе периода ничего дополнительно подгружаться не должно
		$stopDate = date('Y-m-d', strtotime($showFromDate) - 24 * 3600);
	break;
	case "none":
		include ("instruction.php");
	break;
}
//возвращаем дату, начиная с которой будем грузить в следующий раз
echo "<script type='text/javascript'>stopDate = '{$stopDate}';</script>";
include ("db_close.php");
?>